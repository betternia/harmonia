import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.2.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
    kotlin("plugin.jpa") version "1.3.61"
    kotlin("kapt") version "1.2.71"

    // To inject git info in /management/info
    id("com.gorylenko.gradle-git-properties") version "2.2.2"

    id("com.github.node-gradle.node") version "2.2.3"

    id("com.google.cloud.tools.jib") version "2.1.0"

    id("org.liquibase.gradle") version "2.0.2"
}

group = "dev.betternia"
version = "0.0.5-SNAPSHOT"
//java.sourceCompatibility = JavaVersion.VERSION_1_8
java.sourceCompatibility = JavaVersion.VERSION_11
java.targetCompatibility = JavaVersion.VERSION_11

val liquibaseChangelog = "${project.projectDir}/src/main/resources/db/changelog/db.changelog-master.xml"

//val webuiFolder = "./${rootProject.name}-webui/"
val webuiFolder = "${project.projectDir}/${rootProject.name}-webui"

// Environment vars can be passed with -P<varname>=<value>
// Docker registry credentials
val dockerRegUsername: String? by project
val dockerRegPassword: String? by project

// TODO: Use the value (passed from CI, e.g. GitLab CI) to suffix the tag
val ciBuildId: String? by project

repositories {
    mavenCentral()
}

// Attempt to diable logback to use Log4j2 instead...
//configurations {
//    all {
//        exclude(group = "ch.qos.logback", module = "logback-classic")
//    }
//}

dependencies {
    implementation("javax.inject", "javax.inject", "1")
    implementation("javax.cache:cache-api")
//    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
//    {
//        exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
//    }
    implementation("org.springframework.boot:spring-boot-starter-cache")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-hateoas")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-hateoas")
    implementation("org.springframework.security:spring-security-data")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.liquibase:liquibase-core")
    implementation("com.h2database:h2")
    implementation("org.ehcache:ehcache")

    implementation("io.jsonwebtoken", "jjwt-api", "0.11.0")
    implementation("io.jsonwebtoken","jjwt-impl", "0.11.0")
    implementation("io.jsonwebtoken", "jjwt-jackson", "0.11.0")
    implementation("org.apache.commons:commons-lang3")

    implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-csv", "2.10.3")

    implementation("org.mapstruct", "mapstruct", "1.3.1.Final")
    kapt("org.mapstruct:mapstruct-processor:1.3.1.Final")

    implementation("org.springdoc", "springdoc-openapi-ui", "1.2.32")
    runtimeOnly("org.postgresql:postgresql")
    liquibaseRuntime("org.liquibase", "liquibase-core", "3.8.9")
    liquibaseRuntime("org.liquibase.ext", "liquibase-hibernate5", "3.8")
    liquibaseRuntime("org.postgresql", "postgresql")
    liquibaseRuntime( "ch.qos.logback", "logback-classic", "1.2.3")


    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.springframework.security:spring-security-test")
}

tasks.withType<Test> {
//    systemProperty("spring.profiles.active", "dev")
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

liquibase {
    activities.register("main") {
        this.arguments = mapOf(
                "logLevel" to "info",
                "changeLogFile" to liquibaseChangelog,
                "url" to "jdbc:postgresql://localhost:5432/harmonia-local",
                "username" to "harmonia",
                "password" to "harmonia"
        )
    }
}

node {
    // Version of node to use.
    version = "12.16.1"

    // Version of Yarn to use (Not specified will get the latest)
//    yarnVersion = "1.22.4"

    // Setting to false so instead of downloading, uses the installed one
    download = false

    // NOTE: Ultimately yarn_build did not work.
    // I had to create a new custom task yarnBuild based on YarnTask. See below
    yarnWorkDir = file(webuiFolder)

    System.out.println("** yarnWorkDir: " + yarnWorkDir.toString());
}

// Translates to `yarn install`
tasks.register<com.moowork.gradle.node.yarn.YarnTask>("webuiDepsInstall") {
    args = listOf("--cwd", webuiFolder, "install")
}
// Translates to `yarn build` (requires webuiDepsInstall)
tasks.register<com.moowork.gradle.node.yarn.YarnTask>("webuiBuild") {
    dependsOn("webuiDepsInstall")

    args = listOf("--cwd", webuiFolder, "build")
}

// Translates to `npm run install`
tasks.register<com.moowork.gradle.node.npm.NpmTask>("npmWebuiDepsInstall"){
    setArgs(listOf("--prefix", webuiFolder, "install"))
}
// Translates to `npm run build` (requires webuiDepsInstall)
tasks.register<com.moowork.gradle.node.npm.NpmTask>("npmWebuiBuild") {
    dependsOn("npmWebuiDepsInstall")

    setArgs(listOf("--prefix", webuiFolder, "run", "build", "--loglevel", "verbose"))
}

// Copies client build into SpringBoot web folder
tasks.register<Copy>("webuiInstall") {
//    dependsOn("webuiBuild")
    dependsOn("npmWebuiBuild")

    from("${webuiFolder}/build")
    into("$buildDir/resources/main/public")
}

tasks.register<Copy>("webuiInstallIde") {
    dependsOn("webuiBuild")

    from("${webuiFolder}/build")
//    include("*")
    into("${project.projectDir}/out/production/resources/public")
}

// Add dependency on webuiInstall
tasks {
    assemble {
        dependsOn("webuiInstall");
    }
}

// Docker container
jib {
    to {
//        image = "docker.io/betternia/${rootProject.name}:${version}"
        image = "registry.gitlab.com/betternia/${rootProject.name}:${version}"
        auth {
            username = dockerRegUsername // Defined in 'gradle.properties'.
            password = dockerRegPassword
        }
    }
}

tasks.register("dump") {
    doLast {
        println("webuiFolder: " + webuiFolder)
        println("dockerRegUsername: " + dockerRegUsername)
    }
}