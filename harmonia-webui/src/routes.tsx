import React from "react";

import SiteIndex from "./components/site/SiteHome";
import LoginForm from "./components/auth/LoginForm";

import UsersList from "./components/user/UsersList";
import UserDetails from "./components/user/UserDetails";
import UserForm from "./components/user/UserForm";

import PersonsList from "./components/person/PersonsList";
import PersonDetails from "./components/person/PersonDetails";
import PersonFormContainer from "./components/person/PersonForm";
import PersonsAdmin from "./components/person/PersonsAdmin";

//////////
import RealmsList from "./components/realm/RealmsList";
import RealmDetails from "./components/realm/RealmDetails";
import RealmForm from "./components/realm/RealmForm";


import { FaUser, FaAddressBook, FaUserFriends } from 'react-icons/fa';

function RealmHome() {
    return <h2>RealmHome</h2>;
}

function Groups() {
    return <h2>Groups</h2>;
}

interface IRouteItem {
    path: string,
    component: any,
    icon?: any,
    sidebarName?: string,
    showMenu?: boolean  // WIP: flag to show/hide side menu
}

const REALM_PLACEHOLDER = ":realmId";
const REALM_ROUTE_PATH_BASE = "/" + REALM_PLACEHOLDER;

const Routes: Array<IRouteItem> = [
    {
        path: '/login',
        component: LoginForm,
        showMenu: false
    },
    {
        path: '/',
        component: SiteIndex,
        showMenu: false
    },
    {
        path: REALM_ROUTE_PATH_BASE,
        component: RealmHome
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/users',
        sidebarName: "Users",
        icon: <FaUser />,
        component: UsersList
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/users/:userUid/details',
        component: UserDetails
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/users/new',
        component: UserForm
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/users/:userUid/edit',
        component: UserForm
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/persons',
        sidebarName: "Persons",
        icon: <FaAddressBook />,
        component: PersonsList
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/persons/admin',
        sidebarName: "Persons Admin",
        icon: <FaAddressBook />,
        component: PersonsAdmin
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/persons/:personUid/details',
        component: PersonDetails
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/persons/new',
        component: PersonFormContainer
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/persons/:personUid/edit',
        component: PersonFormContainer
    },
    {
        path: REALM_ROUTE_PATH_BASE + '/organizations',
        sidebarName: "Groups",
        icon: <FaUserFriends />,
        component: Groups
    }
];

Routes.push({
    path: REALM_ROUTE_PATH_BASE + '/realms',
    sidebarName: "Realm",
    icon: <FaAddressBook />,
    component: RealmsList
});
Routes.push({
    path: REALM_ROUTE_PATH_BASE + '/realms/:realmUid/details',
    component: RealmDetails
});
Routes.push({
    path: REALM_ROUTE_PATH_BASE + '/realms/new',
    component: RealmForm
});
Routes.push({
    path: REALM_ROUTE_PATH_BASE + '/realms/:realmUid/edit',
    component: RealmForm
});

export default Routes;
