import React, {Component} from 'react';
import {Provider} from 'react-redux';
import createDefaultStore, {createDevStore} from './store';
import App from './App';

import {loginSuccess} from './store/auth/actions';
import {SecurityContext} from './components/auth/SecurityContext';
import {setAuthorizationHeader} from "./framework/apiclient-util";
import * as authApi from "./components/auth/auth-api";

const store = (process.env.NODE_ENV === 'production') ? createDefaultStore() :  createDevStore();

const accessToken = SecurityContext.getAccessToken();
if (accessToken) {
    setAuthorizationHeader(accessToken);
    authApi.fetchUserInSession()
        .then( currentUser => {
            store.dispatch(loginSuccess(accessToken, currentUser));
        })
        .catch( error => {
            alert("Error retrieving userinfo:" + error);
        })

}

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <App/>
            </Provider>
        )
    }
}