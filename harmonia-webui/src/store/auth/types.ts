import {Credentials} from "../../components/auth/Credentials"
import {User} from "../../components/user/User"

// State
export interface AuthState {
    loggedIn: boolean,
    session?: string,
    user?: User,
    error?: string
}

// Actions
export const AUTH_PRE_LOGIN_LOCATION = 'AUTH_PRE_LOGIN_LOCATION'; // TODO
export const AUTH_LOGIN_REQUEST = 'AUTH_LOGIN_REQUEST';
export const AUTH_LOGIN_SUCCESS = 'AUTH_LOGIN_SUCCESS';
export const AUTH_LOGIN_FAILURE = 'AUTH_LOGIN_FAILURE';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

interface LoginRequestAction {
    type: typeof AUTH_LOGIN_REQUEST,
    credentials: Credentials
}

interface LoginSuccessAction {
    type: typeof AUTH_LOGIN_SUCCESS,
    session: string,
    user?: User,
}

interface LoginFailureAction {
    type: typeof AUTH_LOGIN_FAILURE,
    error: any
}

interface LogOutAction {
    type: typeof AUTH_LOGOUT
}

export type SystemActionTypes = LoginRequestAction | LoginSuccessAction | LoginFailureAction |LogOutAction;
