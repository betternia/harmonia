import {Credentials} from "../../components/auth/Credentials";
import {AUTH_LOGIN_FAILURE, AUTH_LOGIN_REQUEST, AUTH_LOGIN_SUCCESS, AUTH_LOGOUT, SystemActionTypes} from "./types";
import {AnyAction, Dispatch} from "redux";
import {ThunkAction} from 'redux-thunk'

import {setAuthorizationHeader} from "../../framework/apiclient-util"
import * as authApi from "../../components/auth/auth-api";
import {User} from "../../components/user/User";
import {SecurityContext} from "../../components/auth/SecurityContext";

export function loginSuccess(session: string, user?: User): SystemActionTypes {
    return {type: AUTH_LOGIN_SUCCESS, session, user}
}

// https://jasonwatmore.com/post/2017/09/16/react-redux-user-registration-and-login-tutorial-example
export function login(username: string, password: string): ThunkAction<Promise<any>, {}, {}, AnyAction> {
    return async (dispatch: Dispatch): Promise<void> => {
        dispatch(request({username, password}));

        try {
            const tokens = await authApi.authenticate({username, password});
            // TODO: fetch user info and pass as second param
            setAuthorizationHeader(tokens.accessToken);
            const currentUser = await authApi.fetchUserInSession();
            // SecurityContext.setCurrentUser();
            // const currentUser: User | undefined = undefined;
            SecurityContext.setAccessToken(tokens.accessToken, currentUser);
            dispatch(loginSuccess(tokens.idToken, currentUser));
        } catch (error) {
            dispatch(failure(error.toString()));
            // dispatch(alertActions.error(error.toString()));
            alert ("Error: " + error.toString());
        }
    };

    function request(credentials: Credentials) {
        return {type: AUTH_LOGIN_REQUEST, credentials}
    }


    function failure(error: string) {
        return {type: AUTH_LOGIN_FAILURE, error}
    }
}

export function logOut(): SystemActionTypes {
    authApi.logOut();
    SecurityContext.removeAccessToken();
    return {
        type: AUTH_LOGOUT
    }
}