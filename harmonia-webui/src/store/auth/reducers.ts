import {AUTH_LOGIN_REQUEST, AUTH_LOGIN_SUCCESS, AUTH_LOGOUT, AuthState, SystemActionTypes} from "./types";

const initialState: AuthState = {
    loggedIn: false,
    session: undefined,
    user: undefined
};

export function authReducer(state = initialState, action: SystemActionTypes): AuthState {
    switch (action.type) {
        case AUTH_LOGIN_REQUEST:
            return Object.assign({}, state, {
                loggedIn: false,
                session: undefined,
                user: undefined
            });
        case AUTH_LOGIN_SUCCESS:
            return Object.assign({}, state, {
                loggedIn: true,
                session: action.session,
                user: action.user
            });
        case AUTH_LOGOUT:
            return Object.assign({}, state, {
                loggedIn: false,
                session: undefined,
                user: undefined
            });
        default:
            return state
    }
}

