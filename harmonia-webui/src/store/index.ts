import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {authReducer} from './auth/reducers';

const loggerMiddleware = createLogger()

const rootReducer = combineReducers({
    auth: authReducer
});

export default function createDefaultStore(preloadedState?: any) {
    return createStore(
        rootReducer,
        preloadedState,
        applyMiddleware(thunkMiddleware, loggerMiddleware),
    )
}

export function createDevStore(preloadedState?: any) {
    return createStore(
        rootReducer,
        preloadedState,

        // Enabling Redux DevTools
        // https://github.com/jaysoo/todomvc-redux-react-typescript/issues/8
        compose(
            applyMiddleware(thunkMiddleware, loggerMiddleware),
            (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
        )
    )
}


export type RootState = ReturnType<typeof rootReducer>
