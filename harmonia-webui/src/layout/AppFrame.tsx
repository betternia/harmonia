import React from 'react';

import {connect} from 'react-redux';
import {generatePath, Link, matchPath, Route, RouteComponentProps, withRouter} from 'react-router-dom';

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import {FaRegUser} from 'react-icons/fa';

import logo from '../logo.svg';

import {useTranslation} from 'react-i18next';
import '../framework/i18n'

import {RootState} from '../store';
import {AuthState} from '../store/auth/types'
import {ThunkDispatch} from "redux-thunk";
import {logOut} from "../store/auth/actions";
// import {SecurityContext} from "../components/auth/SecurityContext"

const drawerWidth = 200;

const appBarColor = (theme: Theme) => {
    if (process.env.NODE_ENV === 'test')
        return theme.palette.secondary.light;
    else if (process.env.NODE_ENV === 'development')
        return theme.palette.primary.light;
    else return theme.palette.primary.main;
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        grow: {
            flexGrow: 1,
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
            backgroundColor: appBarColor(theme)
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        toolbar: theme.mixins.toolbar,

    }),
);

type AppFrameProps = {
    routes: any,
    children?: any,
    auth: AuthState,
    logOut(): any
}



// TODO: Provide type for the props
export function AppFrame(props: AppFrameProps & RouteComponentProps<{ realmId: string }>) {
    const classes = useStyles();
    const {t, i18n} = useTranslation("app");

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const match = matchPath<{ realmId: string }>(window.location.pathname, {
        path: "/:realmId",
        exact: false,
        strict: false
    });
    // const realmId = props.match?.params?.realmId;
    const realmId = match?.params?.realmId;

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const signOut = () => {
        props.logOut();
        props.history.push("/");
    };

    // Through `withRouter`
    const activeRoute = (routeName: string) => {
        return props.location.pathname.indexOf(routeName) > -1 ? true : false;
    };

    // TODO: Parameterize the show side-menu flag
    const showMenu = () => {
        return realmId && ((props.location.pathname.indexOf("login") > -1) ? false : true);
    };

    /**
     * Render the user account menu
     */
    const renderUserMenu = () => {

        //const userMenu = (SecurityContext.getAccessToken()) ?
        const userMenu = (props.auth.loggedIn) ?
            (
                <div>
                    <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={handleMenu}
                        color="inherit"
                    >
                        <FaRegUser/>
                        {props.auth.user?.userName}
                    </IconButton>
                    <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={open}
                        onClose={handleClose}
                    >
                        <MenuItem onClick={handleClose}>{t('Profile')}</MenuItem>
                        <MenuItem onClick={handleClose}>{t('My Account')}</MenuItem>
                        <MenuItem onClick={signOut}>{t('Sign Out')}</MenuItem>
                    </Menu>
                </div>
            ) : (
                <div>
                    <IconButton
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={handleMenu}
                        color="inherit"
                    >
                        <a href={"/login"}>{t("Sign In")}</a>
                    </IconButton>
                </div>
            );

        return userMenu
    };

    const renderLeftMenu = (
        <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.toolbar}/>
            <List>
                {/* Set the Icons Icon and link */}
                {props.routes.map((route: any, key: number) => {
                        return (
                            (realmId && route.sidebarName) ? (
                                <Link to={generatePath(route.path, {realmId})} style={{textDecoration: 'none'}} key={key}>
                                    <MenuItem selected={activeRoute(route.path)}>
                                        <ListItemIcon>
                                            <div>{route.icon}</div>
                                        </ListItemIcon>
                                        <ListItemText primary={t(route.sidebarName)}/>
                                    </MenuItem>
                                </Link>
                            ) : ""
                        );
                    }
                )}
            </List>
        </Drawer>
    );


    return (
        <div className={classes.root}>
            <CssBaseline/>

            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar variant="dense">
                    <img src={logo} className="App-logo" alt="logo" height={40}/>
                    <Typography variant="h6" noWrap onClick={() => {
                        props.history.push(generatePath("/:realmId", {realmId}))
                    }}>
                        Harmonia
                    </Typography>
                    <div className={classes.grow}/>
                    {["ko", "en", "es"].map((lang: any, key: number) => {
                        return (<button onClick={() => i18n.changeLanguage(lang)} key={lang}>{lang} </button>)
                    })}
                    {renderUserMenu()}
                </Toolbar>
            </AppBar>

            {(showMenu()) ? renderLeftMenu : ""}


            {/* The main content pane: */}
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                {props.routes.map((prop: any, key: number) => {
                    return (
                        <Route path={prop.path} exact component={prop.component} key={key}/>
                    )
                })}
                {props.children}

            </main>

        </div>
    );
}

// export default withRouter(AppFrame);

function mapStateToProps(state: RootState) {
    return {
        auth: state.auth
    }
}

const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => ({
    logOut: () => dispatch(logOut())
});

//https://reacttraining.com/react-router/web/guides/redux-integration
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppFrame))
