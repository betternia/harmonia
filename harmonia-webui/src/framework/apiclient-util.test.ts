// another alternative to nock is Moxios (https://github.com/axios/moxios)
// nock when working exclusively with Axios
import nock from "nock";

import httpAdapter from 'axios/lib/adapters/http';

import { createApiClient, AUTH_TOKEN_KEY } from "./apiclient-util";


test('apiClient do not send Authorization header when Storage is empty', async () => {

    // ARRANGE:
    const scope = nock("http://localhost:9000")
        .get(uri => uri.startsWith("/test"))
        .reply(200, function (uri, requestBody) {
            // console.log('path:', this.req.path);
            // console.log('headers:', JSON.stringify(this.req.headers, null, 2));

            expect (this.req.headers).not.toHaveProperty('authorization');
            return [200, {"test-data": "Dummy value"}];
        }, {'Access-Control-Allow-Origin': '*'});

    const apiClient = createApiClient("http://localhost:9000");

    // It is necessary to set the adapter to httpAdapter for the test
    apiClient.defaults.adapter = httpAdapter;

    // ACT:
    const response = await apiClient.get("/test");
    
    // ASSERT:
    expect(response.status).toEqual(200);
});

test('apiClient sends Authorization header from Storage', async () => {

    // ARRANGE:
    const scope = nock("http://localhost:9000")
        .matchHeader('Authorization', 'Bearer TEST-TOKEN')
        .get(uri => uri.startsWith("/test"))
        // .reply(200, {"test-data": "good test"}, {'Access-Control-Allow-Origin': '*'});
        .reply(200, function (uri, requestBody) {
            return [200, {"test-data": "Dummy value"}];
        }, {'Access-Control-Allow-Origin': '*'});

    window.sessionStorage.setItem(AUTH_TOKEN_KEY, "TEST-TOKEN");

    const apiClient = createApiClient("http://localhost:9000");

    // It is necessary to set the adapter to httpAdapter for the test
    apiClient.defaults.adapter = httpAdapter;

    // ACT:
    const response = await apiClient.get("/test");
    window.sessionStorage.removeItem(AUTH_TOKEN_KEY);
    
    // ASSERT:
    expect(response.status).toEqual(200);
});
