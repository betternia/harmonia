import {Moment} from "moment";

export interface ModelBase {
    uid?: string;
    createdBy?: string;
    createdDate?: Moment;
    lastModifiedBy?: string;
    lastModifiedDate?: Moment;
}