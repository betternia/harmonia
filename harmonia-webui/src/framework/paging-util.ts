
export type Pageable = {
    page: number,
    size: number,
    sort?: any
}

export function generatePagingQueryString(pageable?: Pageable) {
    let pagingQueryString = "";
    if (pageable?.page && pageable?.page > 0) {
        pagingQueryString += `&page=${pageable?.page - 1}`;
    }
    if (pageable?.size && pageable?.size > 0) {
        pagingQueryString += `&size=${pageable?.size}`;
    }
    return pagingQueryString;
}

export function generateCriteriaQueryString(criteria?: any, prependAmpersand = true) {
    if (criteria === undefined || criteria === null) {
        return ""
    }
    let str = [];
    for (let p in criteria) {
        if (criteria.hasOwnProperty(p) && notNullOrEmpty(criteria[p]) ) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(criteria[p]));
        }
    }
    let queryString = str.join("&");
    if (prependAmpersand && queryString.trim() !== "") {
        queryString = "&" + queryString;
    }
    return queryString;
}

function notNullOrEmpty(value?: any): boolean {
    if (typeof value === 'string') {
        return (value !== undefined) && (value !== null) && (value.trim().length > 0)
    } else if (Array.isArray(value)) {
        return (value !== undefined) && (value !== null) && (value.length > 0)
    } else {
        return (value !== undefined) && (value !== null)
    }
}

export const initPageInfo = {
    "size": 10,
    "totalElements": 0,
    "totalPages": 0,
    "number": 0
};

