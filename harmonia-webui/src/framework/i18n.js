import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import person_en from '../components/person/i18n/person.en';
import person_ko from '../components/person/i18n/person.ko';

import user_en from '../components/user/i18n/user.en';
import user_ko from '../components/user/i18n/user.ko';


import app_ko from '../layout/i18n/app.ko';


// the translations
// (tip move them in a JSON file and import them)
const resources = {
    en: {
        user: user_en,
        person: person_en
    },
    ko: {
        app: app_ko,
        user: user_ko,
        person: person_ko
    }
};

i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources,
        lng: "ko",
        debug: process.env.NODE_ENV !== 'production',
        keySeparator: false, // we do not use keys in form messages.welcome

        interpolation: {
            escapeValue: false // react already safes from xss
        }
    });

export default i18n;
