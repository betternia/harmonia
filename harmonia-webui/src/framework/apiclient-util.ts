import axios, {AxiosError, AxiosInstance} from "axios";

import {Storage} from "./storage-util";

export const BASE_URL = process.env.REACT_APP_API_BASE_URL
    || (process.env.NODE_ENV === 'development') ? "http://localhost:8080" : ''
    || '';

export const AUTH_TOKEN_KEY = 'harmonia-token';

let _instances: Array<AxiosInstance> = [];

export function createApiClient(basePath: string, baseUrl = BASE_URL) {

    const bearerToken = Storage.session.get(AUTH_TOKEN_KEY);
    // console.log("createApiClient.bearerToken: " + bearerToken);

    let instance = axios.create({
        baseURL: baseUrl + basePath,
        responseType: 'json'
    });

    instance.defaults.headers.common['Content-Type'] = 'application/json';
    if (bearerToken) {
        instance.defaults.headers.common['Authorization'] = "Bearer " + bearerToken;
    }

    _instances.push(instance);

    return instance;
}

export function setAuthorizationHeader(bearerToken: string) {
    // axios.defaults.headers.common['Authorization'] = "Bearer " + bearerToken;
    for(let i=0; i < _instances.length; i++) {
        _instances[i].defaults.headers.common['Authorization'] = "Bearer " + bearerToken;
    }
}

type ServerError = {
    code: string,
    description: string
};

export type RemoteError = AxiosError<ServerError>;