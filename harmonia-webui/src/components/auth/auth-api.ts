import {Credentials} from './Credentials';

import {User} from "../user/User";
import {createApiClient} from "../../framework/apiclient-util";

const apiClient = createApiClient("/api");

export const authenticate = async (creds: Credentials) => {
    const response = await apiClient.post<any>('/authenticate', creds);

    const authHeader = response.headers.authorization;

    if (authHeader && authHeader.slice(0, 7) === 'Bearer ') {
        const jwt = authHeader.slice(7, authHeader.length);
        // SecurityContext.setAccessToken(jwt);
        return {
            idToken: response.data.id_token,
            accessToken: jwt
        }
    }

    // Probably error
    return response.data;
};

/**
 * Retrieves the user details given session token
 *
 * precondition: user logged in, and has valid, unexpired token
 */
export const fetchUserInSession = async () => {
    // TODO: pass header - Authorization: Bearer <token> 
    const response = await apiClient.get<User>('/userinfo');
    return response.data;
};

/**
 * Changes password
 *
 * precondition: user logged in, and has valid, unexpired token
 */
export const changePassword = async (password: String) => {
    const response = await apiClient.put<any>('/password', password);
    return response.data;
};


/**
 * Logout
 *
 * precondition: user logged in, and has valid, unexpired token
 */
export const logOut = async () => {
    const response = await apiClient.get<any>('/logout');
    return response.data;
};
