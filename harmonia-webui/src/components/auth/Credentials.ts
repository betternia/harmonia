export interface Credentials {
    username: string;
    password: string;
}

export function emptyCredentials(): Credentials {
    return {
        username: "",
        password: ""
    }
}