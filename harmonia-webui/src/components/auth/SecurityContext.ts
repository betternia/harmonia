import {Storage} from "../../framework/storage-util";
import {User} from "../user/User"

const AUTH_TOKEN_KEY = 'harmonia-authToken';

export class SecurityContext {

    static currentUser?: User;

    public static setAccessToken(token: string, user?: User) {
        Storage.session.set(AUTH_TOKEN_KEY, token);
        SecurityContext.currentUser = user;
    }

    public static setCurrentUser(user: User) {
        SecurityContext.currentUser = user;
    }

    public static getAccessToken(): string | undefined {
        const token = Storage.session.get(AUTH_TOKEN_KEY);
        return token;
    }

    public static removeAccessToken() {
        Storage.session.remove(AUTH_TOKEN_KEY);
    }
}