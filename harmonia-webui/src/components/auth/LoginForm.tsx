import React, {useState} from 'react';
import {connect} from 'react-redux';
import {ThunkDispatch} from 'redux-thunk';
import {Redirect, RouteComponentProps, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import {useTranslation} from 'react-i18next';
import '../../framework/i18n'

import {Credentials, emptyCredentials} from "./Credentials";

import {RootState} from '../../store';
import {login} from '../../store/auth/actions';
import {AuthState} from "../../store/auth/types";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
    }),
);

type LoginFormProps = {
    onSubmit(credentials: Credentials): void;
};

const LoginForm = (props: LoginFormProps) => {
    const classes = useStyles();
    const {t} = useTranslation("app");

    const {onSubmit} = props;

    const [formValues, setFormValues] = useState<Credentials>(emptyCredentials());
    // console.log("[PersonForm] formValues: " + JSON.stringify(formValues, null, 2));

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        // alert("Event:" + JSON.stringify(eventDetails,null, null, 2));
        onSubmit(formValues)
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TextField id="username" name="username" label={t('Username')} value={formValues.username}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="password" name="password" label={t('Password')} value={formValues.password}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                        {t("Sign In")}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};


type LoginFormContainerProps = {
    auth: AuthState,
    authenticate(username: string, password: string): any;
}

const LoginFormContainer = (props: LoginFormContainerProps & RouteComponentProps<any>) => {

    const performLogin = (credentials: Credentials) => {
        // console.log("Authenticating credentials: " + JSON.stringify(credentials));
        // const response = authApi.authenticate(credentials);
        props.authenticate(credentials.username, credentials.password);

        // TODO: return to the previous page
        props.history.push("/");
        // console.log("Authenticate completed: " + JSON.stringify(response));
    };

    // If already loggedin do not show to form and redirect to root
    if (props.auth.loggedIn) {
        const redirectTo = '/';
        return <Redirect to={redirectTo}/>
    }

    return (
        <LoginForm onSubmit={performLogin}/>
    );
};

const mapStateToProps = (state: RootState) => ({
    auth: state.auth
});


const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, any>) => ({
    authenticate: (username: string, password: string) => dispatch(login(username, password))
});

// export default withRouter(PersonFormContainer)
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginFormContainer))
