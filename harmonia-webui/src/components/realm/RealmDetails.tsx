import React, {useEffect, useState} from 'react';
import {generatePath, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import {FaRegEdit} from 'react-icons/fa';

import {newRealm, Realm} from "./Realm";
import * as realmApi from './realm-api';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            textAlign: 'right',
            color: theme.palette.text.secondary,
        },
        dataCell: {
            padding: theme.spacing(3),
            // color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        }
    }),
);

type RealmDetailsProps = {
    // using `interface` is also ok
    realmId: string;
    realm: Realm;
};

const RealmDetails = (props: RealmDetailsProps) => {
    const classes = useStyles();

    const {realmId, realm} = props;

    return (
        <React.Fragment>
            <a href={generatePath("/:realmId/realms/:uid/edit", { realmId, uid: realm.uid ? realm.uid : "0" })}><FaRegEdit/></a>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Name
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.name}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Synopsis
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.synopsis}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Description
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.description}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Type
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.type}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Created at
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.createdDate}
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Modified at
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {realm.lastModifiedDate}
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

const RealmDetailsContainer = (props: any) => {

    const [realm, setRealm] = useState(newRealm());

    const realmId = props.match?.params?.realmId;

    const realmUid = props.match?.params?.realmUid;

    useEffect(() => {
        const fetchData = async (realmUid: string) => {
            try {
                const result = await realmApi.fetchOne(realmUid);
                if (result) {
                    setRealm(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        if (realmUid) {
            fetchData(realmUid);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <RealmDetails realmId={realmId} realm={realm}/>
    );
};

export default withRouter(RealmDetailsContainer);

