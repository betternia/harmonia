import {ModelBase} from "../../framework/ModelBase"

export interface Realm extends ModelBase {

    id?: string;
    name?: string;
    synopsis?: string;
    description?: string;
    type?: string;
    status?: string;
    audience?: string;
    location?: string;
    country?: string;
    language?: string;
    thumbnailImage?: string;
    coverImage?: string;
    properties?: any;
}

export function newRealm(uid: string | undefined = undefined): Realm {
    return {
        uid: uid,
        createdBy: undefined,
        createdDate: undefined,
        lastModifiedBy: undefined,
        lastModifiedDate: undefined,

        id: '',
        name: '',
        synopsis: '',
        description: '',
        type: '',
        status: '',
        audience: '',
        location: '',
        country: '',
        language: '',
        thumbnailImage: '',
        coverImage: '',
        properties: ''
    };
};
