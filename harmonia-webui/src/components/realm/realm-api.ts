import {createApiClient, RemoteError} from '../../framework/apiclient-util';
import {Realm} from './Realm';

const apiClient = createApiClient("/api/realms");

export const fetchList = async (queryParams: any) => {
    try {
        const response = await apiClient.get('');
        return response.data;
    } catch (error) {
        if (error && error.response) {
            const remoteError = error as RemoteError;
            return remoteError.response?.data;
        }
        throw error;
    }
};

export const fetchOne = async (uid: string) => {
    const response = await apiClient.get<Realm>('/' + uid);
    return response.data;
};

export const create = async (person: Realm) => {
    const response = await apiClient.post<Realm>('/', person);
    return response.data;
};

export const update = async (person: Realm) => {
    const response = await apiClient.put<Realm>('/' + person.uid, person);
    return response.data;
};

export const deleteOne = async (uid: string) => {
    const response = await apiClient.delete<Realm>('/' + uid);
    return response.data;
};