import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import {newRealm, Realm} from "./Realm";
import * as realmApi from './realm-api';
import {useTranslation} from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: 2,
        }
    }),
);

type RealmFormProps = {
    // using `interface` is also ok
    realm: Realm;
    onSubmit(realm: Realm): void;
};

const RealmForm = (props: RealmFormProps) => {
    const classes = useStyles();
    // const theme = useTheme();

    const {t} = useTranslation("app");

    const {realm, onSubmit} = props;

    const [formValues, setFormValues] = useState<Realm>(realm);
    // console.log("[RealmForm] realm: " + JSON.stringify(realm, null, 2));
    // console.log("[RealmForm] formValues: " + JSON.stringify(formValues, null, 2));

    useEffect(() => {
        const fetchData = async (realmUid: string) => {
            try {
                const result = await realmApi.fetchOne(realmUid);
                if (result) {
                    setFormValues(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        // TODO: check if other values are set, not need to fetch again.
        if (realm.uid) {
            fetchData(realm.uid);
        }

    }, [realm.uid]);

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        // alert("Event:" + JSON.stringify(eventDetails,null, null, 2));
        onSubmit(formValues)
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TextField id="lastName" name="lastName" label={t("Id")} value={formValues.id}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="firstName" name="firstName" label="Name" value={formValues.name}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="realm.realmName" name="realmName" label="Synopsis" value={formValues.synopsis}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="realm.email" name="email" label="Description" value={formValues.description}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="realm.langKey" name="langKey" label="Type" value={formValues.type}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="realm.langKey" name="langKey" label="Audience" value={formValues.audience}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>

            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                        {(realm.uid) ? "Update" : "Add"}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

const RealmFormContainer = (props: any) => {

    // props.match populated by withRouter()
    const realm = newRealm(props.match?.params?.realmUid);

    const updateRealm = (realm: Realm) => {
        console.debug("Updating Realm: " + JSON.stringify(realm));
        const response = realmApi.update(realm);
        console.log("Updated Realm: " + JSON.stringify(response));
    };

    const addRealm = (realm: Realm) => {
        console.debug("Add Realm: " + JSON.stringify(realm));
        const response = realmApi.create(realm);
        console.log("Added Realm: " + JSON.stringify(response));
    };

    return (
        <RealmForm realm={realm} onSubmit={(realm.uid) ? updateRealm : addRealm}/>
    );
};

export default withRouter(RealmFormContainer)

