import React, {useEffect, useState} from 'react';
import {generatePath, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import {FaRegEdit} from 'react-icons/fa';

import {newRealm, Realm} from "./Realm";
import * as realmApi from './realm-api';
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            textAlign: 'right',
            color: theme.palette.text.secondary,
        },
        dataCell: {
            padding: theme.spacing(3),
            // color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        }
    }),
);

type RealmHomeProps = {
    // using `interface` is also ok
    realmId: string;
    realm: Realm;
};

const RealmHome = (props: RealmHomeProps) => {
    const classes = useStyles();

    const {realmId, realm} = props;

    return (
        <React.Fragment>
            <h1>{realm.name}</h1>
            <Typography variant="subtitle1" color="textSecondary">
                {realm.synopsis}
            </Typography>


        </React.Fragment>
    );
};

const RealmHomeContainer = (props: any) => {

    const [realm, setRealm] = useState(newRealm());

    const realmId = props.match?.params?.realmId;

    const realmUid = props.match?.params?.realmUid;

    useEffect(() => {
        const fetchData = async (realmUid: string) => {
            try {
                const result = await realmApi.fetchOne(realmUid);
                if (result) {
                    setRealm(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        if (realmUid) {
            fetchData(realmUid);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <RealmHome realmId={realmId} realm={realm}/>
    );
};

export default withRouter(RealmHomeContainer);
