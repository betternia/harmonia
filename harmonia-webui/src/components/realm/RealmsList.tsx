import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { FaPlusCircle, FaMinusCircle, FaRegEdit } from 'react-icons/fa';

import { useTranslation } from 'react-i18next';
import '../../framework/i18n'

import {Realm} from "./Realm";
import * as realmApi from './realm-api';
import {generatePath, withRouter} from "react-router-dom";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    actionIcon: {
        margin: 2
    }
});

// TODO: refactor
const initPageInfo = {
    "size": 20,
    "totalElements": 0,
    "totalPages": 0,
    "number": 0
}

export function RealmsList(props: any) {
    const classes = useStyles();
    const { t } = useTranslation("realm");

    const [rows, setRows] = useState<Array<Realm>>([]);
    const [pageInfo, setPageInfo] = useState<typeof initPageInfo>(initPageInfo);

    const realmId = props.match?.params?.realmId;

    async function fetchList() {
        try {
            const result = await realmApi.fetchList(null);
            if (result) {
                setPageInfo(result?.page);
                setRows(result._embedded.realms)
            }
        } catch (error) {
            const errorMessage = JSON.stringify(error, null, 2);
            console.error(errorMessage);
            // TODO: Error boundary
            alert ("Error: " + error.toString());
        }
    }

    useEffect(() => {
        fetchList();
    }, []);

    const handleDelete = (uid: string | undefined) => (event: React.MouseEvent<HTMLButtonElement>) => {
        async function deleteOne(uid: string) {
            try {
                const result = await realmApi.deleteOne(uid);
                console.log(result)
                fetchList();
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        }

        if (uid) {
            deleteOne(uid);
        }
    };

    return (
        <div>
            <a href={generatePath("/:realmId/realms/new", { realmId })}><FaPlusCircle />{t('Add New')}</a>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Id')}</TableCell>
                            <TableCell>{t('Name')}</TableCell>
                            <TableCell>{t('Audience')}</TableCell>
                            <TableCell>{t('Synopsis')}</TableCell>
                            <TableCell>{t('Status')}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.uid}>
                                <TableCell component="th" scope="row">
                                    <a href={generatePath("/:realmId/realms/:uid/details", { realmId, uid: row.uid })}>{row.id}</a>
                                </TableCell>
                                <TableCell>{row.name}</TableCell>
                                <TableCell>{row.audience}</TableCell>
                                <TableCell>{row.synopsis}</TableCell>
                                <TableCell>{row.status}</TableCell>
                                <TableCell>
                                    <span className={classes.actionIcon}><a href={generatePath("/:realmId/realms/:uid/edit", { realmId, uid: row.uid })}><FaRegEdit /></a></span>
                                    <span className={classes.actionIcon}><button onClick={handleDelete(row.uid)}><FaMinusCircle /></button></span>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            { JSON.stringify(pageInfo) }
        </div>
    );
}

export default withRouter(RealmsList);