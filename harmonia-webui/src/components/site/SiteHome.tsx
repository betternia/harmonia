import React, {useEffect, useState} from 'react';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import {generatePath, withRouter} from "react-router-dom";
import {useTranslation} from 'react-i18next';
import '../../framework/i18n'

import {initPageInfo} from "../../framework/paging-util";
import {Realm} from "../realm/Realm";
import * as realmApi from '../realm/realm-api';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        details: {
            display: 'flex',
            flexDirection: 'column',
        },
        content: {
            flex: '1 0 auto',
        },
        cover: {
            width: 151,
        },
        controls: {
            display: 'flex',
            alignItems: 'center',
            paddingLeft: theme.spacing(1),
            paddingBottom: theme.spacing(1),
        },
        playIcon: {
            height: 38,
            width: 38,
        },
    }),
);



export function SiteIndex(props: any) {
    const classes = useStyles();

    const { t } = useTranslation("realm");

    const [rows, setRows] = useState<Array<Realm>>([]);
    const [pageInfo, setPageInfo] = useState<typeof initPageInfo>(initPageInfo);

    async function fetchList() {
        try {
            const result = await realmApi.fetchList(null);
            if (result) {
                setPageInfo(result?.page);
                setRows(result._embedded.realms)
            }
        } catch (error) {
            const errorMessage = JSON.stringify(error, null, 2);
            console.error(errorMessage);
            // TODO: Error boundary
            alert ("Error: " + error.toString());
        }
    }

    useEffect(() => {
        fetchList();
    }, []);


    return (
        <div>
            {rows.map(row => (
                <Card className={classes.root}>
                    <div className={classes.details}>
                        <CardContent className={classes.content}>
                            <Typography component="h5" variant="h5">
                                {row.name}
                            </Typography>
                            <Typography variant="subtitle1" color="textSecondary">
                                {row.synopsis}
                            </Typography>
                        </CardContent>
                        <div className={classes.controls}>
                            <Button onClick={() => {
                                props.history.push(generatePath("/:realmId", {realmId: row.id}))
                            }}>{t("Go")}</Button>
                        </div>
                    </div>
                    <CardMedia
                        className={classes.cover}
                        image={row.thumbnailImage}
                        title="Realm Image"
                    />
                </Card>
            ))}
            { JSON.stringify(pageInfo) }
        </div>
    );
}

export default withRouter(SiteIndex);