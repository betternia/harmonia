import React, {useState} from 'react';
import {RouteComponentProps, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import * as personApi from './person-api';
import {useTranslation} from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        input: {

        }
    }),
);

type PersonsAdminProps = {
    // using `interface` is also ok
    realmId: string;
};

const PersonsAdmin = (props: PersonsAdminProps & RouteComponentProps<{ realmId: string }>) => {
    const classes = useStyles();
    const { t } = useTranslation("person");

    const [formValues, setFormValues] = useState<{file?: any, action?: string}>({file: undefined, action: undefined});

    const realmId = props.match?.params?.realmId;

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value, files} = event.target;
        const file = files!![0];
        console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: file});
    };

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        const data = new FormData();
        data.append('file', formValues.file);
        const response = personApi.importFile(realmId, data);
        console.log("Response:" + JSON.stringify(response));
    };

    return (
        <React.Fragment>
            {t("Import Persons")}
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    Input: <input
                        // accept="image/*"
                        className={classes.input}
                        // style={{ display: 'none' }}
                        id="file" name="file"
                        type="file"
                        multiple
                        onChange={handleInputChange}
                    />
                    <label htmlFor="raised-button-file">
                        <Button  onClick={handleSubmit}>
                            Upload
                        </Button>
                    </label>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default withRouter(PersonsAdmin);
