import React, {useEffect, useState} from 'react';
import {generatePath, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import {FaRegEdit} from 'react-icons/fa';

import {defaultPerson, Person} from "./Person";
import * as personApi from './person-api';
import {useTranslation} from "react-i18next";
import Paper from "@material-ui/core/Paper";



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            textAlign: 'right',
            color: theme.palette.text.secondary,
        },
        dataCell: {
            padding: theme.spacing(3),
            width: '100%',
            // color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        }
    }),
);

type PersonDetailsProps = {
    // using `interface` is also ok
    realmId: string;
    person: Person;
};

const PersonDetails = (props: PersonDetailsProps) => {
    const classes = useStyles();
    const { t } = useTranslation("person");

    const {realmId, person} = props;
    const dependents: Array<Person> = person.dependents ? person.dependents : new Array<Person>();

    return (
        <React.Fragment>
            <a href={generatePath("/:realmId/persons/:uid/edit", { realmId, uid: person.uid ? person.uid : "0" })}><FaRegEdit/></a>
            <h2>{t("Person Details")} - {person.familyName}, {person.givenName}</h2>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Name")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.familyName} {person.givenName}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Korean Name")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.nlFamilyName} {person.nlGivenName}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Baptismal Name")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.baptismalName}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Gender")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.gender}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Birth Date")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.birthDate}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Email")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.email}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Mobile Number")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.mobileNumber}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Address")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.addressLine}
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("City")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.addressCity}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    {t("Tags")}
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {person.tags.join(", ")}
                </Grid>
            </Grid>

            <h2>{t("Family Members")}</h2>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Name')}</TableCell>
                            <TableCell>{t('Baptismal Name')}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
            {dependents.map((record, idx) => {
                    return (
                        <TableRow key={record.uid}>
                            <TableCell>{record.familyName}, {record.givenName}</TableCell>
                            <TableCell>{record.baptismalName}</TableCell>
                        </TableRow>
                    )
                }
            )}
                    </TableBody>
                </Table>
            </TableContainer>

        </React.Fragment>
    );
};

const PersonDetailsContainer = (props: any) => {

    const [person, setPerson] = useState(defaultPerson);

    const realmId = props.match?.params?.realmId;

    const personUid = props.match?.params?.personUid;

    useEffect(() => {
        const fetchData = async (personUid: string) => {
            try {
                const result = await personApi.fetchOne(realmId, personUid);
                if (result) {
                    setPerson(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        if (personUid) {
            fetchData(personUid);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    // When: empty array --> causes `React Hook useEffect has a missing dependency: 'personUid'. Either include it or remove the dependency array`
    // When: [personUid] in the dependency --> it won't load when page is reloaded
    // When: no dependency --> The client will call indifenitely

    return (
        <PersonDetails realmId={realmId} person={person}/>
    );
};

export default withRouter(PersonDetailsContainer);
