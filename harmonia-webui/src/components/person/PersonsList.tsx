import React, {ChangeEvent, useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Pagination from '@material-ui/lab/Pagination';

import {FaMinusCircle, FaPlusCircle, FaRegEdit} from 'react-icons/fa';

import {useTranslation} from 'react-i18next';
import '../../framework/i18n'

import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import NativeSelect from '@material-ui/core/NativeSelect';
import {initPageInfo, Pageable} from '../../framework/paging-util';
import {newPerson, Person} from "./Person";
import * as personApi from './person-api';
import {generatePath, withRouter} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    actionIcon: {
        margin: 2
    },
    paging: {
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export function PersonsList(props: any) {
    const classes = useStyles();
    const {t /*, i18n*/} = useTranslation("person");

    // Filtering
    const [filterCriteria, setFilterCriteria] = useState<Person>(newPerson());
    const handleFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleFilterChange] name:" + name + ", value:" + value);
        setFilterCriteria({...filterCriteria, [name]: value});
    };
    const handleArrFilterChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleArrFilterChange] name:" + name + ", value:" + value);
        let arrVal = [value];
        if (value.trim().length === 0)
            arrVal = [];
        setFilterCriteria({...filterCriteria, [name]: arrVal});
    };

    const handleClearFilter = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        setFilterCriteria(newPerson());
        fetchList({page: 1, size: 20});
    };
    const handleFilter = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        // alert("Event:" + JSON.stringify(eventDetails,null, null, 2));
        fetchList({page: 1, size: 20});
    };

    // Pagination
    const [rows, setRows] = useState<Array<Person>>([]);
    const [pageInfo, setPageInfo] = useState<typeof initPageInfo>();

    // TODO: Factor out the paging range into a component
    // The current page number
    const [paging, setPaging] = React.useState({page: 1, size: 20});
    const handlePageChange = (event: any, value: any) => {
        const newPaging = {...paging, page: value};
        setPaging(newPaging);
        fetchList(newPaging);
    };
    const handlePageSizeChange = (event: ChangeEvent<HTMLSelectElement>) => {
        const newPaging = {...paging, size: Number(event.target.value)};
        setPaging(newPaging);
        fetchList(newPaging);
    };

    const realmId = props.match?.params?.realmId;

    async function fetchList(pageable: Pageable) {
        try {
            const result = await personApi.fetchList(realmId, filterCriteria, pageable);
            if (result) {
                setPageInfo(result?.page);
                if (result._embedded) {
                    setRows(result._embedded.persons)
                } else {
                    setRows([])
                }
            }
        } catch (error) {
            console.log(JSON.stringify(error, null, 2));
        }
    }

    useEffect(() => {
        fetchList(paging);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleDelete = (uid: string | undefined) => (event: React.MouseEvent<HTMLButtonElement>) => {
        async function deleteOne(uid: string) {
            try {
                const result = await personApi.deleteOne(realmId, uid);
                console.log(result)
                fetchList(paging);
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        }

        if (uid) {
            deleteOne(uid);
        }
    };

    const renderPagination = () => {
        return (<Box display="flex" flexDirection="row" p={1} m={1} justifyContent="center" >
            <Box component="div" display="inline">
                <Pagination count={pageInfo?.totalPages} page={paging.page}
                            onChange={handlePageChange}/>
            </Box>
            <Box component="div" display="inline" style={{marginLeft: "3em"}}>
                <FormControl /*className={classes.formControl}*/>
                    <NativeSelect
                        // className={classes.selectEmpty}
                        value={pageInfo?.size}
                        name="age"
                        onChange={handlePageSizeChange}
                        inputProps={{'aria-label': 'age'}}
                    >
                        <option value="" disabled>
                            {t("Size")}
                        </option>
                        <option value={10}>10</option>
                        <option value={20}>20</option>
                        <option value={50}>50</option>
                    </NativeSelect>
                    <FormHelperText>Page Size</FormHelperText>
                </FormControl>
            </Box>
        </Box>
        )
    };

    const renderFilterForm = () => {
        return (<div>
            <Grid container spacing={2}>
                <Grid item xs={2} >
                    <TextField
                        id="familyName" name="familyName" label={t("Family Name")}
                        value={filterCriteria.familyName}
                        onChange={handleFilterChange}
                    />
                </Grid>
                <Grid item xs={2} >
                    <TextField
                        id="givenName" name="givenName" label={t("Given Name")}
                        value={filterCriteria.givenName}
                        onChange={handleFilterChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={2} >
                    <TextField
                        id="baptismalName" name="baptismalName" label={t("Baptismal Name")}
                        value={filterCriteria.baptismalName}
                        onChange={handleFilterChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={2} >
                    <TextField
                        id="tags" name="tags" label={t("Tags")}
                        value={filterCriteria.tags}
                        onChange={handleArrFilterChange}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={2} >
                    <ButtonGroup size="small" aria-label="small button group">
                        <Button onClick={handleClearFilter} >{ t("app:Clear")}</Button>
                        <Button color="primary" onClick={handleFilter} >{ t("app:Filter")}</Button>
                    </ButtonGroup>
                </Grid>
            </Grid>
        </div>);
    }

    return (
        <div>
            <a href={generatePath("/:realmId/persons/new", {realmId})}><FaPlusCircle/>{t('Add New')}</a>

            {renderFilterForm()}
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Family Name')}</TableCell>
                            <TableCell>{t('Given Name')}</TableCell>
                            <TableCell>{t('Baptismal Name')}</TableCell>
                            <TableCell>{t('City')}</TableCell>
                            <TableCell>{t('Tags')}</TableCell>
                            <TableCell>{t('Actions')}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.uid}>
                                <TableCell component="th" scope="row">
                                    <a href={generatePath("/:realmId/persons/:uid/details", {
                                        realmId,
                                        uid: row.uid
                                    })}>{row.familyName}</a>
                                </TableCell>
                                <TableCell>{row.givenName}</TableCell>
                                <TableCell>{row.baptismalName}</TableCell>
                                <TableCell>{row.addressCity}</TableCell>
                                <TableCell>{row.tags.join(", ")}</TableCell>
                                <TableCell>
                                    <span className={classes.actionIcon}><a
                                        href={generatePath("/:realmId/persons/:uid/edit", {
                                            realmId,
                                            uid: row.uid
                                        })}><FaRegEdit/></a></span>
                                    <span className={classes.actionIcon}><button
                                        onClick={handleDelete(row.uid)}><FaMinusCircle/></button></span>
                                </TableCell>
                            </TableRow>
                        ))}
                        <TableRow>
                            <TableCell colSpan={5} >
                                {/* TODO: Factor out the paging pange into a component*/}
                                {
                                    (pageInfo) ? renderPagination() : ""
                                }
                            </TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </TableContainer>

            {JSON.stringify(pageInfo)}
        </div>
    );
}

export default withRouter(PersonsList);