import React from 'react';

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import {FaMinusCircle} from 'react-icons/fa';
import {Person, newPerson} from "./Person";
import {useTranslation} from "react-i18next";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        iconDisabled: {
            color: "red",
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
    }),
);

type RecordChangeEvent = {
    "target": {
        "name": number,
        "value": Person
    }
}

type FamilyMemberInputProps = {
    // using `interface` is also ok
    idx: number;
    record: Person;
    onChange(event: RecordChangeEvent): void;
    onAction(idx: number, action: string): void;
};

const FamilyMemberInput = (props: FamilyMemberInputProps) => {
    const classes = useStyles();

    const { t } = useTranslation("person");

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let record = { ...props.record, [event.target.name]: event.target.value };
        // The event object passed to the callback has this structure to keep consistency with DOM model
        // So that the parent container can handle in a consistent way. See EventForm's handleInputChange()
        let myEvent = {
            "target": {
                "name": props.idx,
                "value": record
            }
        };

        props.onChange(myEvent);
    };

    return (
        <Grid container spacing={2}>
            <Grid item xs={3} >
                <TextField
                    id="familyName" name="familyName" label={t("Family Name")}
                    value={props.record.familyName}
                    onChange={handleInputChange}
                />
            </Grid>
            <Grid item xs={3} >
                <TextField
                    id="givenName" name="givenName" label={t("Given Name")}
                    value={props.record.givenName}
                    onChange={handleInputChange}
                    fullWidth
                />
            </Grid>
            <Grid item xs={3} >
                <TextField
                    id="baptismalName" name="baptismalName" label={t("Baptismal Name")}
                    value={props.record.baptismalName}
                    onChange={handleInputChange}
                    fullWidth
                />
            </Grid>
            <Grid item >
                {
                    (props.record._action !== 'd') ?
                        <FaMinusCircle onClick={() => props.onAction(props.idx,  'd')} /> :
                        <FaMinusCircle className={classes.iconDisabled} onClick={() => props.onAction(props.idx,  '')} />
                }
            </Grid>
        </Grid>
    )
}

export type RecordsChangeEvent = {
    "target": {
        "name": string,
        "value": Array<Person>
    }
}

type FamilyMembersFormProps = {
    id: string;
    name: string;
    records: Array<Person> | undefined;
    onChange(event: RecordsChangeEvent): void;
};

const FamilyMembersForm = (props: FamilyMembersFormProps) => {
    const {t} = useTranslation("person");

    const records: Array<Person> = props.records ? props.records : new Array<Person>();

    const propagateChangeEvent = (records: Array<Person>) => {
        let myEvent = {
            "target": {
                "name": props.name,
                "value": records
            }
        };

        props.onChange(myEvent);
    }

    const handleInputChange = (event: RecordChangeEvent) => {
        // Value represents a row
        let updatedRecords = records.map((record, idx) =>
            idx === event.target.name ? { ...event.target.value } : record
        )
        propagateChangeEvent(updatedRecords);
    };

    const handleAction = (selectedRec: number, action: string) => {
        let updatedRecords = records.map((record, idx) =>
            idx === selectedRec ? { ...record, _action: action } : record
        )
        propagateChangeEvent(updatedRecords);
    }

    const addRow = () => {
        let newRecord = newPerson();
        let updatedRecords : Array<Person>= [...records, newRecord];
        propagateChangeEvent(updatedRecords);
    }

    return (
        <div  >
            {records.map((record, idx) => {
                    let resourceWithIdx = { ...record, "_idx": idx }
                    return (
                        <div key={idx} >
                            <FamilyMemberInput idx={idx} record={resourceWithIdx}
                                               onChange={handleInputChange}
                                               onAction={handleAction} />
                        </div>
                    )
                }
            )}
            <Grid container spacing={2}>
                <Grid item xs>
                    <Button onClick={addRow} color="primary">
                        {t('Add Member')}
                    </Button>
                </Grid>
            </Grid>

        </div>
    )
}

export default FamilyMembersForm;