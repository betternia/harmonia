import React, {useEffect, useState} from 'react';
import {generatePath, withRouter, Link} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';

import ChipInput from 'material-ui-chip-input'

import {newPerson, Person} from "./Person";
import * as personApi from './person-api';
import {useTranslation} from "react-i18next";

import FamilyMembersForm, {RecordsChangeEvent as MembersChangeEvent} from './FamilyMembersForm'
import {FaRegIdCard} from "react-icons/fa";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
    }),
);

type PersonFormProps = {
    // using `interface` is also ok
    realmId: string;
    person: Person;
    onSubmit(person: Person): void;
};

const PersonForm = (props: PersonFormProps) => {
    const classes = useStyles();
    const { t } = useTranslation("person");

    const {realmId, person, onSubmit} = props;

    const [formValues, setFormValues] = useState<Person>(person);
    // console.log("[PersonForm] person: " + JSON.stringify(person, null, 2));
    // console.log("[PersonForm] formValues: " + JSON.stringify(formValues, null, 2));

    useEffect(() => {
        const fetchData = async (personUid: string) => {
            try {
                const result = await personApi.fetchOne(realmId, personUid);
                if (result) {
                    setFormValues(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        // TODO: check if other values are set, not need to fetch again.
        if (person.uid) {
            fetchData(person.uid);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        // alert("Event:" + JSON.stringify(eventDetails,null, null, 2));
        onSubmit(formValues)
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    const handleMembersChange = (event: MembersChangeEvent) => {
        const {name, value} = event.target;
        // console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    const handleSelectChange = (name: string) => (event: React.ChangeEvent<{ value: unknown }>) => {
        const {value} = event.target;
        // console.log("[handleSelectChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    const handleAddTag = (name: string, tag: string) => {
        setFormValues({...formValues, [name]: [...formValues.tags, tag]});
    }

    const handleDeleteTag = (name: string, tag: string, index: number) => {
        let newTags = formValues.tags.slice();
        newTags.splice(index, 1);
        setFormValues({...formValues, [name]: newTags});
    }

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <h2>{t("Person Details")} - {formValues.familyName}, {formValues.givenName}</h2>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TextField id="familyName" name="familyName" label={t("Family Name")}
                               value={formValues.familyName}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="givenName" name="givenName" label={t("Given Name")}
                               value={formValues.givenName}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.nlFamilyName" name="nlFamilyName" label={t("Native Family Name")} value={formValues.nlFamilyName}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="person.nlGivenName" name="nlGivenName" label={t("Native Given Name")} value={formValues.nlGivenName}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.baptismalName" name="baptismalName" label={t("Baptismal Name")} value={formValues.baptismalName}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="person.headline" name="headline" label={t("Headline")} value={formValues.headline}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <FormControl className={classes.formControl}  fullWidth>
                        <InputLabel id="person-gender-label">{t("Gender")}</InputLabel>
                        <Select
                            labelId="person-gender-label"
                            id="person.gender" name="gender"
                            value={formValues.gender}
                            onChange={handleSelectChange('gender')}
                        >
                            <MenuItem value={"M"}>{t("Male")}</MenuItem>
                            <MenuItem value={"F"}>{t("Female")}</MenuItem>
                            <MenuItem value={"U"}>{t("Undisclosed")}</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.birthDate" name="birthDate" label={t("Birth Date")} value={formValues.birthDate}
                               type="date"
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                {/* Contact */}
                <Grid item xs={12} sm={6}>
                    <TextField id="person.mobileNumber" name="mobileNumber" label={t("Mobile Number")} value={formValues.mobileNumber}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.phoneNumber" name="phoneNumber" label={t("Phone Number")} value={formValues.phoneNumber}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="person.email" name="email" label={t("Email")} value={formValues.email}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>
                <Grid item xs={12} sm={6}>

                </Grid>
            </Grid>

            <Grid container spacing={2}>
                {/* Address */}
                <Grid item xs={12} >
                    <TextField id="person.addressLine" name="addressLine" label={t("Address Line")} value={formValues.addressLine}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.addressCity" name="addressCity" label={t("City")} value={formValues.addressCity}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.addressState" name="addressState" label={t("State")} value={formValues.addressState}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="person.addressZip" name="addressZip" label={t("Zip")} value={formValues.addressZip}
                               onChange={handleInputChange} fullWidth
                               />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <ChipInput
                        label={t("Tags")}
                        value={formValues.tags}
                        onAdd={(chip) => handleAddTag("tags", chip)}
                        onDelete={(chip, index) => handleDeleteTag("tags", chip, index)}
                    />
                </Grid>

            </Grid>

            <h2>{t("Family Members")}</h2>
            <Grid container spacing={2} >
                {/* Family Members */}
                <Grid item xs={12} >
                    <Grid item xs>
                        <FamilyMembersForm id="person.dependents" name="dependents" records={formValues.dependents}
                                           onChange={handleMembersChange} />
                    </Grid>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} >
                    <Button variant="contained" color="primary" onClick={handleSubmit}  >
                        { (person.uid) ? t("app:Update") : t("app:Add")}
                    </Button>

                    <span style={ {marginLeft: "10px"} }>
                    <Link to={generatePath("/:realmId/persons/:uid/details", { realmId, uid: person.uid ? person.uid : "0" })} >
                        <Button variant="contained"  >
                            <FaRegIdCard/> {t('app:Back to Details')}
                        </Button>
                    </Link>

                    <Link to={generatePath("/:realmId/persons", { realmId })}>
                        <Button variant="contained"  >
                            {t('app:Back to List')}
                        </Button>
                    </Link>
                    </span>

                </Grid>
            </Grid>
        </form>
    );
};

const PersonFormContainer = (props: any) => {

    // props.match populated by withRouter()
    const realmId = props.match?.params?.realmId;
    const person = newPerson(props.match?.params?.personUid);

    const updatePerson = (person: Person) => {
        console.debug("Updating Person: " + JSON.stringify(person));
        const response = personApi.update(realmId, person);
        console.log("Updated Person: " + JSON.stringify(response));

        // TODO: update state instead of reload
        window.location.reload();
    };

    const addPerson = (person: Person) => {
        console.debug("Add Person: " + JSON.stringify(person));
        const response = personApi.create(realmId, person);
        console.log("Added Person: " + JSON.stringify(response));
    };

    return (
        <PersonForm realmId={realmId} person={person} onSubmit={(person.uid) ? updatePerson : addPerson}/>
    );
};

export default withRouter(PersonFormContainer)

