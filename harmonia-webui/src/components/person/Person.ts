import {Moment} from 'moment';

export interface Person {
    uid?: string;
    createdBy?: string;
    createdDate?: Moment;
    lastModifiedBy?: string;
    lastModifiedDate?: Moment;

    userUid?: string;
    headline?: string;
    synopsis?: string;
    domain?: string;
    hometown?: string;
    gender?: string;
    givenName?: string;
    familyName?: string;
    nlGivenName?: string;
    nlFamilyName?: string;
    alternateName?: string;
    baptismalName?: string;
    birthDate?: Moment;
    birthPlace?: string;
    email?: string;
    phoneNumber?: string;
    mobileNumber?: string;
    addressLine?: string;
    addressStreet?: string;
    addressCity?: string;
    addressState?: string;
    addressZip?: string;

    dependents?: Person[];
    headOfFamilyUid?: string;

    tags: string[];

    _action?: string;
}

export function newPerson(uid: string | undefined = undefined): Person {
    return {
        uid: uid,
        userUid: "",
        headline: "",
        synopsis: "",
        domain: "",
        hometown: "",
        gender: "",
        givenName: "", // required
        familyName: "",
        nlGivenName: "",
        nlFamilyName: "",
        alternateName: "",
        baptismalName: "",
        birthDate: undefined,
        birthPlace: "",
        email: "",
        phoneNumber: "",
        mobileNumber: "",
        addressLine: "",
        addressStreet: "",
        addressCity: "",
        addressState: "",
        addressZip: "",

        dependents: [],
        headOfFamilyUid: undefined,

        tags: [],

        // E.g.: 'delete', 'archive'
        _action: ""
    };
}

export const defaultPerson: Readonly<Person> = {tags: []};
