import {createApiClient, RemoteError} from "../../framework/apiclient-util";
import {Person} from './Person';
import {Pageable, generatePagingQueryString, generateCriteriaQueryString} from '../../framework/paging-util';

const apiClient = createApiClient("/api/persons");

export const fetchList = async (realmId: string, queryParams?: any, pageable?: Pageable) => {
    const pagingQs = generatePagingQueryString(pageable);

    try {
        let criteriaQs = generateCriteriaQueryString(queryParams);
        const response = await apiClient.get(`?realmId=${realmId}${criteriaQs}${pagingQs}`);
        return response.data;
    } catch (error) {
        if (error && error.response) {
            const remoteError = error as RemoteError;
            return remoteError.response?.data;
        }
        throw error;
    }
};

export const fetchOne = async (realmId: string, uid: string) => {
    const response = await apiClient.get<Person>(`/${uid}?realmId=${realmId}`);
    return response.data;
};

export const create = async (realmId: string, person: Person) => {
    const response = await apiClient.post<Person>(`/?realmId=${realmId}`, person);
    return response.data;
};

export const update = async (realmId: string, person: Person) => {
    const response = await apiClient.put<Person>(`/${person.uid}?realmId=${realmId}`, person);
    return response.data;
};

export const deleteOne = async (realmId: string, uid: string) => {
    const response = await apiClient.delete<Person>(`/${uid}?realmId=${realmId}`);
    return response.data;
};


export const importFile = async (realmId: string, data: any) => {
    const response = await apiClient.post<Person>(`/import?realmId=${realmId}`, data);
    return response.data;
};