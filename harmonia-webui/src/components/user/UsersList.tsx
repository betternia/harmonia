import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import { FaPlusCircle, FaMinusCircle, FaRegEdit } from 'react-icons/fa';

import { useTranslation } from 'react-i18next';
import '../../framework/i18n'

import {User} from "./User";
import * as userApi from './user-api';
import {generatePath, withRouter} from "react-router-dom";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    actionIcon: {
        margin: 2
    }
});

const initPageInfo = {
    "size": 20,
    "totalElements": 0,
    "totalPages": 0,
    "number": 0
}

export function UsersList(props: any) {
    const classes = useStyles();
    const { t } = useTranslation("user");

    const [rows, setRows] = useState<Array<User>>([]);
    const [pageInfo, setPageInfo] = useState<typeof initPageInfo>(initPageInfo);

    const realmId = props.match?.params?.realmId;

    async function fetchList() {
        try {
            const result = await userApi.fetchList(null);
            if (result) {
                setPageInfo(result?.page);
                setRows(result._embedded.users)
            }
        } catch (error) {
            const errorMessage = JSON.stringify(error, null, 2);
            console.error(errorMessage);
            // TODO: Error boundary
            alert ("Error: " + error.toString());
        }
    }

    useEffect(() => {
        fetchList();
    }, []);

    const handleDelete = (uid: string | undefined) => (event: React.MouseEvent<HTMLButtonElement>) => {
        async function deleteOne(uid: string) {
            try {
                const result = await userApi.deleteOne(uid);
                console.log(result)
                fetchList();
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        }

        if (uid) {
            deleteOne(uid);
        }
    };

    return (
        <div>
            <a href={generatePath("/:realmId/users/new", { realmId })}><FaPlusCircle />{t('Add New')}</a>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>{t('Username')}</TableCell>
                            <TableCell>{t('Given Name')}</TableCell>
                            <TableCell>{t('Family Name')}</TableCell>
                            <TableCell>{t('Email')}</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.uid}>
                                <TableCell component="th" scope="row">
                                    <a href={generatePath("/:realmId/users/:uid/details", { realmId, uid: row.uid })}>{row.userName}</a>
                                </TableCell>
                                <TableCell>{row.firstName}</TableCell>
                                <TableCell>{row.lastName}</TableCell>
                                <TableCell>{row.email}</TableCell>
                                <TableCell>
                                    <span className={classes.actionIcon}><a href={generatePath("/:realmId/users/:uid/edit", { realmId, uid: row.uid })}><FaRegEdit /></a></span>
                                    <span className={classes.actionIcon}><button onClick={handleDelete(row.uid)}><FaMinusCircle /></button></span>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            { JSON.stringify(pageInfo) }
        </div>
    );
}

export default withRouter(UsersList);