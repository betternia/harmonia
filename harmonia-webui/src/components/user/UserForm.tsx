import React, {useEffect, useState} from 'react';
import {withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme, useTheme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';

import {newUser, User} from "./User";
import * as userApi from './user-api';
import {useTranslation} from "react-i18next";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: 2,
        }
    }),
);

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

type UserFormProps = {
    // using `interface` is also ok
    user: User;
    onSubmit(user: User): void;
};

const authorities = [
    'ROLE_ADMIN',
    'ROLE_USER'
];

function getStyles(name: string, list: string[], theme: Theme) {
    return {
        fontWeight:
            list.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

const UserForm = (props: UserFormProps) => {
    const classes = useStyles();
    const theme = useTheme();

    const {t} = useTranslation("app");

    const {user, onSubmit} = props;

    const [formValues, setFormValues] = useState<User>(user);
    // console.log("[UserForm] user: " + JSON.stringify(user, null, 2));
    // console.log("[UserForm] formValues: " + JSON.stringify(formValues, null, 2));

    useEffect(() => {
        const fetchData = async (userUid: string) => {
            try {
                const result = await userApi.fetchOne(userUid);
                if (result) {
                    setFormValues(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        // TODO: check if other values are set, not need to fetch again.
        if (user.uid) {
            fetchData(user.uid);
        }

    }, [user.uid]);

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement>) => {
        if (event) event.preventDefault();
        // alert("Event:" + JSON.stringify(eventDetails,null, null, 2));
        onSubmit(formValues)
    };

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = event.target;
        // console.log("[handleInputChange] name:" + name + ", value:" + value);
        setFormValues({...formValues, [name]: value});
    };

    const handleChangeMultiple = (name: string) => (event: React.ChangeEvent<{ value: unknown }>) => {
        const value = event.target.value as string[];
        setFormValues({...formValues, [name]: value});
    };

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                    <TextField id="lastName" name="lastName" label={t("Family Name")} value={formValues.lastName}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="firstName" name="firstName" label="Given Name" value={formValues.firstName}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField id="user.userName" name="userName" label="Username" value={formValues.userName}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="user.email" name="email" label="Email" value={formValues.email}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField id="user.langKey" name="langKey" label="Language" value={formValues.langKey}
                               onChange={handleInputChange} fullWidth
                               InputLabelProps={{
                                   shrink: true,
                               }}/>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="authorities-label">Authorities</InputLabel>
                        <Select
                            labelId="authorities-label"
                            id="authorities-chip"
                            multiple
                            value={formValues.authorities}
                            onChange={handleChangeMultiple('authorities')}
                            input={<Input id="authorities-chip"/>}
                            renderValue={selected => (
                                <div className={classes.chips}>
                                    {(selected as string[]).map(value => (
                                        <Chip key={value} label={value} className={classes.chip}/>
                                    ))}
                                </div>
                            )}
                            MenuProps={MenuProps}
                        >
                            {authorities.map(name => (
                                <MenuItem key={name} value={name}
                                          style={getStyles(name, formValues.authorities!!, theme)}>
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={handleSubmit}>
                        {(user.uid) ? "Update" : "Add"}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

const UserFormContainer = (props: any) => {

    // props.match populated by withRouter()
    const user = newUser(props.match?.params?.userUid);

    const updateUser = (user: User) => {
        console.debug("Updating User: " + JSON.stringify(user));
        const response = userApi.update(user);
        console.log("Updated User: " + JSON.stringify(response));
    };

    const addUser = (user: User) => {
        console.debug("Add User: " + JSON.stringify(user));
        const response = userApi.create(user);
        console.log("Added User: " + JSON.stringify(response));
    };

    return (
        <UserForm user={user} onSubmit={(user.uid) ? updateUser : addUser}/>
    );
};

export default withRouter(UserFormContainer)

