import {ModelBase} from "../../framework/ModelBase"

export interface User extends ModelBase {

    userName?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    activated?: boolean;
    langKey?: string;
    authorities?: string[];

    password?: string;
}

export function newUser(uid: string | undefined = undefined): User {
    return {
        uid: uid,
        createdBy: undefined,
        createdDate: undefined,
        lastModifiedBy: undefined,
        lastModifiedDate: undefined,

        userName: '',
        firstName: '',
        lastName: '',
        email: '',
        activated: false,
        langKey: '',
        authorities: [],
        password: ''
    };
};
