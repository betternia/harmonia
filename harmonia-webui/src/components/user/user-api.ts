import {createApiClient, RemoteError} from '../../framework/apiclient-util';
import {User} from './User';

const apiClient = createApiClient("/api/users");

export const fetchList = async (queryParams: any) => {
    try {
        const response = await apiClient.get('');
        return response.data;
    } catch (error) {
        if (error && error.response) {
            const remoteError = error as RemoteError;
            return remoteError.response?.data;
        }
        throw error;
    }
};

export const fetchOne = async (uid: string) => {
    const response = await apiClient.get<User>('/' + uid);
    return response.data;
};

export const create = async (person: User) => {
    const response = await apiClient.post<User>('/', person);
    return response.data;
};

export const update = async (person: User) => {
    const response = await apiClient.put<User>('/' + person.uid, person);
    return response.data;
};

export const deleteOne = async (uid: string) => {
    const response = await apiClient.delete<User>('/' + uid);
    return response.data;
};