import React, {useEffect, useState} from 'react';
import {generatePath, withRouter} from "react-router-dom";

import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import {FaRegEdit} from 'react-icons/fa';

import {newUser, User} from "./User";
import * as userApi from './user-api';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        labelCell: {
            textAlign: 'right',
            color: theme.palette.text.secondary,
        },
        dataCell: {
            padding: theme.spacing(3),
            // color: theme.palette.text.secondary,
            backgroundColor: theme.palette.background.paper
        }
    }),
);

type UserDetailsProps = {
    // using `interface` is also ok
    realmId: string;
    user: User;
};

const UserDetails = (props: UserDetailsProps) => {
    const classes = useStyles();

    const {realmId, user} = props;

    return (
        <React.Fragment>
            <a href={generatePath("/:realmId/users/:uid/edit", { realmId, uid: user.uid ? user.uid : "0" })}><FaRegEdit/></a>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Name
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.lastName} {user.firstName}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Email
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.email}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Authorities
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.authorities}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Email
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.email}
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Created at
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.createdDate}
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} className={classes.labelCell}>
                    Modified at
                </Grid>
                <Grid item xs={12} sm={6} className={classes.dataCell}>
                    {user.lastModifiedDate}
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

const UserDetailsContainer = (props: any) => {

    const [user, setUser] = useState(newUser());

    const realmId = props.match?.params?.realmId;

    const userUid = props.match?.params?.userUid;

    useEffect(() => {
        const fetchData = async (userUid: string) => {
            try {
                const result = await userApi.fetchOne(userUid);
                if (result) {
                    setUser(result);
                }
            } catch (error) {
                console.log(JSON.stringify(error, null, 2));
            }
        };
        if (userUid) {
            fetchData(userUid);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <UserDetails realmId={realmId} user={user}/>
    );
};

export default withRouter(UserDetailsContainer);

