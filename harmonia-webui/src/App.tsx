import React from 'react';

import AppFrame from "./layout/AppFrame";
import routes from "./routes";
import {BrowserRouter as Router} from "react-router-dom";

function App() {
    return (
        <Router>
            <AppFrame routes={routes}>
            </AppFrame>
        </Router>
    );
}

export default App;
