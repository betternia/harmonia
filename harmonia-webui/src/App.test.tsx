import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import createStore from './store';
import {Provider} from "react-redux";

test('renders side menu links: users, persons, organizations', () => {

  const store = createStore()

  const { container } = render(<Provider store={store}><App /></Provider>);
  // const linkElement = getByText(/organization/i);

  ["/users", "/persons", "/organizations"].forEach( (path, idx) => {
    const link = container.querySelector('[href="'+path+'"]');
    expect(link).toBeInTheDocument();  
  })

  // const userLink = container.querySelector('[href="/users"]');
  // expect(userLink).toBeInTheDocument();

  // const personsLink = container.querySelector('[href="/persons"]');
  // expect(personsLink).toBeInTheDocument();

  // const organizationLink = container.querySelector('[href="/organizations"]');
  // expect(organizationLink).toBeInTheDocument();
  
});
