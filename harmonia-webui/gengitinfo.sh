#!/bin/bash

# https://stackoverflow.com/questions/48391897/add-git-information-to-create-react-app
GIT_SHA=$(git rev-parse --short HEAD)

# https://stackoverflow.com/questions/25563455/how-do-i-get-last-commit-date-from-git-repository/51403241
GIT_COMMIT_DATE=$(git --no-pager log -1 --format="%ai")

echo "{\"git-sha\": \"$GIT_SHA\", \"git-commit-date\": \"$GIT_COMMIT_DATE\"}" > build/gitinfo.json
