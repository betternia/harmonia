Harmonia Community Support System
=================================

A web-based application to support the management of community, including members,
sub-groups and activities.

For more documentation, go to [documentation](./docs/index.md)