<h1>Reference</h1>

## Persons API
The person's API provides endpoints to list, add, update, and delete person entries.
 
Base URL: `/api/persons`

### GET /persons 
```
/api/persons[?queryParams]
```
Where query params:
- `givenName`
- `familyName`

Sample Request:
```console
$ curl http://localhost:8080/api/persons
```
Sample Response:
```yaml
{
  "_embedded": {
    "persons": [
      {
        "uid": "1",
        "userUid": "USER-UID-01",
        "headline": "Seasoned Engineer",
        "synopsis": "20+ years of hands-on experience",
        "domain": "Software Engineering",
        "hometown": "Softwarenia",
        "gender": "M",
        "givenName": "Albert",
        "familyName": "Smith",
        "nlGivenName": "ìë²",
        "nlFamilyName": "ì ",
        "baptismalName": "Daniel",
        "birthDate": "1970-03-01",
        "birthPlace": "Veremia",
        "email": "albert.smith@test.com",
        "phoneNumber": "800-234-1234",
        "mobileNumber": "512-234-1234",
        "addressLine": "5000 Johnston Rd.",
        "addressStreet": "Smithshall",
        "addressCity": "Los Bosques",
        "addressState": "Galifornia",
        "addressZip": "98909",
        "headOfFamilyUid": null,
        "_links": {
          "self": {
            "href": "http://localhost/api/persons/3"
          },
          "persons": {
            "href": "http://localhost/api/persons/"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost/api/persons/"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 10,
    "totalPages": 1,
    "number": 0
  }
}
```

### POST /persons