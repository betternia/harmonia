<h1>Harmonia</h1>
The `Harmonia` project is a simple community support system.
The core feature is member management.

## Features
Feature in Progress:  

- Member management (CRUD)

Future Features

- Member Reporting
- Activity Tracking

## Technology Stack
- Language: Kotlin
- Server-side Framework: Spring
- Client-side Framework: React+Redux / Typescript
- UI/CSS Framework: [Material-UI](https://material-ui.com/)

### Initial Code Scaffolding
For the server-side, the base project was generated from [Spring Initializr](https://start.spring.io/).
Then most of the initial business code including config, controller, domain, security, service and 
web.rest were copy from [kHipster](https://github.com/jhipster/jhipster-kotlin) generated code.
Some modification to the original generated code include changing the entity classes to 
`data class` and using UUID as primary key. In addition, in some part of the code the use of
`Optional` were changed to Kotlin's null-safe `?`. Also the rest controller returns HATEOAS 
representation.

For the client-side, the base code was generated with Create React App (CRA) with typescript 
template. Initial Redux related code were copied from separate CRA generated code with redux 
template, then incrementally converted into typescript.


## Building and Installing
To build the application backend
```console
<project-root>$ ./gradlew assemble
```
Dependency was added to the`assemble` task so it runs `webuiInstall`.

To build webui independently, you can use 
```console
<project-root>$ ./gradlew webuiBuild
```
or
```console
<project-root>$ yarn --cwd ~/test_project/ dev
```
The `--cwd` allows to run yarn from different directory.

If you want to be able to run the application, backend and UI from IntelliJ, you will need to copy
the `harmony-webui/build` folder into `out/production/resources/public` the following command does 
just that:
```console
<project-root>$ ./gradlew webuiInstallIde
```

## Running Locally 
With the spring `dev` profile, the application will use the H2 memory db.

- Running with Gradle
```shell script
$ SPRING_PROFILES_ACTIVE=dev ./gradlew bootRun
```

- Testing
```shell script
$ curl http://localhost:8080/api/persons
```