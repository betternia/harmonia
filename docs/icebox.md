# Development

## Work

### Bootstrapping
Backend:
Started with spring initializer. Too


### Add icons
I attempted to install `@material-ui/icons` and `material-design-icon` but both failed with timeout
when using `yarn add`

Anyhow, I think `https://github.com/react-icons/react-icons` is better option as it includes many 
popular icons.

```console
yarn add react-icons
```

Using the icons in react:
```javascript
import { FaBeer } from 'react-icons/fa'; // fa is for FontAwesome
... <FaBeer />
```

### Integrating NPM/Yarn into gradle
The `com.moowork.node` is mentioned in few articles, but the project is redirected to:
`https://github.com/srs/gradle-node-plugin` which hasn't been updated since March 2019.  
There is this another project which seems like a fork from the prior project 
`https://github.com/node-gradle/gradle-node-plugin` but it looks more active.

There is this another plugwin that has not been active recently:
`https://github.com/palantir/gradle-npm-run-plugin` last update was Jan 2017.

To integrate with gradle, the static assets (the output of yarn/npm) would need to be copied to
`build/resources/main/static` folder.

According to this article `https://shekhargulati.com/2019/01/13/running-tests-and-building-react-applications-with-gradle-build-tool/`
it is possible to configure the gradle-node-plugin so that when `assemble` task is executed,

In local docker the Build process works fine, but in GitLab CI it fails with the following mesaage
```
Process 'command '/builds/betternia/harmonia/harmonia-webui/yarn-latest/bin/yarn'' finished with non-zero exit value 1
```

#### Testing in docker instance
Run gradle docker in interactive mode
```console
$ docker  run --rm -it --name test gradle bash
```

Install necessary ubuntu packages: nodejs, npm, yarn; and git clone repo
```console
apt update
# Following line from: https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/
curl -sL https://deb.nodesource.com/setup_13.x | bash -
apt-get install nodejs && npm install --global yarn
# apt install nodejs -y && apt install npm -y && npm install --global yarn
mkdir /opt/apps && cd "$_" && git clone https://gitlab.com/betternia/harmonia.git
cd harmonia
./gradlew webuiDepsInstall
./gradlew assemble
```

### Containerizing Gradle Project

Reference
- [Jib github](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin)

The dockerhub credentials were placed in `gradle.properties` (added to `.gitignore` to avoid being checked in).
[Dockerhub](https://hub.docker.com) provides free account. 

Building image directly to the docker daemon
```console
$ gradle jibDockerBuild
```
Then the image will be listed in docker
```console
docker image ls
```
And you should be able to run it:
```console
docker run -p 8080:8080 -e "SPRING_PROFILES_ACTIVE=dev" harmonia:0.0.4-SNAPSHOT
```

Building image to docker registry
```console
$ gradle jib
```
The above command saves you from having to
```console
$ docker login -u ${DOCKER_REG_USERNAME} -p ${DOCKER_REG_PASSWORD} your.docker-registry.com
$ docker build -t ${IMAGE} .
$ docker push ${IMAGE}
```



#### Adding git info to the /management/info
Add in `build.gradle.kts` 
[gradle-git-properties](https://plugins.gradle.org/plugin/com.gorylenko.gradle-git-properties) plugin

```kotlin
id("com.gorylenko.gradle-git-properties") version "2.2.2"
```

It will generate `build/resources/main/git.properties` file  

### UI Internationalization
From the multiple i18n JS libraries listed in [bestojs](https://bestofjs.org/projects?tags=i18n),
The four applicable candidates were (in the order of num stars):

- [formatjs](https://github.com/formatjs/react-intl) - Too heavy
- [i18next](https://www.i18next.com/) 
- [facebook/fbt](https://facebook.github.io/fbt/) - Requires babel setup
- [lingui.js](https://lingui.js.org/) 

Among those, `lingui` and `i18next` seems reasonable choice due to their small footprint and small 
learning curve. For the first iteration I chose `itnext` although base on this 
[article](https://medium.com/wantedly-engineering/introduce-multi-language-support-in-a-typescript-react-app-with-lingui-js-f335efcf80d6)
`lingui` looks good option because of typescript support.

### Authentication
jHipsters generated code provides a simple REST authentication endpoint.

For security purpose, it is recommended to store the token in cookie with `HttpOnly` cookie flag
(so to prevent JavaScripts from accessing it, thus avoid cross-site scripting (XSS)). In addition
`Secure` cookie flag guarantees the cookie is only sent over HTTPS.
Nonetheless cookies are vulnerable to cross-site request forgery (CSRF), which can be prevented by
synchronized token patterns.

The other less secure alternatives are
- sessionStorage - Data is available until the tab/window is closed. 
- localStorage - Data is available until manual/explicit deletion. 

As first iteration, the authentication will be implemented as follow:
1. Web client makes a REST request to /api/authenticate endpoint which on success returns
    returns idToken as body and `accessToken` in the header.
2. Web client stores it in the `localStorage`
3. Web client obtains the accessToken from localStorage and populates the `Authorization` header as Bearer token

### UI Unit Testing
The UI code is based on the code generated by Create React App, hence I am using the test tools that 
comes predefined. The [test framework used by CRA is Jest](https://create-react-app.dev/docs/running-tests/).
It also incldues [testing-library](https://testing-library.com/) to facilitate UI testing.

Running `yarn test` will run the test once and will continue watching for changes and trigger test.

#### Axios Testing
Axios is an HTTP client. There are few other alternatives including `fetch()` but axios was chosen
because of its ecosystem and flexible api (see reference below.

To uni test axios, I found multiple approaches including using nock, sinon, moxios, 
axios-mock-adapter, and jest.mock(). 
Personally the nock approach was most intuitive/easy-to-read. The one thing to notice is
setting `apiClient.defaults.adapter = httpAdapter;` where httpAdapter is from `import axios/lib/adapters/http`


References
- https://blog.logrocket.com/axios-or-fetch-api/

### Deploying
When running the UI mounted on API Server (SpringBoot) the REACT_APP_API_BASE_URL environment must
be empty.
When running the UI hosted separately from the API server, the REACT_APP_API_BASE_URL must be set 
accordingly.

```console
REACT_APP_API_BASE_URL=http://localhost:8080 yarn start
```

> In build time, the process.env.REACT_APP_BASE_URL is replaced with the string

#### Cloud Providers
There are various cloud providers from major players to lesser known.
For the deployment one major factor I am considering is the cost. It needs to have "Always Free" 
tier option. Below are the providers considered, along with the link to their respective pricing: 
- [AWS](https://aws.amazon.com/free/) - The EC2s are free for 12 months
- [Goole Cloud](https://cloud.google.com/free/docs/gcp-free-tier#always-free-usage-limits) - 
    The Compute Engine (f1-micro) is part of always free (yay)!!
- [Oracle Cloud](https://www.oracle.com/cloud/free/#always-free) - 2 virtual machines with 
    2GB ea (yay)!!
- [Heroku](https://www.heroku.com/free) - 1 Dyno Always Free, up to 512M (yay)!!
- [Azure](https://azure.microsoft.com/en-us/free/) - Virtual Machines are free fore 12 months.
- [IBM Cloud](https://www.ibm.com/cloud/free/) - Cloud Foundry is always free, but Cluster are 
    fee for a month.
- [OpenShift](https://www.openshift.com/products/online/) - Free for 2 months


Other with no free tiers: Rackspace, DigitalOcean.
  

#### Deploying to Heroku
Heroku expects a web application to bind its HTTP server to the port defined by the $PORT 
environment variable.
The `application-heroku-stg.yaml` includes `server.port: $PORT`.
At heroku application's settings add Config Var 
`SPRING_PROFILES_ACTIVE` with value `heroku-stg`

Heroku free allows up to 512M
https://devcenter.heroku.com/articles/java-support
JAVA_OPTIONS='-Xmx300m'

Since the docker image is built, we want to push it to heroku docker registry.

[Heroku Container Registry deploy doc](https://devcenter.heroku.com/articles/container-registry-and-runtime#logging-in-to-the-registry)
[Article on deploying using GitLag CI](https://medium.com/@nieldw/use-gitlab-ci-to-deploy-docker-images-to-heroku-4e544a3a3c38)
https://toedter.com/2018/06/02/heroku-docker-deployment-update/

```console
docker push registry.heroku.com/<app>/<process-type>
```
try: 
```console
docker login --username=_ --password=$(heroku auth:token) registry.heroku.com
./gradlew jibDockerBuild -Djib.to.image=harmonia:temp
docker tag harmonia:temp registry.heroku.com/harmonia-stg/web
docker push registry.heroku.com/harmonia-stg/web
heroku login
heroku container:release web -a harmonia-stg
heroku logs --tail -a harmonia-stg
```
> BLOCKER 2020/03/29 - RESOLVED (2020/03/30)
> The deployment to Heroku fail with following error:
> 20:06:11.769 [main] INFO  o.s.b.w.e.tomcat.TomcatWebServer - Tomcat started on port(s): 8080 (http) with context path ''
>  2020-03-29T20:06:11.771871+00:00 app[web.1]: 20:06:11.771 [main] INFO  d.b.harmonia.HarmoniaApplicationKt - Started HarmoniaApplicationKt in 29.68 seconds (JVM running for 30.858)
>  2020-03-29T20:06:38.986633+00:00 heroku[web.1]: State changed from starting to crashed
>  2020-03-29T20:06:38.847492+00:00 heroku[web.1]: Error R10 (Boot timeout) -> Web process failed to bind to $PORT within 60 seconds of launch
>  2020-03-29T20:06:38.847492+00:00 heroku[web.1]: Stopping process with SIGKILL
>  2020-03-29T20:06:38.965066+00:00 heroku[web.1]: Process exited with status 137

In GitLab CI
https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

On CI, react-script treats warning as error, hence CI stops with non zero exit:
> Treating warnings as errors because process.env.CI = true
https://github.com/facebook/create-react-app/issues/3657

### Import
Uploading with React: 
- https://www.geeksforgeeks.org/file-uploading-in-react-js/
- https://programmingwithmosh.com/javascript/react-file-upload-proper-server-side-nodejs-easy/

## Icebox
- Better structure for server-side route mapping of UI routes that enumerating each of the router 
in `UiRouterForwardController.kt`

- React [ErrorBoundary](https://reactjs.org/docs/error-boundaries.html)

- Footer: https://www.freecodecamp.org/news/how-to-keep-your-footer-where-it-belongs-59c6aa05c59c/