<h1>Decision Records</h1>
This page consists of list of decisions made for the development of the application.

## Spring+Kotlin on Server-side

### Rationale
Kotlin's expressiveness promotes productivity. Few Kotlin-Java "incompatibility" I have experienced
before that were mostly related to Annotation Processor have been resolved with [kapt](https://kotlinlang.org/docs/reference/kapt.html).

Spring is still the most widely used JVM framework with large ecosystem and documentation.

From the other alternatives I tried before:

- *Nodejs + {Koa | Express} (TypeScript)* 
    - Spring Boot produces simpler deployment package: a 
    single jar file. Although I found tools such as [nexe](https://github.com/nexe/nexe) that 
    compiles node scripts into single executable.
    - Limited feature of ORM: I tried [typeorm](https://typeorm.io/), and bookshelfjs.org,
    yet to try sequelize.org about a year ago, they may have caught up with the features.
    
- *Python + Django* 
    - Similar to Node, Python is interpreted language, packaging is a group of files.

## Authentication (local or AuthServer)
[TBD]


## Client-Side: React+TypeScript

### Rationale
There are three major client-side framework: Vue, Angular and React.
Angular was discarded because it is too opinionated and has a high learning curve.
Vue is modular and has shorter learning curve than React, but React was chosen because of its 
large ecosystem, especially the Material-UI CSS framework is very mature.

## CSS Framework: Material-UI

### Rationale
With the selction of React as the UI framework, Material UI provided the most extensive set of 
components - especially the date picker. 
And if the claim is correct, it is used by several heavy-weight business such as Capgemini, 
Uniqlo, and Coursera.

Other solutions evaluated includes

- [Microsoft Fluent Design](https://www.microsoft.com/design/fluent/#/web) - Looked promising 
    until I realized the fluent implementation includes typography and icons with restricted
    license, and taking them off requires effort.
- [Material Components for the web](https://material.io/develop/web/) - Really wanted to use this
    one but no web implementation of date picker yet.
- [react-bootstrap](https://react-bootstrap.github.io/) - Material-UI has more extensive components. 
- [Tailwind](https://tailwindcss.com/) - Not convinced about the "utility-first CSS". Again, 
    I rather have all the components readily available for use. 