package dev.betternia.harmonia.user

import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "authority")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Authority (

        @field:NotNull
        @field:Size(max = 50)
        @Id
        @Column(length = 50)
        var name: String? = null

) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }
}