package dev.betternia.harmonia.user.support

import dev.betternia.harmonia.user.User
import dev.betternia.harmonia.user.UserRepository
import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator
import org.slf4j.LoggerFactory
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.lang.IllegalStateException
import java.util.*

/**
 * This allows the authentication `authenticationManagerBuilder.getObject().authenticate()` to work
 */
@Component("userDetailsService")
class DomainUserDetailsService (
        private val userRepository: UserRepository
) : UserDetailsService {
    private val log = LoggerFactory.getLogger(javaClass)

    @Transactional
    override fun loadUserByUsername(login: String): UserDetails {
        log.debug("Authenticating {}", login)

        if (EmailValidator().isValid(login, null)) {
            return userRepository.findOneWithAuthoritiesByEmailIgnoreCase(login)?.let {
                        createSpringSecurityUser(login, it)
                    } ?: throw IllegalStateException("User with email $login was not found in the database")
        }

        val lowercaseUsername = login.toLowerCase(Locale.ENGLISH)
        return userRepository.findOneWithAuthoritiesByUserName(lowercaseUsername)?.let {
                    createSpringSecurityUser(lowercaseUsername, it)
                } ?: throw IllegalStateException("User $lowercaseUsername was not found in the database")
    }

    private fun createSpringSecurityUser(lowercaseLogin: String, user: User):
            dev.betternia.harmonia.security.User {
        if (!user.activated) {
            throw IllegalStateException("User $lowercaseLogin was not activated")
        }
        val grantedAuthorities = user.authorities.map { SimpleGrantedAuthority(it.name) }
        val user = dev.betternia.harmonia.security.User(
                user.uid!!,
                user.userName!!,
                user.password!!,
                grantedAuthorities
        )
        return user
    }
}