package dev.betternia.harmonia.user

import dev.betternia.harmonia.config.DEFAULT_PAGE_REQUEST
import dev.betternia.harmonia.config.DEFAULT_PAGE_SIZE
import dev.betternia.harmonia.config.USERNAME_REGEX
import dev.betternia.harmonia.security.ADMIN
import dev.betternia.harmonia.foundation.ResourceNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.core.MethodParameter
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.BeanPropertyBindingResult
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.*
import java.net.URISyntaxException
import javax.validation.Valid


@RestController
@RequestMapping("/api")
class UserResource(
        private val userService: UserService,
        private val userMapper: UserMapper
) {

    private val logger = LoggerFactory.getLogger(javaClass)

    /**
     * `POST  /users`  : Creates a new user.
     *
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param userDTO the user to create.
     * @return the `ResponseEntity` with status `201 (Created)` and with body the new user, or with status `400 (Bad Request)` if the login or email is already in use.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     * @throws BadRequestAlertException `400 (Bad Request)` if the login or email is already in use.
     */
    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole(\"$ADMIN\")")
    @Throws(URISyntaxException::class)
    fun createUser(@Valid @RequestBody userDTO: UserDTO): EntityModel<UserDTO> {
        logger.debug("REST request to save User : {}", userDTO)


        val errors = BeanPropertyBindingResult(userDTO, "userDTO")
        if (userDTO.uid != null) {
            errors.addError(ObjectError("userName", "Cannot be null"))
        }
        // Lowercase the user login before comparing with database
        if (userService.getUserWithAuthoritiesByUsername(userDTO.userName!!.toLowerCase()) != null) {
            errors.addError(ObjectError("userName", "Already exists"))
        }
        if (userService.getUserWithAuthoritiesByEmail(userDTO.email!!) != null) {
            errors.addError(ObjectError("email", "Already exists"))
        }

        if (errors.hasErrors()) {
            throw MethodArgumentNotValidException(
                    MethodParameter(this.javaClass.getDeclaredMethod("createUser", UserDTO::class.java), 0),
                    errors)
        }
        userDTO.createdBy = UserService.getCurrentUserUid() ?: "system"

        val newUser = userService.createUser(userDTO)?.let {
            userMapper.userToUserDTO(it)
        }
//        mailService.sendCreationEmail(newUser)
//        return ResponseEntity.created(URI("/api/users/" + newUser.login))
//                .headers(HeaderUtil.createAlert(applicationName, "userManagement.created", newUser.login))
//                .body(newUser)

        return EntityModel<UserDTO>(
                newUser,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getUser(newUser.uid!!)!!).withSelfRel(),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getAllUsers(DEFAULT_PAGE_REQUEST)).withRel(UserDTO.ENTITY_PLURAL_NAME)
        )
    }

    /**
     * `PUT /users` : Updates an existing User.
     *
     * @param userDTO the user to update.
     * @return the `ResponseEntity` with status `200 (OK)` and with body the updated user.
     * @throws EmailAlreadyUsedException `400 (Bad Request)` if the email is already in use.
     * @throws LoginAlreadyUsedException `400 (Bad Request)` if the login is already in use.
     */
    @PutMapping("/users/{uid}")
    @PreAuthorize("hasRole(\"$ADMIN\")")
    open fun updateUser(@PathVariable uid: String, @Valid @RequestBody userDTO: UserDTO): EntityModel<UserDTO> {
        logger.debug("REST request to update User : {}", userDTO)

        val errors = BeanPropertyBindingResult(userDTO, "userDTO")

        userDTO.uid = uid

        var existingUser = userService.getUserWithAuthoritiesByEmail(userDTO.email!!)
        if (existingUser != null && existingUser.uid != userDTO.uid) {
            errors.addError(ObjectError("email", "Already used"))
        }
        existingUser = userService.getUserWithAuthoritiesByUsername(userDTO.userName!!.toLowerCase())
        if (existingUser != null && existingUser.uid != userDTO.uid) {
            errors.addError(ObjectError("userName", "Already used"))
        }

        if (errors.hasErrors()) {
            throw MethodArgumentNotValidException(
                    MethodParameter(this.javaClass.getDeclaredMethod("createUser", UserDTO::class.java), 0),
                    errors)
        }

        userDTO.lastModifiedBy = UserService.getCurrentUserUid() ?: "system"
        val updatedUser = userService.updateUser(userDTO).orElseGet(null)

        updatedUser ?: throw IllegalStateException("Unable to update User")

        return wrapInEntityModel(updatedUser)!!
    }

    /**
     * `GET /users` : get all users.
     *
     * @param pageable the pagination information.
     * @return the `ResponseEntity` with status `200 (OK)` and with body all users.
     */
    @GetMapping("/users")
    fun getAllUsers(pageable: Pageable): PagedModel<EntityModel<UserDTO>> {
        val page = userService.getAllManagedUsers(pageable)
//        val headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page)
//        return ResponseEntity(page.content, headers, HttpStatus.OK)

        val pageWithModels = page.content.map {
            wrapInEntityModel(it)
        }

        val pagedModel = PagedModel<EntityModel<UserDTO>>(
                pageWithModels,
                PagedModel.PageMetadata(page.size.toLong(), page.number.toLong(), page.totalElements),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getAllUsers(pageable)).withSelfRel()
        )

        return pagedModel
    }

    /**
     * Gets a list of all roles.
     * @return a string list of all roles.
     */
    @GetMapping("/users/authorities")
    @PreAuthorize("hasRole(\"$ADMIN\")")
    fun getAuthorities() = userService.getAuthorities()

    /**
     * `GET /users/:username` : get the "username" user.
     *
     * @param username the login of the user to find.
     * @return the `ResponseEntity` with status `200 (OK)` and with body the "login" user, or with status `404 (Not Found)`.
     */
    @GetMapping("/users/{uid}")
    fun getUser(@PathVariable uid: String): EntityModel<UserDTO>? {
        logger.debug("REST request to get User : {}", uid)

        val user = wrapInEntityModel(userService.getUserWithAuthoritiesByUid(uid)
                ?: throw ResourceNotFoundException(UserDTO.ENTITY_NAME, uid))
        return user;
    }

    /**
     * `GET /users/:username` : get the "username" user.
     *
     * @param username the login of the user to find.
     * @return the `ResponseEntity` with status `200 (OK)` and with body the "login" user, or with status `404 (Not Found)`.
     */
    @GetMapping("/users/byusername/{username:$USERNAME_REGEX}")
    fun getUserByUsername(@PathVariable username: String): EntityModel<UserDTO>? {
        logger.debug("REST request to get User : {}", username)

        val user = wrapInEntityModel(userService.getUserWithAuthoritiesByUsername(username))
        return user;
    }

    /**
     * `DELETE /users/:login` : delete the "login" User.
     *
     * @param login the login of the user to delete.
     * @return the `ResponseEntity` with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/users/{uid}")
    @PreAuthorize("hasRole(\"$ADMIN\")")
    fun deleteUser(@PathVariable uid: String) {
        logger.debug("REST request to delete User: {}", uid)
        userService.deleteUser(uid)
    }

    fun wrapInEntityModel(user: User?): EntityModel<UserDTO>? {
        val pageRequest = PageRequest.of(0, DEFAULT_PAGE_SIZE)
        return user?.let {
            val userDto = userMapper.userToUserDTO(it)
            EntityModel<UserDTO>(
                    userDto,
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getUser(userDto.uid!!)!!).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getAllUsers(pageRequest)).withRel(UserDTO.ENTITY_PLURAL_NAME)
            )
        }
    }

}