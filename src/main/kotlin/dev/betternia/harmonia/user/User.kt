package dev.betternia.harmonia.user

import com.fasterxml.jackson.annotation.JsonIgnore
import dev.betternia.harmonia.config.USERNAME_REGEX
import dev.betternia.harmonia.foundation.AbstractAuditingEntity
import dev.betternia.harmonia.user.Authority
import org.hibernate.annotations.BatchSize
import java.time.Instant
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Entity
@Table(name = "harmonia_user")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class User(
        // Replaced by `sid` from EntityBase
//        @Id
//        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
//        @SequenceGenerator(name = "sequenceGenerator")
//        var id: Long? = null,

        @NotNull
        @field:Pattern(regexp = USERNAME_REGEX)
        @field:Size(min = 1, max = 50)
        @Column(name= "user_name", length = 50, unique = true, nullable = false)
        var userName: String? = null,

        @JsonIgnore
        @field:NotNull
        @field:Size(min = 60, max = 60)
        @Column(name = "password_hash", length = 60, nullable = false)
        var password: String? = null,

        @field:Size(max = 50)
        @Column(name = "first_name", length = 50)
        var firstName: String? = null,

        @field:Size(max = 50)
        @Column(name = "last_name", length = 50)
        var lastName: String? = null,

        @field:Email
        @field:Size(min = 5, max = 254)
        @Column(length = 254, unique = true)
        var email: String? = null,

        @field:NotNull
        @Column(nullable = false)
        var activated: Boolean = false,

        @field:Size(min = 2, max = 10)
        @Column(name = "lang_key", length = 10)
        var langKey: String? = null,

        @field:Size(max = 256)
        @Column(name = "image_url", length = 256)
        var imageUrl: String? = null,

        @field:Size(max = 20)
        @Column(name = "activation_key", length = 20)
        @JsonIgnore
        var activationKey: String? = null,

        @field:Size(max = 20)
        @Column(name = "reset_key", length = 20)
        @JsonIgnore
        var resetKey: String? = null,

        @Column(name = "reset_date")
        var resetDate: Instant? = null,

        @JsonIgnore
        @ManyToMany
        @JoinTable(
                name = "user_authority",
                joinColumns = [JoinColumn(name = "user_uid", referencedColumnName = "uid")],
                inverseJoinColumns = [JoinColumn(name = "authority_name", referencedColumnName = "name")]
        )
//        @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
        @BatchSize(size = 20)
        var authorities: MutableSet<Authority> = mutableSetOf()

) : AbstractAuditingEntity()
{

}