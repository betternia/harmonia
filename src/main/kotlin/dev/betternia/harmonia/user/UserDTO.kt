package dev.betternia.harmonia.user

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.config.USERNAME_REGEX
import org.springframework.hateoas.server.core.Relation
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size



/**
 * A DTO representing a user, with his authorities.
 */
@Relation(collectionRelation = UserDTO.ENTITY_PLURAL_NAME) // For HATEOAS, to produce _embedded/users
@JsonIgnoreProperties(ignoreUnknown = true)
// TODO: extend from DTOBase
open class UserDTO(
    var sid: Long? = null,

    var uid: String? = null,

    @field:NotBlank
    @field:Pattern(regexp = USERNAME_REGEX)
    @field:Size(min = 1, max = 50)
    var userName: String? = null,

    @field:Size(max = 50)
    var firstName: String? = null,

    @field:Size(max = 50)
    var lastName: String? = null,

    @field:Email
    @field:Size(min = 5, max = 254)
    var email: String? = null,

    @field:Size(max = 256)
    var imageUrl: String? = null,

    var activated: Boolean = false,

    @field:Size(min = 2, max = 10)
    var langKey: String? = null,

    var createdBy: String? = null,

    var createdDate: Instant? = null,

    var lastModifiedBy: String? = null,

    var lastModifiedDate: Instant? = null,

    var authorities: Set<String>? = null

) {
    constructor(user: User) :
        this(
            user.sid, user.uid, user.userName, user.firstName, user.lastName, user.email,
            user.imageUrl, user.activated, user.langKey,
            user.createdBy, user.createdDate, user.lastModifiedBy, user.lastModifiedDate,
            user.authorities.map { it.name }.filterNotNullTo(mutableSetOf())
        )

    fun isActivated(): Boolean = activated

    override fun toString() = "UserDTO{" +
        "userName='" + userName + '\'' +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", email='" + email + '\'' +
        ", imageUrl='" + imageUrl + '\'' +
        ", activated=" + activated +
        ", langKey='" + langKey + '\'' +
        ", createdBy=" + createdBy +
        ", createdDate=" + createdDate +
        ", lastModifiedBy='" + lastModifiedBy + '\'' +
        ", lastModifiedDate=" + lastModifiedDate +
        ", authorities=" + authorities +
        "}"

    companion object {
        const val ENTITY_NAME = "user"
        const val ENTITY_PLURAL_NAME = "users"
    }
}
