package dev.betternia.harmonia.user

import java.time.Instant
import org.springframework.cache.annotation.Cacheable
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data JPA repository for the [User] entity.
 */
@Repository
interface UserRepository : JpaRepository<User, String> {

    fun findOneByActivationKey(activationKey: String): User?

    fun findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(dateTime: Instant): List<User>

    fun findOneByResetKey(resetKey: String): User?

    fun findOneByUid(uid: String): User?

    fun findOneByEmailIgnoreCase(email: String?): User?

    fun findOneByUserName(userName: String): User?

    @EntityGraph(attributePaths = ["authorities"])
    fun findOneWithAuthoritiesBySid(sid: Long): User?

    @EntityGraph(attributePaths = ["authorities"])
    fun findOneWithAuthoritiesByUid(uid: String): User?

    @EntityGraph(attributePaths = ["authorities"])
    @Cacheable(cacheNames = [USERS_BY_USERNAME_CACHE])
    fun findOneWithAuthoritiesByUserName(UserName: String): User?

    @EntityGraph(attributePaths = ["authorities"])
    @Cacheable(cacheNames = [USERS_BY_EMAIL_CACHE])
    fun findOneWithAuthoritiesByEmailIgnoreCase(email: String): User?

    fun findAllByUserNameNot(pageable: Pageable, UserName: String): Page<User>

    companion object {

        const val USERS_BY_USERNAME_CACHE: String = "usersByUserName"

        const val USERS_BY_EMAIL_CACHE: String = "usersByEmail"
    }
}
