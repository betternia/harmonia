package dev.betternia.harmonia.user

import org.springframework.stereotype.Service

/**
 * Mapper for the entity [User] and its DTO called [UserDTO].
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
class UserMapper {

    fun usersToUserDTOs(users: List<User?>): MutableList<UserDTO> =
        users.asSequence()
            .filterNotNull()
            .mapTo(mutableListOf()) { userToUserDTO(it) }

    fun userToUserDTO(user: User): UserDTO = UserDTO(user)

    fun userDTOsToUsers(userDTOs: List<UserDTO?>) =
        userDTOs.asSequence()
            .mapNotNullTo(mutableListOf()) { userDTOToUser(it) }

    fun userDTOToUser(userDTO: UserDTO?) =
        when (userDTO) {
            null -> null
            else -> {
                User(
                        userName = userDTO.userName,
                        firstName = userDTO.firstName,
                        lastName = userDTO.lastName,
                        email = userDTO.email,
                        imageUrl = userDTO.imageUrl,
                        activated = userDTO.activated,
                        langKey = userDTO.langKey,
                        authorities = authoritiesFromStrings(userDTO.authorities)
                ).apply {
                    uid = userDTO.uid
                }
            }
        }

    private fun authoritiesFromStrings(authoritiesAsString: Set<String>?): MutableSet<Authority> =
        authoritiesAsString?.mapTo(mutableSetOf()) { Authority(name = it) } ?: mutableSetOf()

    fun userFromUid(uid: String?) = uid?.let { User().apply { this.uid = uid } }
}
