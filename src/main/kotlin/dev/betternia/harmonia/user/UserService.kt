package dev.betternia.harmonia.user

import dev.betternia.harmonia.config.ANONYMOUS_USER
import dev.betternia.harmonia.config.DEFAULT_LANGUAGE
import dev.betternia.harmonia.security.USER
import dev.betternia.harmonia.security.getCurrentUserName
import org.apache.commons.lang3.RandomStringUtils
import org.slf4j.LoggerFactory
import org.springframework.cache.CacheManager
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.security.SecureRandom
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

/**
 * Service class for managing users.
 */
@Service
@Transactional
class UserService(
        private val userRepository: UserRepository,
        private val passwordEncoder: PasswordEncoder,
        private val authorityRepository: AuthorityRepository,
        private val cacheManager: CacheManager
) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun generateRandomAlphanumericString(): String? {
        return RandomStringUtils.random(20, 0, 0, true, true, null as CharArray?, SecureRandom())
    }

    fun activateRegistration(key: String): User? {
        log.debug("Activating user for activation key {}", key)
        return userRepository.findOneByActivationKey(key)?.let {
            // activate given user for the registration key.
            it.activated = true
            it.activationKey = null
            clearUserCaches(it)
            log.debug("Activated user: {}", it)
            it
        }
    }

    fun completePasswordReset(newPassword: String, key: String): User? {
        log.debug("Reset user password for reset key {}", key)

        // T.also{} automatically returns itself (i.e. this)
        return userRepository.findOneByResetKey(key)?.also {
            if (it.resetDate?.isAfter(Instant.now().minusSeconds(86400)) ?: false) {

                it.password = passwordEncoder.encode(newPassword)
                it.resetKey = null
                it.resetDate = null
                clearUserCaches(it)
            }
        }
    }

    fun requestPasswordReset(mail: String): User? {
        return userRepository.findOneByEmailIgnoreCase(mail)?.also {
            if (it.activated) {
                it.resetKey = generateRandomAlphanumericString()
                it.resetDate = Instant.now()
                clearUserCaches(it)
            }
        }
    }

    fun registerUser(userDTO: UserDTO, password: String): User {
        val userName = userDTO.userName ?: throw IllegalArgumentException("Empty login not allowed")
        val email = userDTO.email
        userRepository.findOneByUserName(userName.toLowerCase())?.let { existingUser ->
            val removed = removeNonActivatedUser(existingUser)
            if (!removed) {
                throw RuntimeException("User name already used!")
            }
        }
        userRepository.findOneByEmailIgnoreCase(email)?.let { existingUser ->
            val removed = removeNonActivatedUser(existingUser)
            if (!removed) {
                throw RuntimeException("Email already used!")
            }
        }
        val newUser = User()
        val encryptedPassword = passwordEncoder.encode(password)
        newUser.apply {
            this.userName = userName.toLowerCase()
            // new user gets initially a generated password
            this.password = encryptedPassword
            firstName = userDTO.firstName
            lastName = userDTO.lastName
            this.email = email?.toLowerCase()
            imageUrl = userDTO.imageUrl
            langKey = userDTO.langKey
            // new user is not active
            activated = false
            // new user gets registration key
            activationKey = generateRandomAlphanumericString()
            authorities = mutableSetOf()
            authorityRepository.findById(USER).ifPresent { authorities.add(it) }
        }
        userRepository.save(newUser)
        clearUserCaches(newUser)

        log.debug("Created record for User: {}", newUser)
        return newUser
    }

    private fun removeNonActivatedUser(existingUser: User): Boolean {
        if (existingUser.activated) {
            return false
        }
        userRepository.delete(existingUser)
        userRepository.flush()
        clearUserCaches(existingUser)
        return true
    }

    fun createUser(userDTO: UserDTO): User {
        val encryptedPassword = passwordEncoder.encode(generateRandomAlphanumericString())
        val user = User(
                userName = userDTO.userName?.toLowerCase(),
                firstName = userDTO.firstName,
                lastName = userDTO.lastName,
                email = userDTO.email?.toLowerCase(),
                imageUrl = userDTO.imageUrl,
                langKey = userDTO.langKey ?: DEFAULT_LANGUAGE, // default language
                password = encryptedPassword,
                resetKey = generateRandomAlphanumericString(),
                resetDate = Instant.now(),
                activated = true
        ).apply {
            createdBy = userDTO.createdBy
        }
        userDTO.authorities?.apply {
            val authorities = this.asSequence()
                    .map(authorityRepository::findById)
                    .filter(Optional<Authority>::isPresent)
                    .mapTo(mutableSetOf()) { it.get() }
            user.authorities = authorities
        }
        userRepository.save(user)
        clearUserCaches(user)
        log.debug("Created Information for User: {}", user)
        return user
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName last name of user.
     * @param email email id of user.
     * @param langKey language key.
     * @param imageUrl image URL of user.
     */
    fun updateUser(firstName: String?, lastName: String?, email: String?, langKey: String?, imageUrl: String?) {
        getCurrentUserName()
                .ifPresent { userName ->
                    userRepository.findOneByUserName(userName)?.let {
                        it.firstName = firstName
                        it.lastName = lastName
                        it.email = email?.toLowerCase()
                        it.langKey = langKey
                        it.imageUrl = imageUrl
                        clearUserCaches(it)
                        // TODO: verify if this is needed
                        // userRepository.save(it)
                        log.debug("Updated record for User: {}", it)
                    }
                }
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    fun updateUser(userDTO: UserDTO): Optional<User> {
        return Optional.of(userRepository.findById(userDTO.uid!!))
                .filter(Optional<User>::isPresent)
                .map { it.get() }
                .map { user ->
                    clearUserCaches(user)
                    user.apply {
                        userName = userDTO.userName!!.toLowerCase()
                        firstName = userDTO.firstName
                        lastName = userDTO.lastName
                        email = userDTO.email?.toLowerCase()
                        imageUrl = userDTO.imageUrl
                        activated = userDTO.activated
                        langKey = userDTO.langKey
                    }
                    val managedAuthorities = user.authorities
                    managedAuthorities.clear()
                    userDTO.authorities?.apply {
                        this.asSequence()
                                .map { authorityRepository.findById(it) }
                                .filter { it.isPresent }
                                .mapTo(managedAuthorities) { it.get() }
                    }
                    this.clearUserCaches(user)
                    log.debug("Changed Information for User: {}", user)
                    user
                }
    }

    fun deleteUser(uid: String) {
        userRepository.findOneByUid(uid)?.let { user ->
            userRepository.delete(user)
            clearUserCaches(user)
            log.debug("Deleted User: {}", user)
        }
    }

    fun changePassword(currentClearTextPassword: String, newPassword: String) {
        getCurrentUserName()
                .ifPresent { userName ->
                    userRepository.findOneByUserName(userName)?.let {
                        val currentEncryptedPassword = it.password
                        if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                            throw RuntimeException("InvalidPasswordException")
                        }
                        val encryptedPassword = passwordEncoder.encode(newPassword)
                        it.password = encryptedPassword
                        clearUserCaches(it)
                        log.debug("Changed password for User: {}", it)
                    }
                }
    }

    @Transactional(readOnly = true)
    fun getAllManagedUsers(pageable: Pageable): Page<User> =
            userRepository.findAllByUserNameNot(pageable, ANONYMOUS_USER)

    @Transactional(readOnly = true)
    fun getUserWithAuthoritiesByUid(uid: String): User? =
            userRepository.findOneWithAuthoritiesByUid(uid)

    @Transactional(readOnly = true)
    fun getUserWithAuthoritiesByEmail(email: String): User? =
            userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email)

    @Transactional(readOnly = true)
    fun getUserWithAuthoritiesByUsername(userName: String): User? =
            userRepository.findOneWithAuthoritiesByUserName(userName)

    @Suppress("unused")
    @Transactional(readOnly = true)
    fun getUserWithAuthorities(sid: Long): User? =
            userRepository.findOneWithAuthoritiesBySid(sid)

    @Transactional(readOnly = true)
    fun getUserWithAuthorities(): User? =
            getCurrentUserName().map{
                userRepository.findOneWithAuthoritiesByUserName(it)
            }.get()

    /**
     * Not activated users should be automatically deleted after 3 days.
     *
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    fun removeNotActivatedUsers() {
        userRepository
                .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(
                        Instant.now().minus(3, ChronoUnit.DAYS)
                )
                .forEach { user ->
                    log.debug("Deleting not activated user {}", user.userName)
                    userRepository.delete(user)
                    clearUserCaches(user)
                }
    }

    /**
     * @return a list of all the authorities
     */
    fun getAuthorities() =
            authorityRepository.findAll().asSequence().map { it.name }.filterNotNullTo(mutableListOf())

    fun clearUserCaches(user: User) {
        cacheManager.getCache(UserRepository.USERS_BY_USERNAME_CACHE)?.evict(user.userName!!)
        user.email?.let {
            cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)?.evict(it)
        }
    }

    companion object {
        fun getCurrentUser(): org.springframework.security.core.userdetails.User? {
            return SecurityContextHolder.getContext().authentication?.principal
                    as org.springframework.security.core.userdetails.User
        }

        fun getCurrentUserUid(): String? {
            if (getCurrentUser() is dev.betternia.harmonia.security.User) {
                return (getCurrentUser() as dev.betternia.harmonia.security.User)?.uid
            }
            return null
        }
    }
}
