package dev.betternia.harmonia.controller

import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpRequest
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import javax.servlet.http.HttpServletRequest


/**
 * This ForwardController addresses the issue of SpringBoot returning 404 for the URL defined
 * in the web client routing, in this case by Rect Router
 *
 * The routes configured in the client side were mapped to forward root path, which is handled
 * by the index.html containing the JavaScript.
 */
@Controller
class UiRouterForwardController {
    private val log = LoggerFactory.getLogger(javaClass)
    // Discussion here:
    // https://stackoverflow.com/questions/38516667/springboot-angular2-how-to-handle-html5-urls/46854105#46854105
    // Instead of specifying each path as in
//    @RequestMapping(value = ["/login**", "/users**", "/persons**", "/organizations**"])
    @RequestMapping(value = ["/**/{[path:[^\\.]*}"])
    fun redirect(request: HttpServletRequest): String {

        log.info("UiRouterForwardController:redirect called; request: [{}] {}, from: {}", request.method, request.requestURI, request.remoteAddr)
        // Forward to home page so that route is preserved.
        return "forward:/index.html"
    }
}