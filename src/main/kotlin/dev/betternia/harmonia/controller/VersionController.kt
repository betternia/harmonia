package dev.betternia.harmonia.controller

//import net.logstash.logback.marker.Markers.appendEntries
import dev.betternia.harmonia.realm.RealmContextHolder
import org.slf4j.LoggerFactory
import org.slf4j.MarkerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.io.IOException


@RestController
@RequestMapping("/api")
public class VersionController {
    private val logger = LoggerFactory.getLogger(javaClass)
    companion object {
        const val APP_VERSION = "v1"
    }

    @GetMapping(path = ["/version"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun version(): Map<String, Any?> {
        val realm = RealmContextHolder.getRealm()
        val response = mapOf<String, Any?>(
                "version" to APP_VERSION,
                "realm" to realm
        )
        logger.info("version: ", APP_VERSION)
        return response;
    }
}