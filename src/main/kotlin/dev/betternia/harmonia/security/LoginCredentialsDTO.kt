package dev.betternia.harmonia.security

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class LoginCredentialsDTO(
        @field:NotNull
        @field:Size(min = 4, max = 50)
        var username: String? = null,

        @field:NotNull
        @field:Size(min = 4, max = 50)
        var password: String? = null,

        var isRememberMe: Boolean? = null
) {
}