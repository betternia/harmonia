package dev.betternia.harmonia.security

import com.fasterxml.jackson.annotation.JsonProperty
import dev.betternia.harmonia.security.jwt.JWTFilter
import dev.betternia.harmonia.user.UserService
import dev.betternia.harmonia.user.UserDTO
import dev.betternia.harmonia.user.UserMapper
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import java.security.Principal
import javax.validation.Valid

/**
 * Partially from jHipster's UserJWTController
 * Using `/api/auth` as base path produces 401, even with allowed in the security config
 */
@RestController
@RequestMapping("/api")
class AuthnController(private val tokenProvider: TokenProvider,
                      private val authenticationManagerBuilder: AuthenticationManagerBuilder,
                      private val userService: UserService,
                      private val userMapper: UserMapper
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @PostMapping("/authenticate", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun authenticate(@Valid @RequestBody loginCredentials: LoginCredentialsDTO): ResponseEntity<JWTToken> {

        logger.info("Authenticating")
        val authenticationToken = UsernamePasswordAuthenticationToken(loginCredentials.username, loginCredentials.password)

        val authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken)
        SecurityContextHolder.getContext().authentication = authentication
        val rememberMe = loginCredentials.isRememberMe ?: false
        val jwt = tokenProvider.createToken(authentication, rememberMe)
        val httpHeaders = HttpHeaders()
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer $jwt")
        return ResponseEntity(JWTToken(jwt), httpHeaders, HttpStatus.OK)
    }

    @GetMapping("/userinfo", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun userinfo(principal: Principal? /*, OAuth2AuthenticationToken authentication*/): UserDTO? {
        val authentication = SecurityContextHolder.getContext().authentication
        val message = "Principal Name:" + principal?.name

        val user = userService.getUserWithAuthoritiesByUsername(authentication.name)?.let {
            userMapper.userToUserDTO(it)
        }

//        val idToken: String = (authentication.getPrincipal() as DefaultOidcUser).getIdToken().getTokenValue()
//        val accessToken: String = oAuth2AccessToken.getTokenValue()


        return user;
    }


    /**
     * Object to return as body in JWT Authentication.
     */
    data class JWTToken(
            @get:JsonProperty("id_token")
            var idToken: String?
    ) {}
}