package dev.betternia.harmonia.security

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import java.nio.charset.StandardCharsets
import java.security.Key
import java.util.*

private const val AUTHORITIES_KEY = "auth"

@Component
class TokenProvider(
        @Value("\${authentication.jwt.secret}")
        val authJwtSecret: String,

        @Value("\${authentication.jwt.base64Secret}")
        val authJwtBase64Secret: String,

        @Value("\${authentication.jwt.tokenValidityInSeconds}")
        val authJwtTokenValidityInSec: Long,

        @Value("\${authentication.jwt.tokenValidityInSecondsForRememberMe}")
        val authJwtTokenValidityInSecForRememberMe: Long
) : InitializingBean {

    private val log = LoggerFactory.getLogger(javaClass)

    private var key: Key? = null

    private var tokenValidityInMilliseconds: Long = 0

    private var tokenValidityInMillisecondsForRememberMe: Long = 0

    @Throws(Exception::class)
    override fun afterPropertiesSet() {
        val keyBytes: ByteArray
        val secret = authJwtSecret
        keyBytes = if (!StringUtils.isEmpty(secret)) {
            log.warn("Warning: the JWT key used is not Base64-encoded. " + "We recommend using the `jhipster.security.authentication.jwt.base64-secret` key for optimum security.")
            secret.toByteArray(StandardCharsets.UTF_8)
        } else {
            log.debug("Using a Base64-encoded JWT secret key")
            Decoders.BASE64.decode(authJwtBase64Secret)
        }
        this.key = Keys.hmacShaKeyFor(keyBytes)
        this.tokenValidityInMilliseconds = 1000 * authJwtTokenValidityInSec
        this.tokenValidityInMillisecondsForRememberMe = 1000 * authJwtTokenValidityInSecForRememberMe
    }

    fun createToken(authentication: Authentication, rememberMe: Boolean): String {
        val user = authentication.principal as dev.betternia.harmonia.security.User
        val authorities = authentication.authorities.asSequence()
                .map { it.authority }
                .joinToString(separator = ",")

        val now = Date().time
        val validity = if (rememberMe) {
            Date(now + this.tokenValidityInMillisecondsForRememberMe)
        } else {
            Date(now + this.tokenValidityInMilliseconds)
        }

        // getAuthentication must match
        return Jwts.builder()
                .setClaims(mapOf(CLAIM_USERNAME to authentication.name))
                .setSubject(user.uid)
                .claim(AUTHORITIES_KEY, authorities)
                .signWith(key, SignatureAlgorithm.HS512)
                .setExpiration(validity)
                .compact()
    }

    //
    fun getAuthentication(token: String): Authentication {
        val claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .body

        val authorities = claims[AUTHORITIES_KEY].toString().splitToSequence(",")
                .mapTo(mutableListOf()) { SimpleGrantedAuthority(it) }

        val principal = User(claims.subject, claims.get(CLAIM_USERNAME, String::class.java), "", authorities)

        return UsernamePasswordAuthenticationToken(principal, token, authorities)
    }

    fun validateToken(authToken: String): Boolean {
        try {
            Jwts.parser().setSigningKey(key).parseClaimsJws(authToken)
            return true
        } catch (e: JwtException) {
            log.info("Invalid JWT token.")
            log.trace("Invalid JWT token trace.", e)
        } catch (e: IllegalArgumentException) {
            log.info("Invalid JWT token.")
            log.trace("Invalid JWT token trace.", e)
        }

        return false
    }

    companion object{
        const val CLAIM_USERNAME = "username"
    }
}
