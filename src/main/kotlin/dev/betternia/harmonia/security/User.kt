package dev.betternia.harmonia.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User

class User(
        val uid: String,
        username: String,
        password: String,
        authorities: Collection<out GrantedAuthority>? = null
): User(username, password, authorities) {

}