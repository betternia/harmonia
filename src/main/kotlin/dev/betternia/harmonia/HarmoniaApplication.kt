package dev.betternia.harmonia

import dev.betternia.harmonia.config.HarmoniaConfigProperties
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import javax.annotation.PostConstruct

@SpringBootApplication
@EnableConfigurationProperties(HarmoniaConfigProperties::class)
class HarmoniaApplication {
    private val log = LoggerFactory.getLogger(javaClass)

    @PostConstruct
    private fun init() {
        val port = System.getenv("PORT")
        log.warn("HarmoniaApplication init. PORT: $port")
    }
}

fun main(args: Array<String>) {
    runApplication<HarmoniaApplication>(*args)
}