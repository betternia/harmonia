package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.OrgAffiliation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [OrgAffiliation] entity.
 */
@Repository
interface OrgAffiliationRepository : JpaRepository<OrgAffiliation, String>
