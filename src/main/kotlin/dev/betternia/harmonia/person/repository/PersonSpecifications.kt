package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.Person
import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.Predicate

object PersonSpecifications {

    fun filterOr(criteria: Person?): Specification<Person> {
        return Specification { root, query, cb ->
            val predicates = mutableListOf<Predicate>()

            criteria?.givenName?.let {
                val path = root.get<String>("givenName")
                predicates.add(cb.like(path, criteria.givenName))
            }

            criteria?.familyName?.let {
                val path = root.get<String>("familyName")
                predicates.add(cb.like(path, criteria.familyName))
            }

            criteria?.baptismalName?.let {
                val path = root.get<String>("baptismalName")
                predicates.add(cb.like(path, criteria.baptismalName))
            }

            criteria?.addressCity?.let {
                val path = root.get<String>("addressCity")
                predicates.add(cb.like(path, criteria.addressCity))
            }

            criteria?.tags?.let {
                val path = root.get<Collection<String>>("tags")
                predicates.add(cb.isMember(criteria.tags, path))
            }

            cb.and(*predicates.toTypedArray())
        }
    }

}