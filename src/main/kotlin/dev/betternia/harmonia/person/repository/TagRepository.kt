package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.Tag
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Tag] entity.
 */
@Suppress("unused")
@Repository
interface TagRepository : JpaRepository<Tag, String>
