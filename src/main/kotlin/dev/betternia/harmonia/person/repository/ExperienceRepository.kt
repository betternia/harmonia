package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.Experience
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Experience] entity.
 */
@Suppress("unused")
@Repository
interface ExperienceRepository : JpaRepository<Experience, String>
