package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.Certification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Certification] entity.
 */
@Suppress("unused")
@Repository
interface CertificationRepository : JpaRepository<Certification, String>
