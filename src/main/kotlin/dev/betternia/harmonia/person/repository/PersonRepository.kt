package dev.betternia.harmonia.person.repository

import dev.betternia.harmonia.person.Person
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

/**
 * Spring Data  repository for the [Person] entity.
 */
@Suppress("unused")
@Repository
interface PersonRepository : JpaRepository<Person, String>, JpaSpecificationExecutor<Person>
{
    fun findByUid(uid: String): Person?

    fun findAllByRealmUid(realmUid: String, pageable: Pageable): Page<Person>

    fun findAllByHeadOfFamilyUid(uid: String): Set<Person>

    @Query("SELECT p FROM Person p " +
            "WHERE (p.familyName LIKE :nameLike OR p.givenName LIKE :nameLike) " +
            "AND (p.realmUid = :realmUid)")
    fun queryByRealmUid(@Param("realmUid") realmUid: String, @Param("nameLike") nameLike: String, pageable: Pageable): Page<Person>
}
