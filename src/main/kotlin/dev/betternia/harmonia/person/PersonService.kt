package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.FileFormat
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import java.io.InputStream

/**
 * Service Interface for managing [dev.betternia.harmonia.domain.Person].
 */
// TODO: Return Entity instead of DTO
interface PersonService {

    /**
     * Save a person.
     *
     * @param personDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(personDTO: PersonDTO): Person

    fun save(people: Collection<Person>): Collection<Person>

    /**
     * @deprecated Use queryAll instead
     */
    fun findAll(realmUid: String, criteria: Person?, pageable: Pageable): Page<Person>

    /**
     * Query people based on criteria.
     *
     * @param realmUid the realm Uid.
     * @param criteria the query criteria @see PersonSpecification.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    fun queryAll(realmUid: String, criteria: Person?, pageable: Pageable): Page<Person>

    /**
     * Get the "uid" person.
     *
     * @param uid the uid of the entity.
     * @return the entity.
     */
    fun findOne(uid: String): Person?

    /**
     * Delete the "uid" person.
     *
     * @param uid the uid of the entity.
     */
    fun delete(uid: String)

    /**
     * Import from a text file
     */
    fun deserialize(input: InputStream, fileFormat: FileFormat): List<PersonDTO>

    /**
     * Import from a text file
     */
    fun serialize(dtos: Collection<PersonDTO>, fileFormat: FileFormat): String

    companion object {
        /**
         * Mapper function that creates PersonDTO traversing only one level (headOfFamily)
         */
        fun toDtoOneLevel(entity: Person?, loadReference: Boolean = true): PersonDTO? {
            if (entity == null) {
                return null
            }
            val personDTO = PersonDTO()

            if (loadReference) {
                personDTO.headOfFamily = toDtoOneLevel( entity.headOfFamily, false)
            }
            personDTO.dependents = entity.dependents.map {
                toDtoOneLevel(it, false)!!
            }.toMutableSet()
            personDTO.headOfFamilyUid = entity?.headOfFamily?.uid
            personDTO.uid = entity.uid
            personDTO.realmUid = entity.realmUid
            personDTO.userUid = entity.userUid
            personDTO.headline = entity.headline
            personDTO.synopsis = entity.synopsis
            personDTO.domain = entity.domain
            personDTO.hometown = entity.hometown
            personDTO.gender = entity.gender
            personDTO.givenName = entity.givenName
            personDTO.familyName = entity.familyName
            personDTO.nlGivenName = entity.nlGivenName
            personDTO.nlFamilyName = entity.nlFamilyName
            personDTO.alternateName = entity.alternateName
            personDTO.baptismalName = entity.baptismalName
            personDTO.birthDate = entity.birthDate
            personDTO.birthPlace = entity.birthPlace
            personDTO.email = entity.email
            personDTO.phoneNumber = entity.phoneNumber
            personDTO.mobileNumber = entity.mobileNumber
            personDTO.addressLine = entity.addressLine
            personDTO.addressStreet = entity.addressStreet
            personDTO.addressCity = entity.addressCity
            personDTO.addressState = entity.addressState
            personDTO.addressZip = entity.addressZip
            personDTO.refNum = entity.refNum
            personDTO.tags = entity.tags
            return personDTO
        }
    }
}
