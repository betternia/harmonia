package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable
import javax.validation.constraints.Size

/**
 * A DTO for the [dev.betternia.harmonia.domain.Experience] entity.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
data class ExperienceDTO(

        var uid: String? = null,

        @get: Size(max = 20)
        var kind: String? = null,

        @get: Size(max = 255)
        var institution: String? = null,

        @get: Size(max = 255)
        var location: String? = null,

        @get: Size(max = 255)
        var title: String? = null,

        var subject: String? = null,

        var description: String? = null,

        var achievements: String? = null,

        @get: Size(max = 8)
        var fromDate: String? = null,

        @get: Size(max = 8)
        var toDate: String? = null,

        var personUid: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ExperienceDTO) return false
        if (other.uid == null || uid == null) return false

        return uid == other.uid
    }

    override fun hashCode() = uid.hashCode()
}
