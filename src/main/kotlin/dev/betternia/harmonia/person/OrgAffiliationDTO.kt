package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.hateoas.server.core.Relation
import java.io.Serializable
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * A DTO for the [dev.betternia.harmonia.domain.OrgAffiliation] entity.
 */
@Relation(collectionRelation = "org_affiliations") // For HATEOAS, to produce _embedded/org_affiliations
@JsonIgnoreProperties(ignoreUnknown = true)
data class OrgAffiliationDTO(

        var uid: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var kind: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var name: String? = null,

        @get: NotNull
        @get: Size(max = 60)
        var role: String? = null,

        var description: String? = null,

        @get: Size(max = 255)
        var url: String? = null,

        var address: String? = null,

        @get: Size(max = 8)
        var fromDate: String? = null,

        @get: Size(max = 8)
        var toDate: String? = null,

        var personUid: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is OrgAffiliationDTO) return false
        if (other.uid == null || uid == null) return false

        return uid == other.uid
    }

    override fun hashCode() = uid.hashCode()
}
