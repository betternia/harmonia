package dev.betternia.harmonia.person

import java.util.Optional

/**
 * Service Interface for managing [dev.betternia.harmonia.domain.Certification].
 */
interface CertificationService {

    /**
     * Save a certification.
     *
     * @param certificationDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(certificationDTO: CertificationDTO): CertificationDTO

    /**
     * Get all the certifications.
     *
     * @return the list of entities.
     */
    fun findAll(): MutableList<CertificationDTO>

    /**
     * Get the "uid" certification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    fun findOne(uid: String): Optional<CertificationDTO>

    /**
     * Delete the "id" certification.
     *
     * @param id the id of the entity.
     */
    fun delete(uid: String)
}
