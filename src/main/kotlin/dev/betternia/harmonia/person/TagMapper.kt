package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper

/**
 * Mapper for the entity [Tag] and its DTO [TagDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface TagMapper :
        EntityMapper<TagDTO, Tag> {

    fun fromUid(uid: String?) = uid?.let { Tag().apply { this.uid = uid } }
}
