package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Experience] and its DTO [ExperienceDTO].
 */
@Mapper(componentModel = "spring", uses = [PersonMapper::class])
interface ExperienceMapper :
        EntityMapper<ExperienceDTO, Experience> {
    @Mappings(
        Mapping(source = "person.uid", target = "personUid")
    )
    override fun toDto(entity: Experience): ExperienceDTO

    @Mappings(
        Mapping(source = "personUid", target = "person")
    )
    override fun toEntity(dto: ExperienceDTO): Experience

    fun fromUid(uid: String?) = uid?.let { Experience().apply { this.uid = uid } }
}
