package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Certification] and its DTO [CertificationDTO].
 */
@Mapper(componentModel = "spring", uses = [PersonMapper::class])
interface CertificationMapper :
        EntityMapper<CertificationDTO, Certification> {
    @Mappings(
        Mapping(source = "person.uid", target = "personUid")
    )
    override fun toDto(entity: Certification): CertificationDTO

    @Mappings(
        Mapping(source = "personUid", target = "person")
    )
    override fun toEntity(dto: CertificationDTO): Certification

    fun fromUid(uid: String?) = uid?.let { Certification().apply { this.uid = uid } }
}
