package dev.betternia.harmonia.person

import java.util.Optional

/**
 * Service Interface for managing [dev.betternia.harmonia.domain.Tag].
 */
interface TagService {

    /**
     * Save a tag.
     *
     * @param tagDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(tagDTO: TagDTO): TagDTO

    /**
     * Get all the tags.
     *
     * @return the list of entities.
     */
    fun findAll(): MutableList<TagDTO>

    /**
     * Get the "uid" tag.
     *
     * @param uid the uid of the entity.
     * @return the entity.
     */
    fun findOne(uid: String): Optional<TagDTO>

    /**
     * Delete the "uid" tag.
     *
     * @param uid the uid of the entity.
     */
    fun delete(uid: String)
}
