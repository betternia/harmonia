package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.foundation.DTOBase
import org.springframework.hateoas.server.core.Relation
import java.io.Serializable
import java.time.LocalDate
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * A DTO for the [dev.betternia.harmonia.domain.Person] entity.
 */
//@ApiModel(description = "The Person entity.")
@Relation(collectionRelation = "persons") // For HATEOAS, to produce _embedded/persons
@JsonIgnoreProperties(ignoreUnknown = true)
data class PersonDTO(

        @get: Size(max = 50)
        var userUid: String? = null,

        var headline: String? = null,

        var synopsis: String? = null,

        var domain: String? = null,

        @get: Size(max = 255)
        var hometown: String? = null,

        @get: Size(max = 8)
        var gender: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var givenName: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var familyName: String? = null,

        @get: Size(max = 120)
        var nlGivenName: String? = null,

        @get: Size(max = 120)
        var nlFamilyName: String? = null,

        @get: Size(max = 120)
        var alternateName: String? = null,

        @get: Size(max = 120)
        var baptismalName: String? = null,

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        var birthDate: LocalDate? = null,

        @get: Size(max = 256)
        var birthPlace: String? = null,

        @get: Size(max = 256)
        var email: String? = null,

        @get: Size(max = 60)
        var phoneNumber: String? = null,

        @get: Size(max = 60)
        var mobileNumber: String? = null,

        @get: Size(max = 255)
        var addressLine: String? = null,

        @get: Size(max = 255)
        var addressStreet: String? = null,

        @get: Size(max = 120)
        var addressCity: String? = null,

        @get: Size(max = 120)
        var addressState: String? = null,

        @get: Size(max = 120)
        var addressZip: String? = null,

        var headOfFamilyUid: String? = null,

        var headOfFamily: PersonDTO? = null,

        var dependents: MutableSet<PersonDTO> = mutableSetOf(),

        var refNum: String? = null,

        var tags: Set<String>? = null,

        // action from UI to this record: delete, archive, etc.
        var _action: String? = null

) : DTOBase(), Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is PersonDTO) return false
        if (other.uid == null || uid == null) return false

        return uid == other.uid
    }

    override fun hashCode() = uid.hashCode()

    companion object {
        const val ENTITY_NAME = "person"
        const val ENTITY_PLURAL_NAME = "persons"
    }
}
