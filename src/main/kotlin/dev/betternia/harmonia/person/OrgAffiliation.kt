package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.foundation.EntityBase
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import javax.persistence.*

/**
 * A OrgAffiliation.
 */
@Entity
@Table(name = "org_affiliation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class OrgAffiliation(

    @get: NotNull
    @get: Size(max = 120)
    @Column(name = "kind", length = 120, nullable = false)
    var kind: String? = null,

    @get: NotNull
    @get: Size(max = 120)
    @Column(name = "name", length = 120, nullable = false)
    var name: String? = null,

    @get: NotNull
    @get: Size(max = 60)
    @Column(name = "role", length = 60, nullable = false)
    var role: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @get: Size(max = 255)
    @Column(name = "url", length = 255)
    var url: String? = null,

    @Column(name = "address")
    var address: String? = null,

    @get: Size(max = 8)
    @Column(name = "from_date", length = 8)
    var fromDate: String? = null,

    @get: Size(max = 8)
    @Column(name = "to_date", length = 8)
    var toDate: String? = null,

    @ManyToOne @JsonIgnoreProperties("affiliations")
    var person: Person? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
) : EntityBase() {}
