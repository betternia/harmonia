package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper

/**
 * Mapper for the entity [OrgAffiliation] and its DTO [OrgAffiliationDTO].
 */
@Mapper(componentModel = "spring", uses = [PersonMapper::class])
interface OrgAffiliationMapper :
        EntityMapper<OrgAffiliationDTO, OrgAffiliation> {

    fun fromUid(uid: String?) = uid?.let { OrgAffiliation().apply { this.uid = uid } }
}
