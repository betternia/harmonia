package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.springframework.hateoas.server.core.Relation
import java.io.Serializable
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * A DTO for the [dev.betternia.harmonia.domain.Tag] entity.
 */
@Relation(collectionRelation = "tags") // For HATEOAS, to produce _embedded/tags
@JsonIgnoreProperties(ignoreUnknown = true)
data class TagDTO(

        var uid: String? = null,

        @get: NotNull
        @get: Size(max = 20)
        var kind: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var name: String? = null,

        @get: Min(value = 0)
        @get: Max(value = 100)
        var weight: Int? = null,

        var personUid: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TagDTO) return false
        if (other.uid == null || uid == null) return false

        return uid == other.uid
    }

    override fun hashCode() = uid.hashCode()
}
