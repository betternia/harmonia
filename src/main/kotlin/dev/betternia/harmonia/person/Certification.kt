package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.foundation.EntityBase
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy

/**
 * A Certification.
 */
@Entity
@Table(name = "certification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Certification(

    @get: Size(max = 120)
    @Column(name = "kind", length = 120)
    var kind: String? = null,

    @get: NotNull
    @get: Size(max = 120)
    @Column(name = "name", length = 120, nullable = false)
    var name: String? = null,

    @Column(name = "issuing_organization")
    var issuingOrganization: String? = null,

    @get: Size(max = 8)
    @Column(name = "issue_date", length = 8)
    var issueDate: String? = null,

    @get: Size(max = 8)
    @Column(name = "expiry_date", length = 8)
    var expiryDate: String? = null,

    @get: Size(max = 120)
    @Column(name = "credential_id", length = 120)
    var credentialId: String? = null,

    @ManyToOne @JsonIgnoreProperties("certifications")
    var person: Person? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
) : EntityBase() {}
