package dev.betternia.harmonia.person

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

/**
 * Mapper for the entity [Person] and its DTO [PersonDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface PersonMapper : EntityMapper<PersonDTO, Person> {
    @Mappings(
            Mapping(source = "headOfFamily.uid", target = "headOfFamilyUid"),
            Mapping(source = "headOfFamily", target = "headOfFamily"),
            Mapping(source = "dependents", target = "dependents"),
            Mapping(source = "tags", target = "tags")
    )
    override fun toDto(entity: Person): PersonDTO

    @Mappings(
            Mapping(source = "headOfFamily", target = "headOfFamily"),
            // dependents are loaded manually
            Mapping(source = "dependents", target = "dependents", ignore = true),
            Mapping(source = "tags", target = "tags")
    )
    override fun toEntity(dto: PersonDTO): Person

    fun fromUid(uid: String?) = uid?.let { Person().apply { this.uid = uid } }
}
