package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.foundation.EntityBase
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.Size
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy

/**
 * A Experience.
 */
@Entity
@Table(name = "experience")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Experience(

    @get: Size(max = 120)
    @Column(name = "kind", length = 120)
    var kind: String? = null,

    @get: Size(max = 255)
    @Column(name = "institution", length = 255)
    var institution: String? = null,

    @get: Size(max = 255)
    @Column(name = "location", length = 255)
    var location: String? = null,

    @get: Size(max = 255)
    @Column(name = "title", length = 255)
    var title: String? = null,

    @Column(name = "subject")
    var subject: String? = null,

    @Column(name = "description")
    var description: String? = null,

    @Column(name = "achievements")
    var achievements: String? = null,

    @get: Size(max = 8)
    @Column(name = "from_date", length = 8)
    var fromDate: String? = null,

    @get: Size(max = 8)
    @Column(name = "to_date", length = 8)
    var toDate: String? = null,

    @ManyToOne @JsonIgnoreProperties("experiences")
    var person: Person? = null

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
) : EntityBase() {}
