package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.io.Serializable
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * A DTO for the [dev.betternia.harmonia.domain.Certification] entity.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
data class CertificationDTO(

        var uid: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        var name: String? = null,

        var issuingOrganization: String? = null,

        @get: Size(max = 8)
        var issueDate: String? = null,

        @get: Size(max = 8)
        var expiryDate: String? = null,

        @get: Size(max = 120)
        var credentialId: String? = null,

        var personUid: String? = null

) : Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CertificationDTO) return false
        if (other.uid == null || uid == null) return false

        return uid == other.uid
    }

    override fun hashCode() = uid.hashCode()
}
