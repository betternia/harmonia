package dev.betternia.harmonia.person

import java.util.Optional

/**
 * Service Interface for managing [dev.betternia.harmonia.domain.Experience].
 */
interface ExperienceService {

    /**
     * Save a experience.
     *
     * @param experienceDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(experienceDTO: ExperienceDTO): ExperienceDTO

    /**
     * Get all the experiences.
     *
     * @return the list of entities.
     */
    fun findAll(): MutableList<ExperienceDTO>

    /**
     * Get the "uid" experience.
     *
     * @param uid the uid of the entity.
     * @return the entity.
     */
    fun findOne(uid: String): Optional<ExperienceDTO>

    /**
     * Delete the "uid" experience.
     *
     * @param uid the id of the entity.
     */
    fun delete(uid: String)
}
