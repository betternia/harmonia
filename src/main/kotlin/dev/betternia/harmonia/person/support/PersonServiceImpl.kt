package dev.betternia.harmonia.person.support

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.MappingIterator
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import dev.betternia.harmonia.foundation.FileFormat
import dev.betternia.harmonia.person.Person
import dev.betternia.harmonia.person.PersonDTO
import dev.betternia.harmonia.person.PersonMapper
import dev.betternia.harmonia.person.PersonService
import dev.betternia.harmonia.person.repository.PersonRepository
import dev.betternia.harmonia.person.repository.PersonSpecifications
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Example
import org.springframework.data.domain.ExampleMatcher
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains
import org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.ignoreCase
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.ByteArrayOutputStream
import java.io.InputStream


/**
 * Service Implementation for managing [Person].
 */
@Service
@Transactional
class PersonServiceImpl(
        private val personRepository: PersonRepository,
        private val personMapper: PersonMapper
//        private val csvMapper: CsvMapper
) : PersonService {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val csvMapper =  CsvMapper()

    /**
     * Save a person.
     *
     * @param personDTO the entity to save.
     * @return the persisted entity.
     */
    override fun save(personDTO: PersonDTO): Person {
        logger.debug("Request to save Person : {}", personDTO)

        var person = personMapper.toEntity(personDTO)

        // Adding or updating
        var dependents = personDTO.dependents?.filter { !it._action.equals("d") }.map {
            if (it.uid.isNullOrBlank()) {
                personMapper.toEntity(it).apply {
                    headOfFamily = person
                    realmUid = person.realmUid
                }
            } else {
                // Retrieve and update with new values
                personRepository.findByUid(it.uid!!)?.apply{
                    updateEntity(it, this)
                }
            }
        }.filterNotNull().toSet()
        var savedDependents = personRepository.saveAll(dependents)

        // Deleting those marked with 'd'
        var dependentsForDelete = personDTO.dependents?.filter { !it.uid.isNullOrEmpty() && it._action.equals("d") }.map {
            // Retrieve and update with new values
            personRepository.findByUid(it.uid!!)
        }.filterNotNull()
        personRepository.deleteAll(dependentsForDelete)


        var savedPerson = personRepository.save(person)
        savedPerson.dependents = savedDependents.toSet()

        logger.info("Person updated. Dependents modified: {}, deleted: {}", savedDependents.size, dependentsForDelete.size)

        return savedPerson
    }

    override fun save(people: Collection<Person>): Collection<Person> {
        logger.debug("Request to save people [{}]", people.size)

        return personRepository.saveAll(people)
    }

    /**
     * Get all the people.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    override fun findAll(realmUid: String, criteria: Person?, pageable: Pageable): Page<Person> {
        logger.debug("Request to get all People")

        val matcher: ExampleMatcher = ExampleMatcher
                .matchingAll()
                .withIgnorePaths("headline")
                .withMatcher("familyName", ignoreCase().startsWith())
                .withMatcher("givenName", ignoreCase().startsWith())
                .withMatcher("baptismalName", ignoreCase().startsWith())

        val theCriteria = criteria?.copy()?.apply { this.realmUid = realmUid } ?: Person().apply { this.realmUid = realmUid }
        val example = Example.of<Person>(theCriteria, matcher)

//        return personRepository.findAllByRealmUid(realmUid, pageable)
        return personRepository.findAll(example, pageable)
    }

    override fun queryAll(realmUid: String, criteria: Person?, pageable: Pageable): Page<Person> {

        val spec = PersonSpecifications.filterOr(criteria)

        return personRepository.findAll(spec, pageable)
    }

    /**
     * Get one person by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    override fun findOne(uid: String): Person? {
        logger.debug("Request to get Person : {}", uid)
        val person = personRepository.findByUid(uid)
        person?.apply {
            dependents = personRepository.findAllByHeadOfFamilyUid(person.uid!!)
        }
        return person
    }

    /**
     * Delete the person by id.
     *
     * @param id the id of the entity.
     */
    override fun delete(uid: String) {
        logger.debug("Request to delete Person : {}", uid)

        personRepository.deleteById(uid)
    }

    override fun deserialize(input: InputStream, format: FileFormat): List<PersonDTO> {
        logger.debug("PersonsService::import (format: {})", format)

        val schema: CsvSchema = CsvSchema.emptySchema().withHeader()
        csvMapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true)

        val readValues: MappingIterator<PersonDTO> =
                csvMapper.readerWithSchemaFor(PersonDTO::class.java).readValues(input)
        return readValues.readAll()
    }

    override fun serialize(dtos: Collection<PersonDTO>, format: FileFormat): String {
        val outputStream = ByteArrayOutputStream()

        when (format) {
            FileFormat.CSV -> {
                var schema = csvMapper.schemaFor(PersonDTO::class.java)
                schema = schema.withHeader().withColumnSeparator(',')

                val csvWriter = csvMapper.writer(schema)

                csvWriter.writeValue(outputStream, dtos)
            }
        }

        return String(outputStream.toByteArray())
    }

    /**
     * Mapper function that creates PersonDTO traversing only one level (headOfFamily)
     */
    fun updateEntity(sourceDto: PersonDTO, targetEntity: Person): Person {
//        targetEntity.realmUid = sourceDto.realmUid
        targetEntity.userUid = sourceDto.userUid
        targetEntity.headline = sourceDto.headline
        targetEntity.synopsis = sourceDto.synopsis
        targetEntity.domain = sourceDto.domain
        targetEntity.hometown = sourceDto.hometown
        targetEntity.gender = sourceDto.gender
        targetEntity.givenName = sourceDto.givenName
        targetEntity.familyName = sourceDto.familyName
        targetEntity.nlGivenName = sourceDto.nlGivenName
        targetEntity.nlFamilyName = sourceDto.nlFamilyName
        targetEntity.alternateName = sourceDto.alternateName
        targetEntity.baptismalName = sourceDto.baptismalName
        targetEntity.birthDate = sourceDto.birthDate
        targetEntity.birthPlace = sourceDto.birthPlace
        targetEntity.email = sourceDto.email
        targetEntity.phoneNumber = sourceDto.phoneNumber
        targetEntity.mobileNumber = sourceDto.mobileNumber
        targetEntity.addressLine = sourceDto.addressLine
        targetEntity.addressStreet = sourceDto.addressStreet
        targetEntity.addressCity = sourceDto.addressCity
        targetEntity.addressState = sourceDto.addressState
        targetEntity.addressZip = sourceDto.addressZip

        return targetEntity
    }

}
