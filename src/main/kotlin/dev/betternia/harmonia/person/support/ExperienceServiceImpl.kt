package dev.betternia.harmonia.person.support

import dev.betternia.harmonia.person.ExperienceDTO
import dev.betternia.harmonia.person.ExperienceMapper
import dev.betternia.harmonia.person.ExperienceService
import dev.betternia.harmonia.person.repository.ExperienceRepository
import java.util.Optional
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service Implementation for managing [Experience].
 */
@Service
@Transactional
class ExperienceServiceImpl(
        private val experienceRepository: ExperienceRepository,
        private val experienceMapper: ExperienceMapper
) : ExperienceService {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a experience.
     *
     * @param experienceDTO the entity to save.
     * @return the persisted entity.
     */
    override fun save(experienceDTO: ExperienceDTO): ExperienceDTO {
        log.debug("Request to save Experience : {}", experienceDTO)

        var experience = experienceMapper.toEntity(experienceDTO)
        experience = experienceRepository.save(experience)
        return experienceMapper.toDto(experience)
    }

    /**
     * Get all the experiences.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    override fun findAll(): MutableList<ExperienceDTO> {
        log.debug("Request to get all Experiences")
        return experienceRepository.findAll()
            .mapTo(mutableListOf(), experienceMapper::toDto)
    }

    /**
     * Get one experience by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    override fun findOne(uid: String): Optional<ExperienceDTO> {
        log.debug("Request to get Experience : {}", uid)
        return experienceRepository.findById(uid)
            .map(experienceMapper::toDto)
    }

    /**
     * Delete the experience by id.
     *
     * @param id the id of the entity.
     */
    override fun delete(uid: String) {
        log.debug("Request to delete Experience : {}", uid)

        experienceRepository.deleteById(uid)
    }
}
