package dev.betternia.harmonia.person.support

import dev.betternia.harmonia.person.TagDTO
import dev.betternia.harmonia.person.TagMapper
import dev.betternia.harmonia.person.TagService
import dev.betternia.harmonia.person.repository.TagRepository
import java.util.Optional
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service Implementation for managing [Tag].
 */
@Service
@Transactional
class TagServiceImpl(
        private val tagRepository: TagRepository,
        private val tagMapper: TagMapper
) : TagService {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a tag.
     *
     * @param tagDTO the entity to save.
     * @return the persisted entity.
     */
    override fun save(tagDTO: TagDTO): TagDTO {
        log.debug("Request to save Tag : {}", tagDTO)

        var tag = tagMapper.toEntity(tagDTO)
        tag = tagRepository.save(tag)
        return tagMapper.toDto(tag)
    }

    /**
     * Get all the tags.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    override fun findAll(): MutableList<TagDTO> {
        log.debug("Request to get all Tags")
        return tagRepository.findAll()
            .mapTo(mutableListOf(), tagMapper::toDto)
    }

    /**
     * Get one tag by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    override fun findOne(uid: String): Optional<TagDTO> {
        log.debug("Request to get Tag : {}", uid)
        return tagRepository.findById(uid)
            .map(tagMapper::toDto)
    }

    /**
     * Delete the tag by id.
     *
     * @param id the id of the entity.
     */
    override fun delete(uid: String) {
        log.debug("Request to delete Tag : {}", uid)

        tagRepository.deleteById(uid)
    }
}
