package dev.betternia.harmonia.person.support

import dev.betternia.harmonia.person.CertificationDTO
import dev.betternia.harmonia.person.CertificationMapper
import dev.betternia.harmonia.person.CertificationService
import dev.betternia.harmonia.person.repository.CertificationRepository
import java.util.Optional
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service Implementation for managing [Certification].
 */
@Service
@Transactional
class CertificationServiceImpl(
        private val certificationRepository: CertificationRepository,
        private val certificationMapper: CertificationMapper
) : CertificationService {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a certification.
     *
     * @param certificationDTO the entity to save.
     * @return the persisted entity.
     */
    override fun save(certificationDTO: CertificationDTO): CertificationDTO {
        log.debug("Request to save Certification : {}", certificationDTO)

        var certification = certificationMapper.toEntity(certificationDTO)
        certification = certificationRepository.save(certification)
        return certificationMapper.toDto(certification)
    }

    /**
     * Get all the certifications.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    override fun findAll(): MutableList<CertificationDTO> {
        log.debug("Request to get all Certifications")
        return certificationRepository.findAll()
            .mapTo(mutableListOf(), certificationMapper::toDto)
    }

    /**
     * Get one certification by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    override fun findOne(uid: String): Optional<CertificationDTO> {
        log.debug("Request to get Certification : {}", uid)
        return certificationRepository.findById(uid)
            .map(certificationMapper::toDto)
    }

    /**
     * Delete the certification by id.
     *
     * @param id the id of the entity.
     */
    override fun delete(uid: String) {
        log.debug("Request to delete Certification : {}", uid)

        certificationRepository.deleteById(uid)
    }
}
