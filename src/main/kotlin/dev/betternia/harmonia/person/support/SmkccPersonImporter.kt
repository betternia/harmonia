package dev.betternia.harmonia.person.support

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.MappingIterator
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import dev.betternia.harmonia.foundation.FileFormat
import dev.betternia.harmonia.person.Person
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanWrapperImpl
import java.io.InputStream

fun <T : Any> castIfNotNull(obj: Any?) = if (obj == null) null else obj as T

class SmkccPersonImporter(
        val csvMapper: CsvMapper
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun parse(inputstream: InputStream, format: FileFormat, basePerson: Person = Person()): MutableList<Person> {
        val peopleMaps = deserializeIntoMaps(inputstream, FileFormat.CSV)
        return convertIntoPeople(peopleMaps, basePerson)
    }

    fun deserializeIntoMaps(input: InputStream, format: FileFormat): List<Map<String, Any>> {
        logger.debug("SmkccPersonImporter::deserialize (format: {})", format)

        val schema: CsvSchema = CsvSchema.emptySchema().withHeader()
        schema.withColumnSeparator(',')
        csvMapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN, true)


        val readValues: MappingIterator<Map<String, Any>> =
                csvMapper.readerFor(Map::class.java).with(schema).readValues(input)
        return readValues.readAll()
    }

    fun convertIntoPeople(records: List<Map<String, Any>>, basePerson: Person = Person()): MutableList<Person> {

        val people = mutableListOf<Person>()
        records.forEach { entry ->
            people.addAll(convertIntoPerson(entry, basePerson))
        }
        return people
    }

    /**
     * Return many persons. A row can consists of the head of family and sub member
     */
    fun convertIntoPerson(record: Map<String, Any>, basePerson: Person = Person()): MutableList<Person> {
//        record["ref"]
//        record["mas_group"]
        var people = mutableListOf<Person>()

        val hofFullName: String = castIfNotNull(record["Name_HOF"])
                ?: return people

        val hofName = hofFullName?.split(" ")
        val spouseFullName: String? = castIfNotNull(record["Name"])

        logger.debug("Processing ${hofFullName}...");
        val hofPerson = Person(
                refNum = castIfNotNull(record["ref"]),
                familyName = hofName!![0].trim(),
                givenName = if (hofName.size > 1) hofName[1].trim() else "",
                baptismalName = castIfNotNull(record["mas_mbname"]),
                phoneNumber = castIfNotNull(record["mas_hfon"]),
                addressLine = castIfNotNull(record["mas_addr"]),
                addressCity = castIfNotNull(record["mas_city"]),
                addressState = castIfNotNull(record["mas_state"]),
                addressZip = castIfNotNull(record["mas_zip"])
        )

        val group: String? = castIfNotNull(record["mas_group"])
        group?.let {
            hofPerson.tags = setOf(group)
        }

        people.add(overrideValues(hofPerson, basePerson))
        if (!spouseFullName.isNullOrBlank()) {
            val spouseName = spouseFullName.split(" ")
            val spouseNumber: String? = castIfNotNull(record["mas_bfon"])
            people.add(overrideValues(Person(
                    familyName = spouseName[0],
                    givenName = if (spouseName.size > 1) spouseName[1] else "",
                    baptismalName = castIfNotNull(record["mas_sbname"]),
                    tags = hofPerson.tags,
                    phoneNumber = castIfNotNull(record["mas_bfon"]),
                    addressLine = castIfNotNull(record["mas_addr"]),
                    addressCity = castIfNotNull(record["mas_city"]),
                    addressState = castIfNotNull(record["mas_state"]),
                    addressZip = castIfNotNull(record["mas_zip"]),
                    headOfFamily = hofPerson
            ), basePerson))
        }
        return people
    }

    private fun overrideValues(target: Person, template: Person): Person {
        val targetWrapper = BeanWrapperImpl(target)
        val templateWrapper = BeanWrapperImpl(template)
        val propDescriptors = templateWrapper.propertyDescriptors
        propDescriptors.iterator().forEach {
            val value = templateWrapper.getPropertyValue(it.name)
            if (value != null && it.propertyType.name != "java.lang.Class") {
                targetWrapper.setPropertyValue(it.name, value)
            }
        }

        return target
    }
}