package dev.betternia.harmonia.person.support

import dev.betternia.harmonia.person.OrgAffiliationDTO
import dev.betternia.harmonia.person.OrgAffiliationMapper
import dev.betternia.harmonia.person.OrgAffiliationService
import dev.betternia.harmonia.person.repository.OrgAffiliationRepository
import java.util.Optional
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

/**
 * Service Implementation for managing [OrgAffiliation].
 */
@Service
@Transactional
class OrgAffiliationServiceImpl(
        private val orgAffiliationRepository: OrgAffiliationRepository,
        private val orgAffiliationMapper: OrgAffiliationMapper
) : OrgAffiliationService {

    private val log = LoggerFactory.getLogger(javaClass)

    /**
     * Save a orgAffiliation.
     *
     * @param orgAffiliationDTO the entity to save.
     * @return the persisted entity.
     */
    override fun save(orgAffiliationDTO: OrgAffiliationDTO): OrgAffiliationDTO {
        log.debug("Request to save OrgAffiliation : {}", orgAffiliationDTO)

        var orgAffiliation = orgAffiliationMapper.toEntity(orgAffiliationDTO)
        orgAffiliation = orgAffiliationRepository.save(orgAffiliation)
        return orgAffiliationMapper.toDto(orgAffiliation)
    }

    /**
     * Get all the orgAffiliations.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    override fun findAll(): MutableList<OrgAffiliationDTO> {
        log.debug("Request to get all OrgAffiliations")
        return orgAffiliationRepository.findAll()
                .mapTo(mutableListOf(), orgAffiliationMapper::toDto)
    }

    /**
     * Get one orgAffiliation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    override fun findOne(uid: String): Optional<OrgAffiliationDTO> {
        log.debug("Request to get OrgAffiliation : {}", uid)
        return orgAffiliationRepository.findById(uid)
                .map(orgAffiliationMapper::toDto)
    }

    /**
     * Delete the orgAffiliation by id.
     *
     * @param id the id of the entity.
     */
    override fun delete(uid: String) {
        log.debug("Request to delete OrgAffiliation : {}", uid)

        orgAffiliationRepository.deleteById(uid)
    }
}
