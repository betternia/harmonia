package dev.betternia.harmonia.person.support

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import dev.betternia.harmonia.config.DEFAULT_PAGE_SIZE
import dev.betternia.harmonia.realm.RealmContextHolder
import dev.betternia.harmonia.security.ADMIN
import dev.betternia.harmonia.foundation.FileFormat
import dev.betternia.harmonia.foundation.ResourceNotFoundException
import dev.betternia.harmonia.person.*
import org.slf4j.LoggerFactory
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.lang.IllegalStateException
import java.net.URISyntaxException
import javax.validation.Valid

@RestController
@RequestMapping("/api/${PersonDTO.ENTITY_PLURAL_NAME}")
open class PersonResource(
        private val personService: PersonService,
        private val personMapper: PersonMapper
//        private val csvMapper: CsvMapper
) {
    private val log = LoggerFactory.getLogger(javaClass)

    private val csvMapper = CsvMapper()
    /**
     * `POST  /persons` : Create a new person.
     *
     * @param personDTO the personDTO to create.
     * @return the [ResponseEntity] with status `201 (Created)` and with body the new personDTO, or with status `400 (Bad Request)` if the person has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    fun createPerson(@Valid @RequestBody personDTO: PersonDTO): EntityModel<PersonDTO> {
        val realm = RealmContextHolder.getRealm() ?: throw IllegalStateException("Realm cannot be null")

        log.debug("REST called {}::createPerson (rid={}, Person={})", javaClass.simpleName, realm.id, personDTO)

        if (personDTO.uid != null) {
            throw IllegalArgumentException(
                    "A new person cannot already have an ID"
            )
        }

        personDTO.realmUid = realm.uid
        val saved = personService.save(personDTO)

        return wrapInEntityModel(saved)!!
    }

    /**
     * `PUT  /persons` : Updates an existing person.
     *
     * @param personDTO the personDTO to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated personDTO,
     * or with status `400 (Bad Request)` if the personDTO is not valid,
     * or with status `500 (Internal Server Error)` if the personDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{uid}")
    @ResponseStatus(HttpStatus.OK)
    fun updatePerson(@PathVariable uid: String, @Valid @RequestBody personDTO: PersonDTO): EntityModel<PersonDTO> {
        val realm = RealmContextHolder.getRealm() ?: throw IllegalStateException("Realm cannot be null")

        log.debug("REST called {}::updatePerson (rid={}, Person={})", javaClass.simpleName, realm?.id, personDTO)

        if (personDTO.uid == null) {
            throw IllegalArgumentException("Invalid id. ID cannot be null.")
        }

        personDTO.realmUid = realm.uid
        val saved = personService.save(personDTO)

        return wrapInEntityModel(saved)!!
    }

    /**
     * `GET  /persons` : get all the people.
     *
     * @param pageable the pagination information.
     * @return the [ResponseEntity] with status `200 (OK)` and the list of people in body.
     */
    @GetMapping("")
    @ResponseStatus(HttpStatus.OK)
    fun getAllPeople(
            criteriaDTO: PersonDTO?,
            pageable: Pageable
    ): PagedModel<EntityModel<PersonDTO>> {
        val realm = RealmContextHolder.getRealm() ?: throw IllegalStateException("Realm cannot be null")

        log.debug("REST called {}::getAllPeople (rid={})", javaClass.simpleName, realm?.uid)

        var criteria: Person? = if (criteriaDTO != null) personMapper.toEntity(criteriaDTO) else null

        val page = personService.queryAll(realm?.uid!!, criteria, pageable)
        val pageWithModels = page.content.map {
            wrapInEntityModel(it)
        }

        val pagedModel = PagedModel<EntityModel<PersonDTO>>(
                pageWithModels,
                PagedModel.PageMetadata(page.size.toLong(), page.number.toLong(), page.totalElements),
                linkTo(methodOn(PersonResource::class.java).getAllPeople(null, pageable)).withSelfRel()
        )

        return pagedModel
    }

    /**
     * `GET  /persons/:uid` : get the "uid" person.
     *
     * @param uid the id of the personDTO to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the personDTO, or with status `404 (Not Found)`.
     */
    @GetMapping("/{uid}")
    @ResponseStatus(HttpStatus.OK)
    open fun getPerson(@PathVariable uid: String): EntityModel<PersonDTO>? {
        val realm = RealmContextHolder.getRealm() ?: throw IllegalStateException("Realm cannot be null")

        log.debug("REST called {}::getPerson (rid={}, uid={})", javaClass.simpleName, realm.id, uid)

        val person = personService.findOne(uid) ?: throw ResourceNotFoundException(PersonDTO.ENTITY_NAME, uid)
        return wrapInEntityModel(person)
    }

    /**
     *  `DELETE  /persons/:uid` : delete the "uid" person.
     *
     * @param uid the id of the personDTO to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/{uid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deletePerson(@PathVariable uid: String) {
        log.debug("REST request to delete Person : {}", uid)
        personService.delete(uid)
    }

    @PostMapping("/import")
    @PreAuthorize("hasRole(\"$ADMIN\")")
    @ResponseStatus(HttpStatus.CREATED)
    fun import(@RequestParam("file") file: MultipartFile, @RequestParam("action") action: String?): Map<String, Int> {
        val realm = RealmContextHolder.getRealm() ?: throw IllegalStateException("Realm cannot be null")
        val importer = SmkccPersonImporter(csvMapper)
        val importedPeople = importer.parse(file.inputStream, FileFormat.CSV, Person().apply{realmUid=realm.uid})
        personService.save(importedPeople)

        return mapOf("count" to importedPeople.size);
    }

    // TODO: Use this
    fun wrapInEntityModel(person: Person?): EntityModel<PersonDTO>? {
        val pageRequest = PageRequest.of(0, DEFAULT_PAGE_SIZE)
        return person?.let {
            val userDto = PersonService.toDtoOneLevel(it)!!
            EntityModel<PersonDTO>(
                    userDto,
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(PersonResource::class.java).getPerson(userDto.uid!!)!!).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(PersonResource::class.java).getAllPeople(null, pageRequest)).withRel(PersonDTO.ENTITY_PLURAL_NAME)
            )
        }
    }
}