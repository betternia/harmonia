package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.realm.RealmBoundEntity
import org.hibernate.annotations.*
import org.hibernate.annotations.Cache
import java.time.LocalDate
import javax.persistence.*
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * The Person entity.
 */
@Entity
@Table(name = "person")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Person(

        @get: Size(max = 50)
        @Column(name = "user_uid", length = 50)
        var userUid: String? = null,

        @Column(name = "headline")
        var headline: String? = null,

        @Column(name = "synopsis")
        var synopsis: String? = null,

        @Column(name = "domain")
        var domain: String? = null,

        @get: Size(max = 255)
        @Column(name = "hometown", length = 255)
        var hometown: String? = null,

        @get: Size(max = 8)
        @Column(name = "gender", length = 8)
        var gender: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        @Column(name = "given_name", length = 120, nullable = false)
        var givenName: String? = null,

        @get: NotNull
        @get: Size(max = 120)
        @Column(name = "family_name", length = 120)
        var familyName: String? = null,

        @get: Size(max = 120)
        @Column(name = "nl_given_name", length = 120, nullable = false)
        var nlGivenName: String? = null,

        @get: Size(max = 120)
        @Column(name = "nl_family_name", length = 120)
        var nlFamilyName: String? = null,

        @get: Size(max = 120)
        @Column(name = "alternate_name", length = 120)
        var alternateName: String? = null,

        @get: Size(max = 120)
        @Column(name = "baptismal_name", length = 120)
        var baptismalName: String? = null,

        @Column(name = "birth_date")
        var birthDate: LocalDate? = null,

        @get: Size(max = 256)
        @Column(name = "birth_place", length = 256)
        var birthPlace: String? = null,

        @get: Size(max = 256)
        @Column(name = "email", length = 256)
        var email: String? = null,

        @get: Size(max = 60)
        @Column(name = "phone_number", length = 60)
        var phoneNumber: String? = null,

        @get: Size(max = 60)
        @Column(name = "mobile_number", length = 60)
        var mobileNumber: String? = null,

        @get: Size(max = 255)
        @Column(name = "address_line", length = 255)
        var addressLine: String? = null,

        @get: Size(max = 255)
        @Column(name = "address_street", length = 255)
        var addressStreet: String? = null,

        @get: Size(max = 120)
        @Column(name = "address_city", length = 120)
        var addressCity: String? = null,

        @get: Size(max = 120)
        @Column(name = "address_state", length = 120)
        var addressState: String? = null,

        @get: Size(max = 120)
        @Column(name = "address_zip", length = 120)
        var addressZip: String? = null,

        @get: Size(max = 50)
        @Column(name = "ref_num", length = 50)
        var refNum: String? = null,

//        @OneToMany(mappedBy = "headOfFamily", fetch = FetchType.LAZY)
//        @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
        @Transient
        @JsonIgnoreProperties("headOfFamily")
        var dependents: Set<Person> = setOf(),

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "head_of_family_uid")
        @JsonIgnoreProperties("dependents")
        var headOfFamily: Person? = null,

        @ElementCollection
        @CollectionTable(name = "person_tag",
                joinColumns = [JoinColumn(name = "person_uid", referencedColumnName = "uid")])
        @Fetch(FetchMode.JOIN)
        @Cascade(org.hibernate.annotations.CascadeType.DELETE)
        @Column(name = "tag")
        var tags: Set<String>? = null

) : RealmBoundEntity() {
//    fun addDependent(person: Person): Person {
//        this.dependents.add(person)
//        person.headOfFamily = this
//        return this
//    }
//
//    fun addDependents(people: Collection<Person>): Person {
//        this.dependents.addAll(people)
//        people.forEach { it.headOfFamily = this }
//        return this
//    }
//
//    fun removeDependent(person: Person): Person {
//        this.dependents.remove(person)
//        person.headOfFamily = null
//        return this
//    }

}
