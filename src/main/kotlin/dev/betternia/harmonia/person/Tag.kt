package dev.betternia.harmonia.person

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.foundation.EntityBase
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.ManyToOne
import javax.persistence.Table
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * A Tag.
 */
@Entity
@Table(name = "tag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Tag(

        // Eg. email
        @get: NotNull
        @get: Size(max = 20)
        @Column(name = "kind", length = 20, nullable = false)
        var kind: String? = null,

        // Eg. sample@test.com
        @get: NotNull
        @get: Size(max = 120)
        @Column(name = "name", length = 120, nullable = false)
        var name: String? = null,

        // Sorting order
        @get: Min(value = 0)
        @get: Max(value = 100)
        @Column(name = "weight")
        var weight: Int? = null,

        @ManyToOne
        @JsonIgnoreProperties("tags")
        var person: Person? = null

// jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
) : EntityBase() {}
