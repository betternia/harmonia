package dev.betternia.harmonia.person

import java.util.Optional

/**
 * Service Interface for managing [dev.betternia.harmonia.domain.OrgAffiliation].
 */
interface OrgAffiliationService {

    /**
     * Save a organization.
     *
     * @param orgAffiliationDTO the entity to save.
     * @return the persisted entity.
     */
    fun save(orgAffiliationDTO: OrgAffiliationDTO): OrgAffiliationDTO

    /**
     * Get all the organizations.
     *
     * @return the list of entities.
     */
    fun findAll(): MutableList<OrgAffiliationDTO>

    /**
     * Get the "uid" organization.
     *
     * @param uid the uid of the entity.
     * @return the entity.
     */
    fun findOne(uid: String): Optional<OrgAffiliationDTO>

    /**
     * Delete the "uid" organization.
     *
     * @param uid the uid of the entity.
     */
    fun delete(uid: String)
}
