package dev.betternia.harmonia.web.rest

import dev.betternia.harmonia.foundation.ResourceNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.lang.Nullable
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest


// From https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc#using-controlleradvice-classes
// Modified with https://www.baeldung.com/exception-handling-for-rest-with-spring#controlleradvice
// Fore more improvements on error handling see: https://dzone.com/articles/rest-api-error-handling-with-spring-boot
@ControllerAdvice
internal class GlobalDefaultExceptionHandler {

    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(value = [Exception::class])
    @Throws(Exception::class)
    fun defaultErrorHandler(req: HttpServletRequest, ex: Exception): ResponseEntity<Any> {
        val bodyOfResponse = "Unhandled Exception"

//        val attribs = loggingAttributes(req, ex)
        logger.warn(bodyOfResponse, ex)

        // The following block is to re-throw the error if is spring one. The effect is that Spring's
        // `ResponseEntityExceptionHandler` will catch it and do proper handling: log & return a JSON response.
        // Caveat: The ResponseEntityExceptionHandler will log again.
        //
        // The idea is based on this blog (https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc#using-controlleradvice-classes)
        // In the blog, the rethrow condition is based on the Annotation check, but the logic is porbably based on
        // old version of spring.

        // The suggestion of extending from `ResponseEntityExceptionHandler` did not do the trick.
        // Extending just ignores `ExceptionHandler`s in it.
        // https://stackoverflow.com/questions/48991353/how-to-catch-all-unhandled-exceptions-i-e-without-existing-exceptionhandler
        if (ex.javaClass.name.startsWith("org.springframework")) {
            throw ex
        }

        return ResponseEntity(null, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(value = [ResourceNotFoundException::class])
    @Throws(Exception::class)
    fun resourceNotFoundHandler(req: HttpServletRequest, ex: Exception): ResponseEntity<Any> {
        return handleExceptionInternal(ex, null, HttpHeaders(), HttpStatus.NOT_FOUND, req)
    }

    protected fun handleExceptionInternal(ex: Exception?, @Nullable body: Any?, headers: HttpHeaders?, status: HttpStatus, request: HttpServletRequest): ResponseEntity<Any> {

        return ResponseEntity(body, headers, status)
    }

    fun loggingAttributes(request: HttpServletRequest, ex: Throwable): Map<String, Any> {
        return mapOf<String, Any>(
                "path" to request.requestURI,
                "error.message" to ex.message!!,
                "error.stack_trace" to ex.stackTrace,
                "error.type" to ex.javaClass.canonicalName
        )
    }
}