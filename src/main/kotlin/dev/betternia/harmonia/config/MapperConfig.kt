package dev.betternia.harmonia.config

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDate
import java.util.*


@Configuration
class MapperConfig {
//    @Bean
    fun objectMapper(): ObjectMapper {
        val mapper = ObjectMapper()
        mapper.registerModule(JavaTimeModule())
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        // https://github.com/FasterXML/jackson-databind/issues/1786#issuecomment-475912741
//        mapper.configOverride(Date::class.java).format = JsonFormat.Value.forPattern("yyyy-dd-MM")
//        mapper.configOverride(LocalDate::class.java).format = JsonFormat.Value.forPattern("yyyy-dd-MM")
//        mapper.dateFormat = ISO8601DateFormat()
//        mapper.findAndRegisterModules()

        return mapper
    }

//    @Bean
    fun csvMapper(): CsvMapper {
        val mapper = CsvMapper()
        mapper.configOverride(Date::class.java).format = JsonFormat.Value.forPattern("yyyy.dd.MM")

//        mapper.dateFormat = ISO8601DateFormat()
//        mapper.findAndRegisterModules()

        return mapper
    }


}