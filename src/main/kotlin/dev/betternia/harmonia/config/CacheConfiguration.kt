package dev.betternia.harmonia.config

import dev.betternia.harmonia.user.Authority
import dev.betternia.harmonia.user.User
import dev.betternia.harmonia.user.UserRepository
import java.time.Duration
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.jsr107.Eh107Configuration
//import org.hibernate.cache.jcache.ConfigSettings
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableCaching
class CacheConfiguration(
        @Value("\${ehcache.maxEntries:1000}")
        val maxEntries: Long,
        @Value("\${ehcache.timeToLiveSeconds:3600}")
        val timeToLiveSeconds: Long
) {

    private val jcacheConfiguration: javax.cache.configuration.Configuration<Any, Any>

    init {

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(
                Any::class.java, Any::class.java,
                ResourcePoolsBuilder.heap(maxEntries)
            )
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(timeToLiveSeconds)))
                .build()
        )
    }

//    @Bean
//    fun hibernatePropertiesCustomizer(cacheManager: javax.cache.CacheManager) = HibernatePropertiesCustomizer {
//        hibernateProperties -> hibernateProperties[ConfigSettings.CACHE_MANAGER] = cacheManager
//    }

    @Bean
    fun cacheManagerCustomizer(): JCacheManagerCustomizer {
        return JCacheManagerCustomizer { cm ->
            createCache(cm, UserRepository.USERS_BY_USERNAME_CACHE)
            createCache(cm, UserRepository.USERS_BY_EMAIL_CACHE)
            createCache(cm, User::class.java.name)
            createCache(cm, Authority::class.java.name)
            createCache(cm, User::class.java.name + ".authorities")
            // jhipster-needle-ehcache-add-entry
        }
    }

    private fun createCache(cm: javax.cache.CacheManager, cacheName: String) {
        val cache: javax.cache.Cache<Any, Any>? = cm.getCache(cacheName)
        if (cache != null) {
            cm.destroyCache(cacheName)
        }
        cm.createCache(cacheName, jcacheConfiguration)
    }
}
