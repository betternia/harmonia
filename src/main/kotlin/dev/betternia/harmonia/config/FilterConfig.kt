package dev.betternia.harmonia.config

import dev.betternia.harmonia.realm.RealmLoaderFilter
import dev.betternia.harmonia.realm.RealmService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class FilterConfig(
        var  realmService: RealmService
) : WebMvcConfigurer {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Bean
    fun loadRealmFilterRegistration(): FilterRegistrationBean<RealmLoaderFilter>
    {
        var registration = FilterRegistrationBean<RealmLoaderFilter>(loadRealmFilter())
        registration.setName("RealmLoaderFilter")
        registration.addUrlPatterns("/api/*")
        // Url patterns should be the simple format, it does not support the ant pattern
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE)

        logger.info ("RealmLoaderFilter registered!!")

        return registration
    }

    @Bean
    fun loadRealmFilter() : RealmLoaderFilter {
        return RealmLoaderFilter(realmService)
    }
}
