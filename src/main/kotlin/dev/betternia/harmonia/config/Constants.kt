package dev.betternia.harmonia.config

import org.springframework.data.domain.PageRequest

// Regex for acceptable userNames
const val ID_REGEX: String = "^[_.@A-Za-z0-9-]*\$"

const val USERNAME_REGEX: String = "^[_.@A-Za-z0-9-]*\$"
const val SYSTEM_ACCOUNT: String = "system"
const val ANONYMOUS_USER: String = "anonymoususer"
const val DEFAULT_LANGUAGE: String = "en"

const val DEFAULT_PAGE_SIZE: Int = 10

val DEFAULT_PAGE_REQUEST = PageRequest.of(0, DEFAULT_PAGE_SIZE)
