package dev.betternia.harmonia.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.web.cors.CorsConfiguration

@ConfigurationProperties(prefix = "harmonia", ignoreUnknownFields = false)
data class HarmoniaConfigProperties (
        var cors: CorsConfiguration = CorsConfiguration()
) {
}