package dev.betternia.harmonia.realm

import dev.betternia.harmonia.foundation.EntityMapper
import org.mapstruct.Mapper

/**
 * Mapper for the entity [Realm] and its DTO [RealmDTO].
 */
@Mapper(componentModel = "spring", uses = [])
interface RealmMapper :
        EntityMapper<RealmDTO, Realm> {
    override fun toDto(entity: Realm): RealmDTO

    fun fromUid(uid: String?) = uid?.let { Realm().apply { this.uid = uid } }
}
