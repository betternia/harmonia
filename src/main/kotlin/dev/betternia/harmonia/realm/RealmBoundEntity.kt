package dev.betternia.harmonia.realm

import dev.betternia.harmonia.foundation.EntityBase
import javax.persistence.Column
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class RealmBoundEntity(
        @Column(name = "realm_uid")
        var realmUid: String?= null
): EntityBase() {
}