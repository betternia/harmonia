package dev.betternia.harmonia.realm

import org.springframework.beans.BeanUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class RealmServiceImpl @Autowired constructor(
        val realmRepository: RealmRepository
) : RealmService {

    @Transactional(readOnly = true)
    override fun findAll(types: Set<String>?, page: Pageable): Page<Realm> {

        val sorting = if (page?.sort != null && page.sort.isSorted) page.sort else Sort.by(Sort.Direction.ASC, "name")
        var paging = if (page != null) PageRequest.of(page.pageNumber, page.pageSize, sorting)
        else PageRequest.of(0, 10, sorting)

        return realmRepository.findAll(paging)
    }

    @Transactional(readOnly = true)
    override fun find(realmUid: String): Realm? {
        return realmRepository.findByUid(realmUid)
    }

    @Transactional(readOnly = true)
    override fun findById(realmId: String): Realm? {
        return realmRepository.findById(realmId)
    }

    override fun add(realm: Realm): Realm {
        realm.sid = null
        realm.uid = null
        val savedRealm = realmRepository.save(realm)

        realmRepository.save(savedRealm)

        return savedRealm
    }

    override fun update(realm: Realm): Realm {
        if (realm.uid == null) {
            throw IllegalArgumentException("UID not provided")
        }

        var foundRealm = realmRepository.findByUid(realm.uid!!)
                ?: throw IllegalStateException("Realm [$realm.uid] Not Found")
        BeanUtils.copyProperties(realm, foundRealm, "sid", "uid", "createdAt", "updatedAt")

        return realmRepository.save(foundRealm)
    }

    override fun deleteByUid(realmUid: String): Realm {
        var foundRealm = realmRepository.findByUid(realmUid) ?: throw IllegalStateException("Not Found: UID[$realmUid]")

        realmRepository.delete(foundRealm)

        return foundRealm
    }
}