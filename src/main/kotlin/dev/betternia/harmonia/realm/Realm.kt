package dev.betternia.harmonia.realm

import dev.betternia.harmonia.config.ID_REGEX
import dev.betternia.harmonia.foundation.EntityBase
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Entity
@Table(name = "realm")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
data class Realm(
        @NotNull
        @field:Pattern(regexp = ID_REGEX)
        @field:Size(min = 3, max = 50)
        @Column(name = "id", length = 50, unique = true, nullable = false)
        var id: String? = null, // Unique id that will be used for this realm's slug

        @field:NotNull
        @field:Size(min = 4, max = 255)
        @Column(name = "name")
        var name: String? = null,

        @Column(name = "synopsis")
        var synopsis: String? = null,

        @Column(name = "description")
        var description: String? = null,

        @Column(name = "type")
        var type: String? = null, // Learning

        @Column(name = "status")
        var status: String? = null, // active, inactive

        @Column(name = "audience")
        var audience: String? = null,

        @Column(name = "location")
        var location: String? = null,

        @Column(name = "country")
        var country: String? = null,

        @Column(name = "language")
        var language: String? = null,

        @Column(name = "thumbnail_image")
        var thumbnailImage: String? = null, // square image used on icons. Up to 300 x 300

        @Column(name = "cover_image")
        var coverImage: String? = null, // rectangular image used for cover.

        @Column(name = "properties")
        var properties: String? = null // Additional properties (settings) in JSON
) : EntityBase() {}