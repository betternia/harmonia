package dev.betternia.harmonia.realm

import org.slf4j.LoggerFactory
import org.springframework.util.AntPathMatcher
import java.lang.IllegalStateException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest

class RealmLoaderFilter(val realmService: RealmService) : Filter {

    private val logger = LoggerFactory.getLogger(javaClass)

    companion object {
        const val REALM_ATTRIB = "_realm_"
        const val REALM_ID = "realmId"
        const val PATH_PATTERN = "/api/v1/{$REALM_ID:\\w+}/**"
    }

    override fun init(filterConfig: FilterConfig?) {
        super.init(filterConfig)
        logger.info("RealmLoaderFilter.init called")
    }


    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {

        logger.info("RealmLoaderFilter.doFilter called")
        val realmId = extractRealmIdFromPath(request!!) ?: extractRealmIdFromQueryString(request!!)

        // "realms" is the prefix
        if (realmId != null && realmId != "realms") {
            val realm = realmService.findById(realmId) ?:
                throw IllegalStateException("Realm [$realmId] not found")

            RealmContextHolder.setRealm(realm!!)
            request!!.setAttribute(REALM_ATTRIB, realm)
        }

        chain!!.doFilter(request, response)
    }


    private fun extractRealmIdFromPath(request: ServletRequest): String? {

        val req = request as HttpServletRequest
        val apMatcher = AntPathMatcher()

        // TODO: return null if the pathParameter is actually "realms"
        return if (apMatcher.match(PATH_PATTERN, req.servletPath))
            apMatcher.extractUriTemplateVariables(PATH_PATTERN, req.servletPath).getOrDefault(REALM_ID, null)
        else null
    }

    private fun extractRealmIdFromQueryString(request: ServletRequest): String? {

        val req = request as HttpServletRequest
        val apMatcher = AntPathMatcher()

        val realmId = req.getParameterValues(REALM_ID)
        return if (realmId != null)
            realmId[0]
        else null
    }
}