package dev.betternia.harmonia.realm

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.querydsl.QuerydslPredicateExecutor
import org.springframework.stereotype.Repository

@Repository
interface RealmRepository : JpaRepository<Realm, Long> {
    fun findByType(type: String): List<Realm>

    fun findByUid(uid: String): Realm?

    fun findById(id: String): Realm?
}