package dev.betternia.harmonia.realm

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import dev.betternia.harmonia.config.ID_REGEX
import dev.betternia.harmonia.foundation.EntityBase
import org.springframework.hateoas.server.core.Relation
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Relation(collectionRelation = RealmDTO.ENTITY_PLURAL_NAME) // For HATEOAS, to produce _embedded/users
@JsonIgnoreProperties(ignoreUnknown = true)
data class RealmDTO(
        @field:NotBlank
        @field:Pattern(regexp = ID_REGEX)
        @field:Size(min = 3, max = 50)
        var id: String? = null, // Unique id that will be used for this realm's slug

        @field:NotNull
        @field:Size(min = 4, max = 255)
        var name: String? = null,

        var synopsis: String? = null,

        var description: String? = null,

        var type: String? = null, // Learning

        var status: String? = null, // active, inactive

        var audience: String? = null,

        var location: String? = null,

        var country: String? = null,

        var language: String? = null,

        var thumbnailImage: String? = null, // square image used on icons. Up to 300 x 300

        var coverImage: String? = null, // rectangular image used for cover.

        var properties: String? = null // Additional properties (settings) in JSON
) : EntityBase() {
    companion object {
        const val ENTITY_NAME = "realm"
        const val ENTITY_PLURAL_NAME = "realms"
    }
}