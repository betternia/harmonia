package dev.betternia.harmonia.realm

import dev.betternia.harmonia.config.DEFAULT_PAGE_SIZE
import dev.betternia.harmonia.security.ADMIN
import dev.betternia.harmonia.foundation.ResourceNotFoundException
import dev.betternia.harmonia.user.UserResource
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.web.PageableDefault
import org.springframework.hateoas.EntityModel
import org.springframework.hateoas.PagedModel
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/${RealmDTO.ENTITY_PLURAL_NAME}")
class RealmController (
        private val realmService: RealmService,
        private val realmMapper: RealmMapper
) {

    @GetMapping(value = [""], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRealms(@RequestParam(required = false) types: Set<String>?,
                  @PageableDefault(size = 20) pageable: Pageable): PagedModel<EntityModel<RealmDTO>> {

        val page = realmService.findAll(types, pageable)

        val pageWithModels = page.content.map {
            wrapInEntityModel(it)
        }

        val pagedModel = PagedModel<EntityModel<RealmDTO>>(
                pageWithModels,
                PagedModel.PageMetadata(page.size.toLong(), page.number.toLong(), page.totalElements),
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(UserResource::class.java).getAllUsers(pageable)).withSelfRel()
        )

        return pagedModel
    }

    @GetMapping(value = ["/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRealm(@PathVariable id: String): EntityModel<RealmDTO>? {
        val realm = realmService.findById(id) ?: realmService.find(id)
            ?: throw ResourceNotFoundException(RealmDTO.ENTITY_NAME, id)

        return wrapInEntityModel(realm)
    }

    @PostMapping(value = [""], produces = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseStatus(HttpStatus.CREATED)
    fun addRealm(@RequestBody realm: Realm): EntityModel<RealmDTO> {
        return wrapInEntityModel(realmService.add(realm))!!
    }

    @PutMapping(value = ["/{uid}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("hasRole(\"$ADMIN\")")
    fun updateRealm(@PathVariable uid: String, @RequestBody realm: Realm): EntityModel<RealmDTO>? {
        realm.uid = uid
        return wrapInEntityModel(realmService.update(realm))
    }

    @DeleteMapping(value = ["/{uid}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @PreAuthorize("hasRole(\"$ADMIN\")")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteRealm(@PathVariable uid: String): Realm {
        return realmService.deleteByUid(uid)
    }

    fun wrapInEntityModel(realm: Realm?): EntityModel<RealmDTO>? {
        val pageRequest = PageRequest.of(0, DEFAULT_PAGE_SIZE)
        return realm?.let {
            val realmDto = realmMapper.toDto(it)
            EntityModel<RealmDTO>(
                    realmDto,
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(RealmController::class.java).getRealm(realmDto.uid!!)!!).withSelfRel(),
                    WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(RealmController::class.java).getRealms(null, pageRequest)).withRel(RealmDTO.ENTITY_PLURAL_NAME)
            )
        }
    }
}