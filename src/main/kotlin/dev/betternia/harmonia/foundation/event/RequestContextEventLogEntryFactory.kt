package dev.betternia.harmonia.foundation.event

import org.springframework.security.core.context.SecurityContextHolder
import java.util.*


object RequestContextEventLogEntryFactory {

    var context: ThreadLocal<LinkedHashMap<String, Any?>>
            = ThreadLocal.withInitial(({ LinkedHashMap<String, Any?>() }))

    fun createLogEvent(customAttrs: Map<String, Any?> ): EventLogEntry {

        var eventLogEntry = EventLogEntry()

        val auth = SecurityContextHolder.getContext().authentication
        if (auth != null) {
            val user = auth.principal
            eventLogEntry.userId(auth.name)
        }

        return eventLogEntry
    }
}