package dev.betternia.harmonia.foundation.event

class EventLogEntry {
    var map = LinkedHashMap<String, Any?>()


    fun userId(value: String): EventLogEntry {
        return put("user-id", value)
    }


    fun custom(key: String, value: Any?): EventLogEntry {
        this.map ["custom.$key"] = value
        return this
    }

    fun custom(fields: Map<String, Any?>): EventLogEntry {
        fields.forEach { (key: String, value: Any?) -> this.custom(key, value) }
        return this
    }

    private fun put(attrName: String, value: Any): EventLogEntry {
        map[attrName] = value
        return this
    }

}