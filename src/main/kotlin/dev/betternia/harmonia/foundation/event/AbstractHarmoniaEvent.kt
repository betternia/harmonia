package dev.betternia.harmonia.foundation.event

import org.springframework.context.ApplicationEvent

// Inheriting from ApplicationEvent we can then add listener, e.g.: ApplicationListener<AbstractUaaEvent> {
abstract class AbstractHarmoniaEvent(source: Any) : ApplicationEvent(source) {

}