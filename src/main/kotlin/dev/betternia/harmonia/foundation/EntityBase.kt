package dev.betternia.harmonia.foundation

import org.hibernate.annotations.GenericGenerator
import java.io.Serializable
import java.util.*
import javax.persistence.*

@MappedSuperclass
open class EntityBase (

        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        @Column(name = "sid", insertable = false, updatable = false, nullable = false)
        var sid: Long? = null,

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(
                name = "UUID",
                strategy = "org.hibernate.id.UUIDGenerator"
        )
        @Column(name = "uid", updatable = false, nullable = false)
        var uid: String? = null

) : Serializable {

    @PrePersist
    fun prePersist() {
        if (uid == null) {
            uid = UUID.randomUUID().toString()
        }
    }

}
