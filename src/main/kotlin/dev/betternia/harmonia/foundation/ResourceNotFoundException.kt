package dev.betternia.harmonia.foundation

class ResourceNotFoundException (
        val resourceType: String,
        val resourceId: String
): RuntimeException("Resource not found ${resourceType}/${resourceId}") {
}