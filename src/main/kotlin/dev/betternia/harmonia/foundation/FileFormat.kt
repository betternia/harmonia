package dev.betternia.harmonia.foundation

enum class FileFormat(val format: String) {
    JSON("json"),
    CSV("csv"),
    XML("xml")
}