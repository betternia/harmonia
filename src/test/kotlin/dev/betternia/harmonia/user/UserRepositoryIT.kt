package dev.betternia.harmonia.user

import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.apache.commons.lang3.RandomStringUtils
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension

// JUnit5: RunWith replaced with ExtendWith
@ExtendWith(SpringExtension::class)

@DataJpaTest
class UserRepositoryIT: HarmoniaTest {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Test
    fun `Should CRUD User`() {
        userRepository.save(newUser("MOCK_USERNAME"))
        var foundUser = userRepository.findOneByUserName("MOCK_USERNAME")

        Assertions.assertThat(foundUser).isNotNull

        userRepository.deleteById(foundUser?.uid!!)

        var foundUser2 = userRepository.findOneByUserName("MOCK_USERNAME")
        Assertions.assertThat(foundUser2).isNull()
    }

    companion object {
        fun newUser(userName: String): User {
            return User(userName = userName, password = RandomStringUtils.random(60)).apply{createdBy = "TEST"}
        }
    }
}