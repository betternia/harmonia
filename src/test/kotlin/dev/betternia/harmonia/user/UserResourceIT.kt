package dev.betternia.harmonia.user

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.betternia.harmonia.user.UserDTO
import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class UserResourceIT : HarmoniaTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser(value = "test-admin", roles=["ADMIN"])
    fun `Should Create a User`() {

        val userJson = "{\"userName\" : \"testuser\",\n" +
                "  \"firstName\" : \"Tess\",\n" +
                "  \"lastName\" : \"Tester\",\n" +
                "  \"email\" : \"test@localhost\",\n" +
                "  \"imageUrl\" : \"\",\n" +
                "  \"langKey\" : \"en\",\n" +
                "  \"authorities\" : [ \"ROLE_USER\", \"ROLE_ADMIN\" ]}"

        this.mockMvc.perform(post("/api/users").contentType(MediaType.APPLICATION_JSON_VALUE).content(userJson ))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.uid").isString)

        this.mockMvc.perform(get("/api/users/byusername/testuser"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.uid").isString)
                .andExpect(jsonPath("$.userName", `is`("testuser")))
    }

    @Test
    @WithMockUser(value = "test-user", roles=["USER"])
    fun `Should Fetch a User`() {
        this.mockMvc.perform(get("/api/users/byusername/admin"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.uid").isString)

    }

    @Test
    @WithMockUser(value = "test-user", roles=["ADMIN"])
    fun `Should Update a User`() {

        val userUid = "335a4479-e902-4472-958f-eec28edc2719"
        val getResultActions: ResultActions = mockMvc.perform(
                get("/api/users/${userUid}"))
//                .andExpect(status().isOk())
        val response = getResultActions.andReturn().response.contentAsString

        val mapper = jacksonObjectMapper()
        mapper.registerModule(JavaTimeModule())
        val user = mapper.readValue<UserDTO>(response)

        val newVal = "I-AM-NEW"
        user.firstName = newVal

//        System.out.println (user.toString())

        this.mockMvc.perform(put("/api/users/${userUid}").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(user)) )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.firstName").value(newVal))


        this.mockMvc.perform(get("/api/users/${userUid}"))
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.firstName").value(newVal))

    }

    @Test
    @WithMockUser
    fun `Should Fetch User List`() {
        this.mockMvc.perform(get("/api/users"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.users").isArray)

    }
}