package dev.betternia.harmonia.test.utils

import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("dev")
interface HarmoniaTest {
}