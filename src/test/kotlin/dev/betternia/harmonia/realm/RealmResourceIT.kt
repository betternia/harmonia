package dev.betternia.harmonia.realm

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class RealmResourceIT : HarmoniaTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `Should Fetch a Realm`() {
        this.mockMvc.perform(get("/api/realms/host"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.uid").isString)
                .andExpect(jsonPath("$.id").value("host"))

    }

    @Test
    @WithMockUser(value = "test-user", roles=["ADMIN"])
    fun `Should Create and Delete a Realm`() {
        val mapper = jacksonObjectMapper()
        mapper.registerModule(JavaTimeModule())

        val name = "Dummy Realm"
        val realm = RealmDTO(id = "dummy_realm", name="Dummy Realm", synopsis = "Test", description = "Testing")

        val getResultActions: ResultActions = this.mockMvc.perform(post("/api/realms?realmId=host").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(realm)) )
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andExpect(jsonPath("$.name").value(name))
        val response = getResultActions.andReturn().response.contentAsString
        val createdRealm = mapper.readValue<RealmDTO>(response)

        this.mockMvc.perform(get("/api/realms/" + createdRealm.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.name").value(name))

        this.mockMvc.perform(delete("/api/realms/" + createdRealm.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        this.mockMvc.perform(get("/api/realms/" + createdRealm.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)

    }

    @Test
    @WithMockUser(value = "test-user", roles=["ADMIN"])
    fun `Should Update a Realm`() {
        val getResultActions: ResultActions = mockMvc.perform(
                get("/api/realms/host"))
        val response = getResultActions.andReturn().response.contentAsString

        val mapper = jacksonObjectMapper()
        mapper.registerModule(JavaTimeModule())
        val realm = mapper.readValue<RealmDTO>(response)

        val newVal = "I-AM-MODIFIED"
        realm.synopsis = newVal

//        System.out.println (realm.toString())

        this.mockMvc.perform(put("/api/realms/host").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(realm)) )
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))


        this.mockMvc.perform(get("/api/realms/host"))
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))

    }

    @Test
    @WithMockUser
    fun `Should Fetch Realm List`() {
        this.mockMvc.perform(get("/api/realms?realmId=host"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.realms").isArray)

    }
}