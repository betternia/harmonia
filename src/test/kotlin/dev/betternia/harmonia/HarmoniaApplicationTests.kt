package dev.betternia.harmonia

import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class HarmoniaApplicationTests: HarmoniaTest {

    @Test
    fun contextLoads() {
    }

}
