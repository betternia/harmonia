package dev.betternia.harmonia.person

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.hamcrest.Matchers
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class PersonResourceIT : HarmoniaTest {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `Should Fetch a Person`() {
        this.mockMvc.perform(get("/api/persons/1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.uid").isString)
                .andExpect(jsonPath("$.realmUid").value("host"))

    }

    @Test
    @WithMockUser(value = "test-user", roles = ["ADMIN"])
    fun `Should Create and Delete a Person`() {
        val mapper = jacksonObjectMapper()
        mapper.registerModule(JavaTimeModule())

        val givenName = "Doe"
        val person = PersonDTO(headline = "Created in test", synopsis = "Test",
                givenName = givenName, familyName = "Tester", email = "test@tester.com"
        )
        person.tags = setOf("T1", "T2")

        val getResultActions: ResultActions = this.mockMvc.perform(post("/api/persons?realmId=host").contentType(MediaType.APPLICATION_JSON_VALUE).content(mapper.writeValueAsString(person)))
                .andExpect(MockMvcResultMatchers.status().isCreated)
                .andExpect(jsonPath("$.givenName").value(givenName))
        val response = getResultActions.andReturn().response.contentAsString
        val createdPerson = mapper.readValue<PersonDTO>(response)

        this.mockMvc.perform(get("/api/persons/" + createdPerson.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(jsonPath("$.givenName").value(givenName))
                .andExpect(jsonPath<Collection<Any>>("$.tags", hasSize(2)))
                .andExpect(jsonPath<Iterable<String>>("$.tags", hasItems("T1", "T2")))

        this.mockMvc.perform(delete("/api/persons/" + createdPerson.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isNoContent)

        this.mockMvc.perform(get("/api/persons/" + createdPerson.uid + "?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isNotFound)

    }

    @Test
    @WithMockUser(value = "test-user", roles = ["ADMIN"])
    fun `Should Update a Person`() {
        val getResultActions: ResultActions = mockMvc.perform(
                get("/api/persons/1?realmId=host"))
//                .andExpect(status().isOk())
        val response = getResultActions.andReturn().response.contentAsString

        val mapper = jacksonObjectMapper()
        mapper.registerModule(JavaTimeModule())
        val person = mapper.readValue<PersonDTO>(response)

        val newVal = "I-AM-MODIFIED"
        person.synopsis = newVal

        // Add dependents and update

        var dependents = arrayOf(
                PersonDTO(familyName = "Smith", givenName = "Joe", baptismalName = "John"),
                PersonDTO(familyName = "Doe", givenName = "Jane", baptismalName = "Johana")
        )
        person.dependents.addAll(dependents)

        person.tags = setOf("T1", "T2")

        this.mockMvc.perform(put("/api/persons/1?realmId=host").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(person)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))

        var mvcResult = this.mockMvc.perform(get("/api/persons/1?realmId=host"))
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))
                .andExpect(jsonPath("$.dependents").isArray)
                .andExpect(jsonPath<Collection<Any>>("$.tags", hasSize(2)))
                .andExpect(jsonPath<Collection<Any>>("$.dependents", hasSize(2)))
                .andExpect(jsonPath<Iterable<String>>("$.dependents[*].familyName", hasItems("Smith", "Doe")))
                .andReturn()

        var updatedPerson = mapper.readValue(mvcResult.getResponse().getContentAsString(), PersonDTO::class.java);

        // Modify dependents and update

        updatedPerson.dependents.elementAt(0).givenName = "Julian"
        updatedPerson.dependents.elementAt(1)._action = "d"

//        var dependentsModif = arrayOf(
//                PersonDTO(familyName = "Smith", givenName = "updatedPerson", baptismalName = "John").apply { uid = updatedPerson.dependents.elementAt(0).uid },
//                PersonDTO(familyName = "Doe", givenName = "Jane", baptismalName = "Johana", _action = "d").apply { uid = updatedPerson.dependents.elementAt(1).uid }
//        )
//        person.dependents.clear()
//        person.dependents.addAll(dependentsModif)

        this.mockMvc.perform(put("/api/persons/1?realmId=host").contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(updatedPerson)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))

        this.mockMvc.perform(get("/api/persons/1?realmId=host"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.synopsis").value(newVal))
                .andExpect(jsonPath("$.dependents").isArray)
                .andExpect(jsonPath<Collection<Any>>("$.dependents", hasSize(1)))
                .andExpect(jsonPath<Iterable<String>>("$.dependents[*].givenName", hasItems("Julian")))

    }

    @Test
    @WithMockUser
    fun `Should Fetch Person List`() {
        this.mockMvc.perform(get("/api/persons?realmId=host"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.persons").isArray)

    }

    @Test
    @WithMockUser
    fun `Should filter People`() {
        this.mockMvc.perform(get("/api/persons?realmId=host&familyName=Smi%"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$._embedded.persons").isArray)
                .andExpect(jsonPath<Iterable<String>>("$._embedded.persons[*].familyName", hasItems("Smith")))

    }
}