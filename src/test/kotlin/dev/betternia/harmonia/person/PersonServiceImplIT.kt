package dev.betternia.harmonia.person

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import dev.betternia.harmonia.foundation.FileFormat
import dev.betternia.harmonia.person.repository.PersonRepository
import dev.betternia.harmonia.person.support.PersonServiceImpl
import dev.betternia.harmonia.person.support.SmkccPersonImporter
import dev.betternia.harmonia.realm.Realm
import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.context.annotation.Import
import org.springframework.data.domain.PageRequest
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@Import(PersonMapperImpl::class)
@DataJpaTest
class PersonServiceImplIT: HarmoniaTest {

    @Autowired
    lateinit var entityManager: TestEntityManager

    @Autowired
    lateinit var personRepository: PersonRepository
    @Autowired
    lateinit var personMapper: PersonMapper

    @Test
    fun `Should bulk save`() {
        val mapper = CsvMapper()
        val sut = PersonServiceImpl(personRepository, personMapper)
        val importer = SmkccPersonImporter(mapper)

        var testFile = "/people.test.csv"

        val REALM_UID = "sample"
        this.javaClass.getResourceAsStream(testFile).use { inputstream ->
            val people = importer.parse(inputstream, FileFormat.CSV, Person().apply{realmUid=REALM_UID})

            val savedPeople = sut.save(people)
            assertThat(savedPeople).hasSize(10)

            val foundPeople = sut.findAll(REALM_UID, null, PageRequest.of(0, 99))

            assertThat(foundPeople).hasSize(10)
        }
    }

    @Test
    fun `Should filter by field`() {
        val persons = populate();
        val sut = PersonServiceImpl(personRepository, personMapper)

        val criteria = Person(givenName = "GN1")

        val result = sut.queryAll(persons[0].realmUid!!, criteria, PageRequest.of(0, 20))

        assertThat(result).hasSize(2)
    }

    @Test
    fun `Should filter by tag`() {
        val persons = populate();
        val sut = PersonServiceImpl(personRepository, personMapper)

        val criteria = Person(tags = setOf("T1b"))

        val result = sut.queryAll(persons[0].realmUid!!, criteria, PageRequest.of(0, 20))

        assertThat(result).hasSize(2)
        result.forEach {
            assertThat(it.tags).containsAnyOf("T1b")
        }
    }

    fun populate(): List<Person> {
        val realm = Realm(id="MOCK_REALM", name = "MOCK")
        val savedRealm = entityManager.persist(realm)

        val dummyPersons = listOf(
                Person(givenName = "GN1", familyName = "FN1", tags = setOf("T1a", "T1b")).apply { realmUid = savedRealm.uid },
                Person(givenName = "GN1", familyName = "FN2", tags = setOf("T2a", "T1b")).apply { realmUid = savedRealm.uid },
                Person(givenName = "GN2", familyName = "FN3", tags = setOf("T2a", "T2b")).apply { realmUid = savedRealm.uid }
        )
        val saved = dummyPersons.map {
            entityManager.persist(it)
        }
        return saved
    }

}