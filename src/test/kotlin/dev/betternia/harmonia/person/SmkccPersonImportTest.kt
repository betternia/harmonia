package dev.betternia.harmonia.person

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import dev.betternia.harmonia.foundation.FileFormat
import dev.betternia.harmonia.person.support.SmkccPersonImporter
import dev.betternia.harmonia.test.utils.HarmoniaTest
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import java.io.File
import java.io.InputStream

class SmkccPersonImportTest : HarmoniaTest {


    @Test
    fun `Should Deserialize from CSV`() {
        val mapper = CsvMapper()
        val sut = SmkccPersonImporter(CsvMapper())

        var testFile = "/people.test.csv"

        val realmId = "sample"
        this.javaClass.getResourceAsStream(testFile).use { inputstream ->
            val people = sut.parse(inputstream, FileFormat.CSV, Person().apply{realmUid=realmId})

            people.forEach { Assertions.assertThat(it.realmUid).isEqualTo(realmId) }
            Assertions.assertThat(people).isNotEmpty
            Assertions.assertThat(people).hasSize(10)
            Assertions.assertThat(people[0].tags).hasSize(1)
        }

    }

    @Disabled("Test is ignored - it is only meant to run locally")
    @Test
    fun `SKIP ME - Should Deserialize from dump CSV`() {
        val mapper = CsvMapper()
        val sut = SmkccPersonImporter(CsvMapper())

        var testFile = "/Users/ahnpy001/workspace/incubation/harmonia/legacy-data/SMKCC_people.csv"

        openFile(testFile).use { inputstream ->
            val people = sut.parse(inputstream, FileFormat.CSV)

            Assertions.assertThat(people).isNotEmpty
            Assertions.assertThat(people).hasSize(1239)
        }

    }

    fun openFile(fileName: String): InputStream {
        return File(fileName).inputStream()
    }
}